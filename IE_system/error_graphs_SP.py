from utils_new import *
from metrics import *
import ipdb

import sequences.structured_perceptron as spc
import sequences.id_feature_bigram as idfc
#import sequences.id_feature as idfc
import sequences.extended_feature as exfc

import sequences.confusion_matrix as cm
import matplotlib.pyplot as plt
import numpy as np

#np.seterr(all='print')

train,test,val = getData()

feature_mapper = exfc.ExtendedFeatures(train)

feature_mapper.build_features()
sp = spc.StructuredPerceptron(train.x_dict, train.y_dict, feature_mapper)
cs_train = MyChunkScore(train)
cs_test = MyChunkScore(test)


def plot_Error_vs_data(p_ini=10, step=5, num_epochs=130):
    X = np.arange(p_ini, 101, step) / 100.0
    J_train = np.zeros(X.size)
    J_val = np.zeros(X.size)

    for i, train_size in enumerate(X):
        sp.num_epochs = num_epochs
        new_train = reader.trimTrain(train, train_size)
        print("Training dataset size: ", train_size * 0.6, new_train.size())

        #sp.train_supervised(new_train)
        sp.train_supervised_bigram(new_train)
        pred_train = sp.viterbi_decode_corpus_bigram(new_train)
        pred_val = sp.viterbi_decode_corpus_bigram(val)
        J_train[i] = 100 * (1.0 - sp.evaluate_corpus(new_train, pred_train))
        J_val[i] = 100 * (1.0 - sp.evaluate_corpus(val, pred_val))

    plt.plot(X, J_train, 'r', X, J_val, 'b')
    plt.show()


def plot_Error_vs_Epochs(ep_ini=5, ep_fin=121, step=10):
    X = np.arange(ep_ini, ep_fin, step)
    J_train = np.zeros(X.size)
    J_val = np.zeros(X.size)

    print("Running %d iterations" % X.size)

    for i, epocas in enumerate(X):
        print("Epocas : ", epocas)
        sp.num_epochs = epocas
        sp.train_supervised_bigram(train)
        #sp.train_supervised(train)
        #sp.regularization_param = 0.001

        pred_train = sp.viterbi_decode_corpus_bigram(train)
        pred_val = sp.viterbi_decode_corpus_bigram(val)

        #pred_train = sp.viterbi_decode_corpus(train)
        #pred_val = sp.viterbi_decode_corpus(val)

        cs_train.evaluate(train,pred_train)
        cs_test.evaluate(test,pred_val)

        #J_train[i] = 100 * (1.0 - sp.evaluate_corpus(train, pred_train))
        #J_val[i] = 100 * (1.0 - sp.evaluate_corpus(val, pred_val))

        J_train[i] = cs_train.f_measure()
        J_val[i] = cs_test.f_measure()

    opt_epoch = X[np.argmin(J_val)]
    print("Best number of epochs: ", opt_epoch)

    plt.plot(X, J_train, 'r', X, J_val, 'b')
    plt.show()


def plot_Error_vs_Reg(epochs = 10, reg_ini=0, reg_fin=3, step=1):
    X = np.arange(reg_ini, reg_fin, step)
    J_train = np.zeros(X.size)
    J_val = np.zeros(X.size)
    sp.num_epochs = epochs

    print("Running %d iterations" % X.size)

    for i, reg in enumerate(X):
        print("Regularization : ", reg)
        sp.regularization_param = reg
        sp.train_supervised_bigram(train)
        pred_train = sp.viterbi_decode_corpus_bigram(train)
        pred_val = sp.viterbi_decode_corpus_bigram(val)
        J_train[i] = 100 * (1.0 - sp.evaluate_corpus(train, pred_train))
        J_val[i] = 100 * (1.0 - sp.evaluate_corpus(val, pred_val))
        print("max param: ",np.max(sp.parameters))

    reg_opt = X[np.argmin(J_val)]
    print("Best regularization parameter: ", reg_opt)

    print("Valores de J_val:")
    print(J_val)
    plt.plot(X, J_train, 'r', X, J_val, 'b')
    plt.show()


# plot_Error_vs_data()
plot_Error_vs_Epochs(ep_ini=1, ep_fin=1, step=20)

#plot_Error_vs_Reg(epochs=10, reg_ini=0, reg_fin=1, step=0.1)
#plot_Error_vs_Reg(epochs=10, reg_ini=0, reg_fin=0.101, step=0.001)





