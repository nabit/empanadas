#!/bin/bash
# script para Generar TMVE a partir de resultados de R
# run trunk
# enjoy!

if [ $# -lt 2 ]; then
	echo "usage: gen_tmve <clusters> <model> <res_dir>"
	exit 0
fi

clusters=$1
model=$2
res_dir=$3

cd ../../../clustering/lda-c-dist
data=../$countsdir/word_counts.dat
resultsdir=${countsdir}_${clusters}
if [ ! -d $resultsdir ]; then
	mkdir $resultsdir
fi
./lda est 1/$clusters $clusters settings.txt $data seeded ../$resultsdir

beta=../$resultsdir/final.beta
gamma=../$resultsdir/final.gamma

echo "Sampling topic words"
python2 topics.py $beta ../$countsdir/vocab.dat 15 > ../$resultsdir/topics.dat

echo "=============================================="
echo "Generating Doc-Title map"
echo "=============================================="

cd ../$countsdir
ipython2 make_doc_title_map.py

echo "=============================================="
echo "Generating SQLite DB"
echo "=============================================="

cd ../trunk
# borrar actual docs
if [ ! -d $resultsdir ]; then
	mkdir $resultsdir
	content="template: JobBrowser\ndatabase:  $resultsdir/jobs_$clusters\ntitle: Empleatron data"
	echo -e $content > $resultsdir/job.tmv
fi

if [ -d "$results_dir/job" ]; then
	rm -R "$results_dir/job"
fi
# borrar sqlite db
if [ -f "$resultsdir/jobs_$clusters" ]; then
	rm "$resultsdir/jobs_$clusters"
fi


ipython2 lib/generate_db.py $resultsdir/jobs_$clusters $data $beta $gamma ../$countsdir/vocab.dat ../$countsdir/title_map.dat

echo "=============================================="
echo "Running Trunk"
echo "=============================================="

python2 src/tmve.py -v $resultsdir/job.tmv