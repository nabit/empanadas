# SYSTEM PIPELINE
import os, sys
import pymongo
from pymongo import MongoClient
from datetime import date, datetime

import os,sys
import pdb,ipdb

path_utils = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
features_path = os.path.join(path_utils,'carr_model')

sys.path.append(features_path)
sys.path.append(path_utils)

import ext2 as exfc2
from utils import tokenizer
from utils_new import saveObject, uploadObject
from utils_pipe import *

#########################################################################
## MongoDB authentication and connection
client = MongoClient()
db = client.JobDB

#tokenized = db.core_tokenized
db_prueba = db.prueba


if __name__ == '__main__':
	"""
	model = uploadObject('sp_5_by_sent')
	docs_path = '/home/ronald/clustering/jobs_enero/docs'
	count = 0
	n_docs = 0
	for root, dirs, filenames in os.walk(docs_path):
		for f in filenames:
			if f[-1]!='~':
				#print("Doc:",f)
				found = False
				for ff in db.prueba.find({"name":f}):
					found=True
					continue
				if found:
					continue
					
				doc = readDoc(os.path.join(docs_path,f))
				n_docs += 1
				name_entities = []
				for sent in doc:
					sequence = makeSequence(sent)
					pred_sequence,_ = model.viterbi_decode_bigram(sequence)
					pred_sequence.sequence_list.seq_list[0].y = pred_sequence.y

					nes = getNameEntities(pred_sequence)
					name_entities.extend(nes)

				carreras = discretizeCareers(name_entities)
				if carreras==[]:
					print(name_entities)
					print(carreras)
					ipdb.set_trace()
				else:
					try:
						db_prueba.insert({'name':f, 'carreras':carreras})
					except:
						print("NO INSERTO")

				if count%100==0:
					print('-->',count)
				count +=1
	"""

	topics = 10
	doc_topic = load_doc_topic_matrix(topics = topics,folder='jobs_enero',n_docs=db_prueba.count())
	topicsByCareer = {}

	docs = db_prueba.find()
	for pointer in docs:
		name = pointer['name']
		id = int(name[3:])-1 # doc...
		careers = list(pointer['carreras'])
		if len(careers)==1 and careers[0]=='otros':
			continue
		for car in careers:
			if car not in topicsByCareer:
				topicsByCareer[car] = np.zeros(topics)
			topicsByCareer[car] += doc_topic[id]
	output = open('career_topic_%i.csv' % topics,'w')

	for car,topics in topicsByCareer.items():
		line = car + ' ' + ' '.join([str(top) for top in topics]) + '\n'
		output.write(line)

	
