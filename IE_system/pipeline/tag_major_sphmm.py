import os, sys
import ipdb

from utils_pipe import *

utils_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(utils_path)
from utils_new import uploadObject, ChunkSet

#########################################################################
BASE_DIR = os.path.dirname( os.path.dirname(os.path.dirname(os.path.abspath(__file__))) )
clustering_dir = os.path.join(BASE_DIR,'dataR')
models_dir = os.path.join(utils_path,'SP hmm_feat models')

carrmodel_path = os.path.join(models_dir,'carr')
sys.path.append(carrmodel_path)

_dir = 'NE_ene_ing_sp_hmm'
source_dir = os.path.join(clustering_dir,'docs/full_ene_ing')
docs_ne__dir =  os.path.join(clustering_dir,'docs/'+_dir)
counts_dir = os.path.join(clustering_dir,'counts/'+_dir)

#########################################################################
#########################################################################
print("Loading model...")
#carr_model = uploadObject(os.path.join(carrmodel_path,'carr_sp_hmm'))


def parallel_funct(docname):
	print(docname)
	doc = readDoc(os.path.join(source_dir,docname))
	name_entities = []

	for sent in doc:
		sequence = makeSequence(sent)
		carr_pred,_ = carr_model.viterbi_decode_bigram(sequence)
		carr_pred.sequence_list.seq_list[0].y = carr_pred.y

		nes = getNameEntities(carr_pred)
		name_entities.extend(nes)

	ipdb.set_trace()
	
	carreras = discretizeCareers(name_entities)
	return carreras


if __name__ == '__main__':
	stem=False
	startTime = datetime.now()

	filenames = [f for r,d,fn in os.walk(docs_ne__dir) for f in fn if f[-1]!='~']
	new_title_map = open(os.path.join(counts_dir,"title_map.dat"),'w')
	new_MbyD = open(os.path.join(counts_dir,"majors_by_doc.dat"),'w')

	MbyD_reference      = [line             for line in open(os.path.join(clustering_dir,'counts/NE_ene_ing/majors_by_doc.dat'),'r')
												if line!='\n']
	title_map_reference = [line.strip('\n') for line in open(os.path.join(clustering_dir,'counts/NE_ene_ing/title_map.dat'),'r')
												if line!='\n']
	ipdb.set_trace()

	for i,docname in enumerate(title_map_reference):
		if docname in filenames:
			new_title_map.write(docname + '\n')
			new_MbyD.write(MbyD_reference[i])

	"""
	parallelize = False
	
	
	majors_by_doc = []
	if parallelize:
		pool = Pool(processes=3)
		majors_by_doc = pool.map(parallel_funct,filenames)
		pool.close()
		pool.join()
	else:
		count = 0
		for docname in filenames:
			majors_by_doc.append( parallel_funct(docname) )
			if count%1==0:
				print("->",count)
			count+=1
			if count>=10:
				break
	

	output = open(os.path.join(counts_dir,'majors_by_doc.dat'),'w')
	for i in range(len(majors_by_doc)):
		majors = majors_by_doc[i]
		docname= filenames[i]
		if len(majors)>0:
			new_title_map.write(docname + '\n')
			output.write(','.join(majors) + '\n')
	"""

	print("Execution time: ",datetime.now()-startTime)