from utils_new import *
import sequence_lxmls.crf_online as crfo
import sequence_lxmls.id_feature as idfc
#import sequences.id_feature as idfc
import sequence_lxmls.extended_feature as exfc

if __name__ == '__main__':
    print("Reading data...")
    #train,test,val = getData(mode='by_doc',target='TOTAL')
    train,test,val = getData(START_END_TAGS=False)

    #ipdb.set_trace()

    print("Building features...")
    #feature_mapper = idfc.IDFeatures(train)
    feature_mapper = exfc.ExtendedFeatures(train)
    feature_mapper.build_features()

    #ipdb.set_trace()
    print("Init CRF online")
    crf_online = crfo.CRFOnline(train.x_dict, train.y_dict, feature_mapper)
    crf_online.num_epochs = 20
    crf_online.initial_learning_rate=10.0
    #crf_online.regularizer=0.0001

    print("Training...")
    crf_online.train_supervised(train)

    print("Predicting...")
    pred_train = crf_online.viterbi_decode_corpus(train)
    pred_val   = crf_online.viterbi_decode_corpus(val)
    pred_test  = crf_online.viterbi_decode_corpus(test)

    print("Evaluating...")
    eval_train = crf_online.evaluate_corpus(train, pred_train)
    eval_val   = crf_online.evaluate_corpus(val, pred_val)
    eval_test  = crf_online.evaluate_corpus(test, pred_test)

    print("CRF - Extended Features Accuracy Train: %.3f Val: %.3f Test: %.3f"%(eval_train, eval_val,eval_test))

    print("Metrics: Training data")
    cs_train = MyChunkScore(train)
    #cs_train = MyChunkScore(train, mode = 'TODO')     ######
    cs_train.evaluate(train,pred_train)
    print(cs_train)

    print("---------------")

    print("Metrics: Testing data")
    cs_test = MyChunkScore(test)
    #cs_test = MyChunkScore(test, mode = 'TODO')      ######
    cs_test.evaluate(test,pred_test)
    print(cs_test)
    #ipdb.set_trace()

    print("Confussion Matrix: sapeee!") # DICT HAS TO BE FROM THE SAME PART (TRAIN, VAL OR TEST)
    confusion_matrix = cm.build_confusion_matrix(val.seq_list, pred_val, crf_online.get_num_states())

    cm.plot_confusion_bar_graph(confusion_matrix, val.y_dict, range(crf_online.get_num_states()), 'Confusion matrix')
    plt.show()
