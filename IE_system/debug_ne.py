from utils_new import *
mode = 'by_sent'
#mode = 'by_doc'
train,test,val = getData(test=0, val=0, tags = ['CARR'], mode=mode,filter_empty=True)

chunks_train = ChunkSet(dataset=train)


lens = [ch.length for ch in chunks_train.chunk_list]
print("n chunks:",len(chunks_train.chunk_list))
print("avg len:",np.mean(lens))

"""
from nltk import FreqDist
ff = FreqDist(lens)

a = ff.most_common()
acum = [a[0]]
for i in range(1,len(a)):
	acum.append( (a[i][0],acum[i-1][1]+a[i][1]) )

total = acum[-1][1]
acum = [(p,q/total) for (p,q) in acum]

for chunk in chunks_train.chunk_list:
	if chunk.length > 5:
		print(train[chunk.sequence_id])
		print(chunk.sequence_id)
		print(train[chunk.sequence_id].file_id)
		ipdb.set_trace()

#####################
chunks_val = ChunkSet(dataset=val)

chunks_test = ChunkSet(dataset=test)

for chunk in chunks_test.chunk_list:
	if chunk.length > 3:
		print(test[chunk.sequence_id])
		print(chunk.sequence_id)
		print(test[chunk.sequence_id].file_id)
		ipdb.set_trace()

"""