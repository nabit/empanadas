import pymongo
from pymongo import MongoClient
from datetime import datetime, date

client = MongoClient()
db = client.JobDB.core

res = []
for mm in range(6,13):
	ini = date(2014,mm,1)
	fin = ''
	if mm==12:
		fin = date(2015,1,1)
	else:
		fin = date(2014,mm+1,1)
	cc = db.find({'$and':[
							{'date':{'$gte': datetime.combine(ini,datetime.min.time())}},
							{'date':{'$lt' : datetime.combine(fin,datetime.min.time())}}
							]}).count()
	res.append((mm,cc))

for mm in range(1,3):
	ini = date(2015,mm,1)
	fin = date(2015,mm+1,1)	
	cc = db.find({'$and':[
							{'date':{'$gte': datetime.combine(ini,datetime.min.time())}},
							{'date':{'$lt' : datetime.combine(fin,datetime.min.time())}}
							]}).count()
	res.append((mm,cc))

print(res)