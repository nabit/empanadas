# SYSTEM PIPELINE
import os, sys

from utils_new import *

from nltk.stem import SnowballStemmer
from nltk.stem.snowball import SpanishStemmer
import re, string

stemmer = SpanishStemmer()
eng_stemmer = SnowballStemmer("english")

import pdb,ipdb

##########################################################################################

if __name__ == '__main__':
	word_dict = uploadObject('word_dict')

	track = {}

	WORD_THR = 5
	filtered = [(k,v) for k,v in word_dict.items() if v <= WORD_THR]
	for k,v in filtered:
		tag = assignFilterTag(k)
		if tag not in track:
			track[tag] = []
		track[tag].append(k)
		del word_dict[k]
		insert(word_dict,tag,v)

	temp = {}

	for k,v in word_dict.items():
		if k in filter_names:
			insert(temp,k,v)
			continue
		if k == '':
			continue
		tag = assignFilterTag(k,permanent_filters)
		if tag != RARE:
			if tag not in track:
				track[tag] = []
			track[tag].append(k)
			insert(temp,tag,v)
		else:
			insert(temp,k,v)

	word_dict = temp

	temp_stem = set()
	for k,v in word_dict.items():
		if k not in filter_names:
			try:
				temp_stem.add(stemmer.stem(k))
			except:
				temp_stem.add(eng_stemmer.stem(k))

	temp_stem = list(temp_stem)

	saveObject(word_dict,'word_dict_filtered')
	saveObject(temp_stem,'stem_dict')
	saveObject(track,'filtered_words')