__author__ = 'kevinbm'

import sys
import numpy as np
import operator

args = sys.argv[1:]

doc_tf = args[0]
vocab_file = args[1]

matrix = list()
with open(doc_tf, 'r') as ff:
    text = ff.read()
    text = text.split('\n')[:-1]
    for line in text:
        line = line.split(' ')
        row = [(e.split(':')[0], e.split(':')[1]) for e in line[1:]]
        matrix.append(row)

vocab = dict()
with open(vocab_file, 'r') as ff:
    text = ff.read()
    text = text.split('\n')
    for i, term in enumerate(text):
        vocab[i] = term


N = len(matrix)
idf = dict()
for doc in matrix:
    for term in doc:
        Id = int(term[0])
        if idf.get(Id) is None:
            idf[Id] = 1
        else:
            idf[Id] += 1

for key in idf:
    idf[key] = np.log10(N / idf[key])

tfidf_scores = dict()

for doc in matrix:
    for term in doc:
        Id = int(term[0])
        Freq = int(term[1])
        if tfidf_scores.get(vocab[Id]) is None:
            tfidf_scores[vocab[Id]] = (1 + np.log10(Freq)) * idf[Id]
            #tfidf_scores[vocab[Id]] = Freq
        else:
            if (1 + np.log10(Freq)) * idf[Id] > tfidf_scores[vocab[Id]]:
                tfidf_scores[vocab[Id]] = (1 + np.log10(Freq)) * idf[Id]
            # tfidf_scores[vocab[Id]] += Freq


#tfidf_scores = sorted(tfidf_scores.items(), key=operator.itemgetter(1))
#tfidf_scores.reverse()

tfidf_scores = sorted(idf.items(), key=operator.itemgetter(1))
tfidf_scores.reverse()

with open('TFIDF_PRUNE.txt', 'w') as ff:
    for term in tfidf_scores:
        ff.write(vocab[term[0]] + " " + str(term[1]))
        ff.write('\n')