__author__ = 'ronald'
"""
PRUEBAS DE FRECUENCIA DE PALABRAS
"""
from nltk import FreqDist
import ipdb
import matplotlib.pyplot as plt

careers_dir = "careers tagged/"
random_dir = "random/"

RARE = "<RARE>"

RARE_DICT = {}
words = []
data = []
for i in range(1, 401):
    doc = []
    for line in open(careers_dir + str(i) + '.tsv'):
        line = line.strip('\n')
        if len(line)>0:
            temp = line.split('\t')
            doc.append(temp)
            pos = temp[1]
            x = temp[0]
            words.append(x.lower())
        else:
            doc.append('')
    data.append(doc)
"""
for i in range(1, 401):
    doc = []
    for line in open(random_dir + str(i) + '.tsv'):

        line = line.strip('\n')
        if len(line)>0:
            temp = line.split('\t')
            doc.append(temp)
            pos = temp[1]
            x = temp[0]
            words.append(x.lower())
        else:
            doc.append('')
    data.append(doc)
"""
words = FreqDist(words)
UMBRAL = 1
lexicon = [word for word in words.keys() if words[word] > UMBRAL]

vals = list(words.values())
vals.sort(reverse=True)
print(len(lexicon))

plt.hist(vals,bins=len(vals)-100)

plt.show()
