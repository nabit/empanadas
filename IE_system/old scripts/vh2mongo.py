__author__ = 'ronald'
''' Migracion de SQL a MongoDB de tablas core_description, core_description_jobs & core_details
    LOG
    * 6.07 s para query SELECT * FROM DETAILS + django_mysql_driver
'''

import os, sys
import time
import pymongo
from datetime import date, datetime
from pymongo import MongoClient

import pdb


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
WWW_DIR = os.path.join(BASE_DIR, 'www')
UTIL_DIR = os.path.join(BASE_DIR, 'crawler/nlp scripts/')
sys.path.append(UTIL_DIR)
sys.path.append(WWW_DIR)
#	os.environ['DJANGO_SETTINGS_MODULE'] = 'project.dev'

from utilities import *


## MongoDB authentication and connection
uri = 'mongodb://ronotex:1234@localhost/admin'
client = MongoClient(uri)
# conexion a nueva base de datos
db = client.JobDB
# Coleccion de trabajos
job_posts = db.core_peru


carreras = list(set(getDocnameByCareer().values()))

cont = 0

for carrera in carreras:
  name = 'vh_' + carrera
  data = readHashVector('vh_' + carrera)
  
  for post in data:
    _id = post[0] + '_' + post[1]
    job_posts.update({'_id' : _id}, {'$addToSet': { 'carreras': carrera }})
    
    if cont % 1000 == 0:
      print '-->',cont
    cont += 1

