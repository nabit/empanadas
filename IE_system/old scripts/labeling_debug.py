import nltk
import pdb
import numpy as np
from sklearn.cross_validation import train_test_split
from nltk.corpus.reader.util import concat
import os, sys

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.append(path)

from utils import *
import ipdb

data = []


for i in range(1,401):
	doc=[]
	#doc.append( tuple([BR_LABEL,BR_LABEL]) )
	k = 1
	for line in open('../careers tagged/' + str(i) + '.tsv'):
		line = line.strip('\n')
		if len(line)>0:
			temp = line.split('\t')
			if len(temp) != 3 or '' in temp:
				print("--",i,k)
				#pdb.set_trace()
			doc.append( tuple(temp) )

#			if k>1 and len(temp[-1]) > 1:
#				if temp[]
		else:
			#doc.append(tuple([BR,BR_LABEL,BR_LABEL]))
			doc.append( (BR,BR,BR) )
		k += 1
	doc.pop()
	data.append(doc)


ne = [ne for doc in data for w,pos,ne in doc]
ff = nltk.FreqDist(ne)

TAGS = ['REQ','AREA','JOB','FUN','CARR']
for i,doc in enumerate(data):
	for k in range(1,len(doc)):
		tag = doc[k][-1][0]
		prev = doc[k-1][-1][0]
		if tag == 'I' and prev != 'B' and prev !='I':
			print(1 + i, k,tag,prev)


for i,doc in enumerate(data):
	for k in range(1,len(doc)):
		tag = doc[k][-1]
		prev = doc[k-1][-1]
		if prev != 'O' and tag != 'O' and tag != BR and prev != BR and prev[2:] != tag[2:] and prev[2:] in TAGS and tag[2:] in TAGS:
			print(1 + i, k,prev,tag)


"""
for i in range(1,401):
	doc = []
	k = 1
	for line in open('../random/' + str(i) + '.tsv'):
		line = line.strip('\n')
		if len(line)>0:
			temp = line.split('\t')
			if len(temp) != 3 or temp[-1] == '':
				print("--",i,k)
				#pdb.set_trace()
			doc.append( tuple(temp) )
		else:
			#doc.append(tuple([BR,BR_LABEL,BR_LABEL]))
			doc.append( (BR,BR,BR) )
		k += 1
	doc.pop()
	data.append(doc)

ne = [ne for doc in data for w,pos,ne in doc]
ff = nltk.FreqDist(ne)

pdb.set_trace()

for i,doc in enumerate(data):
	for k in range(1,len(doc)):
		tag = doc[k][-1][0]
		prev = doc[k-1][-1][0]
		if tag == 'I' and prev != 'B' and prev !='I':
			print(1 + i, k,tag,prev)


"""

"""

data_ancora = getDataAncora(100)
words = len(concat(data_ancora))
print("Words from ancora :",words)

TRIALS = 1
wit_res = np.zeros(TRIALS)
gt_res = np.zeros(TRIALS)
hmm_res = np.zeros(TRIALS)

for i in range(TRIALS):
	(train,test) = train_test_split(data,test_size=0.10)

	train.extend(data_ancora)

	print("------------ ",i," --------------------")
	print("Training and testing Witten")
	wit_res[i] = train_and_test(train,test,witten)

	print("Training and testing Good Turing")
	gt_res[i] = train_and_test(train,test,gt)

	print("Entrenando HMM default...")
	hmm = HiddenMarkovModelTagger.train(train)

	print("Evaluando HMM default...")
	hmm_res[i] = hmm.evaluate(test) * 100


print("Witten : %.4f" % np.mean(wit_res))
print("GTuring : %.4f" % np.mean(gt_res))
print('HMM: %.4f' %  np.mean(hmm_res))



#pdb.set_trace()

#

DATA JODB | promedio de 10 pasadas
Evaluando HMM default...
Witten : 90.1591
GTuring : 90.3913
HMM: 88.1189

DATA JOBDB + 50 ANCORA	| promedio de 10 pasadas
Witten : 90.8386
GTuring : 91.3587
HMM: 88.9021


DATA JOBDB + 100 ANCORA
Witten : 91.7348
GTuring : 91.9236
HMM: 89.7420

"""


