use JobDB
// SALARY -- incluir tmb no especificado
db.core.find({$or:[ { salary:{$regex:'^[Ii]ndefinido\s*$'}},
                         { salary:{$regex:'^[x.]+$'}           }]}, {_id:0,salary:1}
                 ).sort({salary:-1})
db.core.count({$or:[ { salary:{$regex:'^[Ii]ndefinido\s*$'}},
                          { salary:{$regex:'^[x.]+$'}           }
                        ]} )
                        
db.core.update({$or:[ { salary:{$regex:'^[Ii]ndefinido\s*$'}},
                           { salary:{$regex:'^[x.]+$'}           }
                         ]
                    },
                    {$unset : {salary:1}},
                    {multi : true}
                     )

// COMPANY
db.core.find({$or:[ { company:{$regex:'^[Ii]ndefinido\s*$'}},
                         { company:{$regex:'^[x.]+$'}           }]}, {_id:0,company:1}
                 ).sort({company:-1})
// PLACE
db.core.find( {$or:[ { place:{$regex:'^[Ii]ndefinido\s*$'}},
                          { place:{$regex:'^[x.]+$'}           },
                          { place:{$regex:'^[Nn]o especificado\s*$'}           }
                        ]
                   },
                   {_id:0,place:1}       
                  ).sort({place:-1})
db.core.update( {$or:[ { place:{$regex:'^[Ii]ndefinido\s*$'}},
                            { place:{$regex:'^[x.]+$'}           },
                            { place:{$regex:'^[Nn]o especificado\s*$'}           }
                          ]
                     },
                     {$unset: {place:1}},
                     {multi:true}
                   )


// AREA
db.core.aggregate([{$group : {_id:'$area',sum:{$sum:1}} }])
db.core.find( {$or:[ { area:{$regex:'^[Ii]ndefinido\s*$'}},
                          { area:{$regex:'^[x.]+$'}           },
                          { area:{$regex:'^[Nn]o especificado\s*$'}           }
                        ]
                   },
                   {_id:0,area:1}       
                  ).sort({area:-1})
// DESC
db.core.aggregate([{$group : {_id:'$description',sum:{$sum:1}} },
                        {$sort : {description:1}}
                       ],
                       {allowDiskUse : true}
                      )
db.core.find( {$or:[ { description:{$regex:'^[Ii]ndefinido\s*$'}},
                          { description:{$regex:'^[x.]+$'}           },
                          { description:{$regex:'^\s*[Nn]o especificado\s*$'}           }
                        ]
                   },
                   {_id:0,description:1}       
                  ).sort({description:-1})
var dafuq1 = " \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n  \n ";
var dafuq2 = " \n  \n ";
db.core.find( {$or:[ { description : dafuq1},
                          { description : dafuq2}
                        ]
                   },
                   {_id:0,description:1}       
                  ).sort({description:-1})
db.core.count( {$or:[ { description : dafuq1},
                          { description : dafuq2}
                        ]
                   },
                   {_id:0,description:1}       
                  )
db.core.update( {$or:[ { description : dafuq1},
                            { description : dafuq2}
                          ]
                     },
                     {$unset : {description:1}},
                     {multi : true}
                   )
