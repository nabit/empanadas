import numpy as np
from utils import *
import os, sys
import pdb
import pymongo
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
# conexion a nueva base de datos
db = client.JobDB
# Coleccion de trabajos
job_posts = db.core_peru

ids = []
with open('careers tagged/ids', 'r') as file:
    for id in file:
        ids.append(id.strip('\n'))

query = {'carreras': {'$exists': 1}, '$and': []}

for id in ids:
    item = {'_id': {'$ne': id}}
    query['$and'].append(item)

L = job_posts.find(query).count()
data = job_posts.find(query).batch_size(500)

print(L)

hmm = uploadModel('witten_model')

idx = list(set(np.random.randint(0, L, 400)))

while len(idx) < 400:
    idx.extend(list(set(np.random.randint(0, L, 400 - len(idx)))))
    idx = list(set(idx))

idx.sort()

print(idx[:10])

ii = 0
cont = 1

id_file = open('random/ids', 'w')

for post in data:
    if ii in idx:
        text = '\n'.join([post.get('title', ''), post.get('description', '')])
        if text == '':
            print("No texto :S")
            continue

        id_file.write(post.get('_id') + '\n')
        sents = tokenizer(text)

        with open('random/' + str(cont) + '.tsv', 'w') as file:
            for sent in sents:
                try:
                    tagged = hmm.tag(sent)
                except:
                    pdb.set_trace()

                for (word, pos) in tagged:
                    file.write('%s\t%s\tO\n' % (word, pos))
                file.write('\n')
        cont += 1
        if cont % 50 == 0:
            print("-->", cont)
        # pdb.set_trace()
    ii += 1
    if ii % 100 == 0:
        print("..(ii)..>", ii)

