db.prueba.aggregate([
  {$project:
    {_id:0,
     carreras:1
    }
  },
  {$unwind:'$carreras'},
  {$group :
    {_id:'$carreras',
     num: {$sum:1}
    }
  },
  {$sort:
    {num:-1}
  },
  {$out: 'major_counts'}
])
