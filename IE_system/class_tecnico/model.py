
from utils_model import *

from sklearn.metrics import classification_report, accuracy_score
from sklearn.decomposition import TruncatedSVD as SVD
from sklearn.decomposition import PCA,KernelPCA
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.preprocessing import normalize
from sklearn.lda import LDA
from scipy import sparse
from sklearn import svm, grid_search

from prueba_nec import plot2D, plot3D


###########################################################################################################
###########################################################################################################
###########################################################################################################

if __name__ == '__main__':
	#makeDataset()
	# SAVE/LOAD components
	upload_vocab = True
	upload_matrixes = False
	#mode = 'BOOL'
	mode = 'TF'
	#mode = 'TF-IDF'

	ids = uploadObject("sample_ids")
	Y = uploadObject("sample_classes")
	Y = np.array([int(clase) for clase in Y])

	train,val,test = splitData(ids,test=0.1,val=0.1)
	X_train = []
	X_test = []
	X_val = []
	Y_train,Y_val,Y_test = splitData(Y,test=0.1,val=0.1)

	if upload_vocab:
		vocabulary = uploadObject('vocab')
		idf_count = uploadObject('idf_count')
	else:
		vocabulary,idf_count = makeVocabs(train)
		saveObject(vocabulary,'vocab')
		saveObject(idf_count,'idf_count')


	if upload_matrixes:
		X_train = uploadObject('xtrain')
		X_test = uploadObject('xtest')
		X_val = uploadObject('xval')
		print("Matrixes loaded...")
	else:
		# FEATURE EXTRACTION
		print("Extracting features from training set...")
		for i,id in enumerate(train):
			post = db.find_one({'$and':[{'_id.description':id['description'] },
									    {'_id.details':id['details'] }
									   ]})
			tokens = post["tokens"]
			X_train.append(featureExtraction(tokens,vocabulary,weight=mode,idf_count=idf_count))

		print("Extracting features from validation set...")
		for i,id in enumerate(val):
			post = db.find_one({'$and':[{'_id.description':id['description'] },
									    {'_id.details':id['details'] }
									   ]})
			tokens = post["tokens"]
			X_val.append(featureExtraction(tokens,vocabulary,weight=mode,idf_count=idf_count))

		print("Extracting features from testing set...")
		for i,id in enumerate(test):
			post = db.find_one({'$and':[{'_id.description':id['description'] },
									    {'_id.details':id['details'] }
									   ]})
			tokens = post["tokens"]
			X_test.append(featureExtraction(tokens,vocabulary,weight=mode,idf_count=idf_count))
		saveObject(X_train,'xtrain')
		saveObject(X_test,'xtest')
		saveObject(X_val,'xval')

	ipdb.set_trace()
	
	###########################################################################
    # normalize
	print("Normalizing..")
	X_train = normalize(X_train, copy = False)
	X_val   = normalize(X_val, copy=False)
	X_test  = normalize(X_test, copy=False)
	
	label_names = ['tecn','otro','univ']
    ###########################################################################
	print("Dimesionality reduction..")

	### SVD
	svd = SVD(n_components=2, n_iter = 10)
	svd.fit(X_train)
	X_svd = svd.transform(X_train)

	svd3 = SVD(n_components=3, n_iter = 10)
	svd3.fit(X_train)
	X_svd3 = svd3.transform(X_train)
	plot2D(X_svd,Y_train, label_names, 'SVD reduction')
	plot3D(X_svd3,Y_train, label_names, 'SVD reduction 3 comp')

	
	### PCA
	#kpca = KernelPCA(kernel='rbf',gamma=10)
	pca = PCA()
	#score = pca.explained_variance_ratio_  # despues de hacer fit

	# select relevant original features w/ CHI2
	selection = SelectKBest(chi2,k=100)
	combined_features = FeatureUnion([("pca", pca), ("univ_select", selection)])
	
	X_dr = combined_features.fit(X_train, Y_train).transform(X_train)
	plot2D(X_dr,Y_train, label_names, 'PCA+CHI2_100')
	plot3D(X_dr,Y_train, label_names, 'PCA+CHI2_100 3 comp')
	
	#X_dr = kpca.fit_transform(X_train)
	X_dr = pca.fit_transform(X_train)
	plot2D(X_dr,Y_train, label_names, 'PCA')
	plot3D(X_dr,Y_train, label_names, 'PCA 3 comp')

	"""
    print("Dimesionality reduction LDA...")
	lda = LDA(n_components = 2)
	lda.fit(X_train, Y_train)
	X_lda = lda.transform(X_train)
    
	lda3 = LDA(n_components = 3)
	lda3.fit(X_train, Y_train)
	X_lda3 = lda3.transform(X_train)
	plot2D(X_lda,Y_train, label_names, 'LDA reduction')
	"""

	ipdb.set_trace()

	print("Ploting...")	
	plt.show()

	###########################################################################
	"""
	print("Dimesionality reduction SVD...")
	svd = SVD(n_components=300)
	svd.fit(X_train)

	X_train = svd.transform(X_train)
	X_val = svd.transform(X_val)
	X_test = svd.transform(X_test)


	print("Fitting SVM...")
	
    
	svmc = svm.SVC(kernel = 'rbf', C=100, gamma=0.001)
	
	print("Predicting VAL...")
	pred_val = svmc.predict(X_val)
	print(classification_report(Y_val, pred_val, target_names=label_names))
	"""