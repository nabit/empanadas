import os,sys
from pymongo import MongoClient
import random
import ipdb
from math import log

from collections import Counter
from multiprocessing import Pool
from functools import partial
from datetime import datetime

utils_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(utils_path)
from utils_new import *


######################
client = MongoClient()
db = client.JobDB.core_tokenized

#####################
samples = 2000
random.seed(42)		# siempre, justo antes de usar random
random_idx = random.sample(range(258000),samples)

RANDOM_STATE = 42
np.random.seed(RANDOM_STATE)
#################################

# Get global stopwords
stopwords = set()
for line in open('stopwords','r'):
	line = line.strip('\n')
	if line!='':
		line = ' '.join([stemAugmented(w) for w in line.lower().split(' ') ])
		stopwords.add(line)
print("Stopword list loaded...")

#################################
#################################
ident_car = set()
ident_tec = set()

IDENTIFIER_STEM_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'pipeline/identifiers')
for root, dirs, filenames in os.walk(IDENTIFIER_STEM_DIR):
	for f in filenames:
		if f[-1]!='~' and f!='tecnico':
			for line in open(os.path.join(IDENTIFIER_STEM_DIR, f)):
				line = line.strip('\n')
				if line!='':
					ident_car.add(line)

for line in open(os.path.join(IDENTIFIER_STEM_DIR, 'tecnico')):
	line = line.strip('\n')
	if line!='':
		ident_tec.add(line)


#################################
#################################

def makeDataset():
	count = 0
	data = db.find().batch_size(10000)
	id_list = []
	classes = []

	for job in data:
		if count in random_idx:
			print("-->",count)

			tokens = job["tokens"]
			for sent in tokens:
				print(' '.join(sent))
			print("---------------------------------------")
			try:
				opc = input("Clase: [0]tecnico  [1]otro(default)  [2]universitario :: ")
				opc = int(opc)
				
				if opc not in [0,1,2]:
					print("Clase incorrecta, intente de nuevo")
					opc = input("Clase: [0]tecnico  [1]otro(default)  [2]universitario :: ")
			except:
				opc= 1
			id_list.append(job["_id"])
			classes.append(opc)
			print("###############################")
		count += 1

	saveObject(id_list,"sample_ids")
	saveObject(classes,"sample_classes")

def splitData(ids,test=0.1,val=0.1):
	val_size = val/(val+test)
	train_idx,temp    = train_test_split(ids ,test_size = test+val, random_state=RANDOM_STATE)
	test_idx ,val_idx = train_test_split(temp,test_size = val_size, random_state=RANDOM_STATE)
	train_idx = np.array(train_idx)
	test_idx = np.array(test_idx)
	val_idx = np.array(val_idx)

	return train_idx,val_idx,test_idx

def filterDict(_dict, thr=5):
	res = Counter()
	for k,v in _dict.items():
		if v>=thr:
			res[k]=v
	return res

def insert(_dict,key,val=1):
	if key not in _dict:
		_dict[key] = 0
	_dict[key] += val


filter_idx = list(range(1,12)) + [14,15,16]

def makeVocabs(ids):
	startTime = datetime.now()
	unigrams = Counter()
	bigrams = Counter()
	trigrams = Counter()
	idf_count = {}
	print("Data:",len(ids))

	# Get initial dicts
	count = 0
	for id in ids:
		if count%500 == 0:
			print("-->",count)
		count +=1
		post = db.find_one({'$and':[{'_id.description':id['description'] },
								    {'_id.details':id['details'] }
								   ]})
		tokens = post["tokens"]
		
		unique_tokens = set()
		for sent in tokens:
			n = len(sent)
			for i in range(n):
				ss = permanentFilter(sent[i],filter_idx)
				# unigrams
				if ss not in filter_names:
					w = stemAugmented(ss.lower())
					if w not in stopwords:
						insert(unigrams,w)
						unique_tokens.add(w)

				# bigrams
				if i>0:
					ss_1 = permanentFilter(sent[i-1],filter_idx)
					if  ss not in filter_names and \
						ss_1 not in filter_names:
						w = stemAugmented(ss.lower())
						w_prev = stemAugmented(ss_1.lower())
						phrase = ' '.join([w_prev,w])
						if all([ w not in stopwords,
								 w_prev not in stopwords,
								 phrase not in stopwords,
							]):
							insert(bigrams,phrase)
							unique_tokens.add(phrase)
				# trigrams
				if i>1:
					ss_1 = permanentFilter(sent[i-1],filter_idx)
					ss_2 = permanentFilter(sent[i-2],filter_idx)
					if  ss not in filter_names and \
						ss_2 not in filter_names:
						w = stemAugmented(ss.lower())
						w_prev = stemAugmented(ss_1.lower())
						w_prev_prev = stemAugmented(ss_2.lower())
						phrase = ' '.join([w_prev_prev,w_prev,w])
						if all([ w not in stopwords,
								 w_prev_prev not in stopwords,
								 phrase not in stopwords,
							]):
							insert(trigrams,phrase)
							unique_tokens.add(phrase)
		for ut in unique_tokens:
			insert(idf_count,ut)
	# Filter low freq terms/phrases
	print("Filtering low freq phrases...")
	UNI_THR = 3
	BI_THR = 2
	TRI_THR = 2

	unigrams = filterDict(unigrams,UNI_THR)
	bigrams  = filterDict(bigrams ,BI_THR)
	trigrams = filterDict(trigrams,TRI_THR)

	total = unigrams + bigrams + trigrams

	res = dict(zip(total.keys(),range(len(total))))

	# Calculating IDF score
	temp = np.zeros(len(res))
	for k,v in idf_count.items():
		if k in res:
			temp[res[k]] = log(len(ids)/v)
	idf_count = temp

	print("---------------------------------------")
	print("Execution time: ",datetime.now()-startTime)
	print("=======================================")

	return res,idf_count
	

def featureExtraction(doc,vocab,weight='FREQ',idf_count=[]):
	extra_feat = 3
	X_i = np.zeros(len(vocab)+extra_feat)
	
	if idf_count==[]:
		idf_count = np.zeros(len(vocab)+extra_feat)
	else:
		idf_count = np.append(idf_count,[0]*extra_feat)

	n_words = 0
	n_ident_car = 0
	n_ident_tec = 0
	# FREQ FEATURES
	for sent in doc:
		n = len(sent)
		for i in range(n):
			ss = permanentFilter(sent[i],filter_idx)
			# unigrams
			if ss not in filter_names:
				w = stemAugmented(ss.lower())
				if w not in stopwords and w in vocab:
					n_words+=1
					if weight=='BOOL':
						X_i[vocab[w]]=1
					else:
						X_i[vocab[w]]+=1
						
				if w in ident_car:
					n_ident_car+=1
				if w in ident_tec:
					n_ident_tec+=1
			# bigrams
			if i>0:
				ss_1 = permanentFilter(sent[i-1],filter_idx)
				if  ss not in filter_names and \
					ss_1 not in filter_names:
					w = stemAugmented(ss.lower())
					w_prev = stemAugmented(ss_1.lower())
					phrase = ' '.join([w_prev,w])
					if all([ w not in stopwords,
							 w_prev not in stopwords,
							 phrase not in stopwords,
							 phrase in vocab,
						]):
						if weight=='BOOL':
							X_i[vocab[phrase]]=1
						else:
							X_i[vocab[phrase]]+=1
							
					if phrase in ident_car:
						n_ident_car+=1
					if phrase in ident_tec:
						n_ident_tec+=1
			# trigrams
			if i>1:
				ss_1 = permanentFilter(sent[i-1],filter_idx)
				ss_2 = permanentFilter(sent[i-2],filter_idx)
				if  ss not in filter_names and \
					ss_2 not in filter_names:
					w = stemAugmented(ss.lower())
					w_prev = stemAugmented(ss_1.lower())
					w_prev_prev = stemAugmented(ss_2.lower())
					phrase = ' '.join([w_prev_prev,w_prev,w])
					if all([ w not in stopwords,
							 w_prev_prev not in stopwords,
							 phrase not in stopwords,
							 phrase in vocab,
						]):
						if weight=='BOOL':
							X_i[vocab[phrase]]=1
						else:
							X_i[vocab[phrase]]+=1
							
					if phrase in ident_car:
						n_ident_car+=1
					if phrase in ident_tec:
						n_ident_tec+=1
	# NUM OF WORDS FEATURES
	if weight=='TF':
		X_i /= n_words
	elif weight=='TF-IDF':
		X_i = (X_i/n_words) * idf_count
	m = len(vocab)
	X_i[m] = n_words
	X_i[m+1] = n_ident_car
	X_i[m+2] = n_ident_tec

	return X_i
