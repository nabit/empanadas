__author__ = 'ronald'

import sequences.confusion_matrix as cm
import matplotlib.pyplot as plt
import numpy as np
import ipdb, pdb

#from utils import *
from utils_new import *
from metrics import *

#import classifiers.linear_classifier as lcc
#import classifiers.naive_bayes as nbc
#import classifiers.perceptron as percc
import classifiers.svm as svmc

#import classifiers.mira as mirac
#import classifiers.max_ent_batch as mec_batch
#import classifiers.max_ent_online as mec_online

import classifiers.id_feature as idfc
import classifiers.id_features_NEC as featNEC

from mpl_toolkits.mplot3d import Axes3D


#############################################################################################

from sklearn import linear_model
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import classification_report
from sklearn import svm, grid_search

def plot2D(X,Y,labels, title_text=''):
    n_classes = len(labels)

    plt.figure()
    colors = 'rgbmky'
    colors = colors[:n_classes]

    markers = 'o^vs+d*'
    markers = markers[:n_classes]
    for c,m,i,class_name in zip(colors,markers,range(n_classes),labels):
        plt.scatter(X[Y == i,0],X[Y == i,1],c=c, marker=m, label=class_name,alpha = 0.8)
    plt.legend()
    plt.title(title_text)


def plot3D(X,Y,labels, title_text=''):
    n_classes = len(labels)

    colors = 'rgbmky'
    colors = colors[:n_classes]

    markers = 'o^vs+d*'
    markers = markers[:n_classes]
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for c,m,i,class_name in zip(colors,markers,range(n_classes),labels):
        ax.scatter(X[Y == i,0],X[Y == i,1],X[Y == i,2], c=c, marker=m, label=class_name,alpha = 0.8)
    
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')
    plt.legend()
    plt.title(title_text)


def run_classifier(model,Xtrain,Ytrain, Xtest, Ytest, parameters={},scores=[], classes=[]):
    for score in scores:
        print("###################################################")
        print('CV for score ' + score)
        gridCV = grid_search.GridSearchCV(model, parameters, scoring=score, cv = 5)
        gridCV.fit(Xtrain,Ytrain)

        print("Best %s: %.4f" % (score,gridCV.best_score_))
        print("Best parameters:")
        print(gridCV.best_params_)

        pred_test = gridCV.predict(Xtest)

        print("Metrics Testing data...")
        print(classification_report(Ytest, pred_test, target_names=classes))

#############################################################################################
from sklearn.metrics import confusion_matrix


if __name__ == '__main__':
    X_train,Y_train,X_test,Y_test,chunks_train = getData_NEC(mode='by_sent')

    class_names = [chunks_train.entity_classes.get_label_name(i) for i in range(len(chunks_train.entity_classes))]

    print("Dimesionality reduction SVD...")
    svd = SVD(n_components=1000)
    svd.fit(X_train)

    X_train = svd.transform(X_train)
    X_test = svd.transform(X_test)

    print("Dimesionality reduction LDA...")
    lda = LDA(n_components = 100)
    lda.fit(X_train, Y_train)
    X_train = lda.transform(X_train)
    X_test = lda.transform(X_test)

    ###############################################################################
    ## LOGISTIC REGRESSION
    """
    logreg = OneVsRestClassifier(linear_model.LogisticRegression(C = 1))
    print("########  LOGISTIC REGRESSION ########")
    logreg = linear_model.LogisticRegression(C = 1)

    parameters = {'C':[1e-4,0.01,0.1,1,10,50,100,1000]}

    scores = ['precision','recall','f1','accuracy']
    run_classifier(logreg, X_train, Y_train, X_test, Y_test, parameters, scores)
    print("##################################################################")
    """

    
    print("########         SVM          ########")
    svmc = svm.SVC()
    parameters = [
#        {'C': [0.1, 1, 10, 100, 1000], 'kernel': ['linear']},
        {'C': [0.1, 1, 10, 100, 1000, 5000], 'gamma': [0.1, 0.001, 0.005, 0.0001], 'kernel': ['rbf']},
    ]
    scores = ['precision','recall','f1','accuracy']
    #run_classifier(svmc, X_train,Y_train,X_test,Y_test,parameters,scores, classes = class_names)
    #print(Y_test[10])
    #print(svmc.predict(X_test[10]))

    classifier = svm.SVC(C=10,kernel='rbf',gamma=0.001)
    classifier.fit(X_train,Y_train)
    pred = classifier.predict(X_test)
    print(pred[10])
    ipdb.set_trace()
    print("##################################################################")
    

    """
    print("Entrenando...")
    svmc = svm.SVC(C=1, gamma = 0.005)
    svmc.fit(X_train,Y_train)

    print("Evaluando")
    pred_test = svmc.predict(X_test)
    
    yg = [class_names[i] for i in Y_test]
    yp = [class_names[i] for i in pred_test]

    print(classification_report(Y_test, pred_test, target_names=class_names))

    cm = confusion_matrix(yg, yp, labels = class_names)
    print(cm)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(cm)
    plt.title('Confusion matrix of the classifier')
    fig.colorbar(cax)
    ax.set_xticklabels([''] + class_names)
    ax.set_yticklabels([''] + class_names)
    plt.xlabel('Predicted')
    plt.ylabel('True')
    plt.show()

    """
    ###############################################################################
    """
    svd = SVD(n_components=2, n_iter = 10)
    svd.fit(X_train)
    X_svd = svd.transform(X_train)


    svd3 = SVD(n_components=3, n_iter = 10)
    svd3.fit(X_train)
    X_svd3 = svd3.transform(X_train)


    #print("Dimesionality reduction LDA...")
    #lda = LDA(n_components = 2)
    #lda.fit(X_train.toarray(), Y_train)
    #X_lda = lda.transform(X_train.toarray())
    
    #lda3 = LDA(n_components = 3)
    #lda3.fit(X_train.toarray(), Y_train)
    #X_lda3 = lda3.transform(X_train.toarray())


    print("Ploting...")

    plot2D(X_svd,Y_train, class_names, 'SVD reduction')
    plot3D(X_svd3,Y_train, class_names, 'SVD reduction 3 comp')

    #plot2D(X_lda,Y_train, class_names, 'LDA reduction')
    #plot3D(X_lda3,Y_train, class_names, 'LDA reduction 3 comp')
    plt.show()
    """

