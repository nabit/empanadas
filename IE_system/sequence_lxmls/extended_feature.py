from sequence_lxmls.id_feature import IDFeatures

import os, sys
import re, string
from nltk.stem.snowball import SpanishStemmer
stemmer = SpanishStemmer()
#from utils import getCareerDict


WINDOW = 3

LONG_WORD_THR = 15
PUNCTUATION = string.punctuation + 'º¡¿ª°'

START = '_START_'
END = '_END_'
START_TAG = '<START>'
END_TAG = '<STOP>'
BR = '**'
RARE = "<RARE>"


# ####################################################
ORT = [

    re.compile(r'^[A-Z]+$'),  # ALL CAPS
    re.compile(r'^[A-Z][a-z]+'),  # CAPITALIZED
    re.compile(r'^[A-Z][a-z]{1,3}\.?$'),  # POSSIBLE ACRONYM
    re.compile(r'[A-Z]'),  # HAS ANY UPPERCASE CHAR
    re.compile(r'^[a-z]+$'),  # ALL LOWERCASE
    re.compile(r'[a-z]'),  # ANY LOWERCASE CHAR
    re.compile(r'^.$'),  # SINGLE CHAR
    re.compile(r'^.{%i,}$' % LONG_WORD_THR),  # LONG WORD >= THRESHOLD
    re.compile(r'^\d+$'),  # ALL NUMBERS
    re.compile(r'^\d+([.,]\d+)?([.,]\d+)?$'),  # NUMBER W/ COMMA OR PERIOD
    re.compile(r'^((S/\.?)|\$)\s*\d+([.,]\d+)?([.,]\d+)?$'),  # IS MONEY
    re.compile(r'^\d+(:\d{2})?[a-zA-Zª]\.?[a-zA-Zª]\.?$'),  # HOUR 9am, 9:00am, 9no, ...
    re.compile(r'^\d+:\d{2}$'),  # HOUR 10:00
    re.compile(r'\d'),  # HAS DIGIT
    re.compile(r'^\w+$'),  # ALPHA-NUMERIC
    re.compile(r'^\w+[%s]+\w+[%s]?$' % (PUNCTUATION, PUNCTUATION)),  # alphanum + PUNCT + alphanum
    re.compile(r'^[xX]+([%s][xX]+)?$' % PUNCTUATION),  # ONLY X's - for emails, websites
    re.compile(r'(www)|(https?)|(com)|(WWW)|(HTTPS?)|(COM)'),  # urls
    re.compile(r'\.+'),  # CONTAINS DOTS
    re.compile(r'-+'),  # CONTAINS HYPENS
    re.compile(r'^[%s]$' % PUNCTUATION),  # IS PUNCTUATION
    re.compile(r'[%s]' % PUNCTUATION),  # HAS PUNCTUATION
]

DEBUG_ORT = [
    'ALL_CAPS',
    'CAPITALIZED',
    'ACRONYM',
    'ANY_UPPERCASE',
    'ALL_LOWERCASE',
    'ANY_LOWERCASE',
    'SINGLE CHAR',
    'LONG WORD',
    'ALL_NUMBERS',
    'NUMBER_COMMA_PERIOD',
    'MONEY',
    'HOUR_ALPHANUM',
    'HOUR_NUM',
    'HAS_DIGIT',
    'ALPHA_NUMERIC',
    'ALPHANUM_PUNCT_ALPHANUM',
    'XXXX',
    'URL',
    'HAS_DOTS',
    'HAS_HYPENS',
    'PUNCTUATION',
    'HAS_PUNCTUATION',

    'OTHERS',
]

NUMBER = [
    re.compile(r'^\d+$'),  # ALL NUMBERS
    re.compile(r'^\d+([.,]\d+)?([.,]\d+)?$'),  # NUMBER W/ COMMA OR PERIOD
    re.compile(r'^((S/\.?)|\$)\s*\d+([.,]\d+)?([.,]\d+)?$'),  # IS MONEY
    re.compile(r'^\d+:\d{2}$'),  # HOUR 10:00
]

# ####################################################



#######################
#### Feature Class
### Extracts features from a labeled corpus (only supported features are extracted
#######################
class ExtendedFeatures(IDFeatures):
    def add_emission_features(self, sequence, pos_current, y_current, features):
        ########    SOLO PALABRA ACTUAL : EMISSION ORIGINAL
        # Get word name from ID.
        x = sequence.x[pos_current]
        word = sequence.sequence_list.x_dict.get_label_name(x)
        stem = stemmer.stem(word.lower())
        if stem not in self.dataset.stem_vocabulary:
            word = RARE

        y_name = sequence.sequence_list.y_dict.get_label_name(y_current)
        
        # Get tag name from ID.
        pos_id = sequence.pos[pos_current]
        pos_tag = sequence.sequence_list.pos_dict.get_label_name(pos_id)
        if self.dataset.pos_dict.get_label_id(pos_tag) == -1:
            pos_tag = RARE
        
        low_word = ''
        if word == RARE:
            low_word = word
        else:
            low_word = word.lower()

        # CURRENT WORD FORM
        feat_name = "id:%s::%s" % (low_word, y_name)
        # Get feature ID from name.
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)

        # CURRENT POS
        feat_name = "pos:%s::%s" % (pos_tag, y_name)
        # Get feature ID from name.
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)

        
        #CURRENT STEM
        stem = low_word
        if low_word != RARE:
            stem = stemmer.stem(low_word)
        feat_name = "stem_id:%s::%s" % (stem, y_name)
        # Get feature ID from name.
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)
        

        # ORTOGRAPHIC FEATURES
        rare_ort = True
        if word in [START,BR,END,RARE]:
            feat_name = word + "::" + y_name
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)
            rare_ort = False
        else:
            for i, pat in enumerate(ORT):
                if pat.search(word):
                    rare_ort = False
                    feat_name = DEBUG_ORT[i] + "::" + y_name
                    feat_id = self.add_feature(feat_name)
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)
        if rare_ort:
            feat_name = "OTHER_ORT::" + y_name
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)

        if low_word != RARE:
            ##Suffixes
            max_suffix = 3
            for i in range(max_suffix):
                if (len(low_word) > i + 1):
                    suffix = low_word[-(i + 1):]
                    # Generate feature name.
                    feat_name = "suffix:%s::%s" % (suffix, y_name)
                    # Get feature ID from name.
                    feat_id = self.add_feature(feat_name)
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)
            ##Prefixes
            max_prefix = 3
            for i in range(max_prefix):
                if (len(low_word) > i + 1):
                    prefix = low_word[:i + 1]
                    # Generate feature name.
                    feat_name = "prefix:%s::%s" % (prefix, y_name)
                    # Get feature ID from name.
                    feat_id = self.add_feature(feat_name)
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)

        return features


    def get_context_features(self, sequence, pos_current, features):
        # CONTEXTUAL FEATURES
        length = len(sequence.y)

        for pos in range(max(0, pos_current-WINDOW), min(pos_current+WINDOW + 1, length)):
            x = sequence.x[pos]
            pos_id = sequence.pos[pos]
            # Get pos name from ID
            pos_tag = sequence.sequence_list.pos_dict.get_label_name(pos_id)
            # Get word name from ID.
            word = sequence.sequence_list.x_dict.get_label_name(x)
            
            if self.dataset.pos_dict.get_label_id(pos_tag) == -1:
                pos_tag = RARE
            stem = stemmer.stem(word.lower())
            if stem not in self.dataset.stem_vocabulary:
                word = RARE
            low_word = ''
            if word == RARE:
                low_word = word
            else:
                low_word = word.lower()


            # WINDOW WORD FORM
            #feat_name = "%i::id:%s::%s" % (pos - pos_current, word, y_name)
            feat_name = "%i::id:%s" % (pos - pos_current, low_word)
            # Get feature ID from name.
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)

            # WINDOW POS_tag
            #feat_name = "%i::pos:%s::%s" % (pos - pos_current, pos_tag, y_name)
            feat_name = "%i::pos:%s" % (pos - pos_current, pos_tag)
            # Get feature ID from name.
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)

            #############################################################################
            # WINDOW STEM
            
            stem = low_word
            if low_word != RARE:
                stem = stemmer.stem(low_word)
            feat_name = "%i::stem_id:%s" % (pos - pos_current, stem)
            # Get feature ID from name.
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)
            #############################################################################
            

            # WINDOW ORTOGRAPHIC FEATURES
            rare_ort = True
            if word in [START,BR,END,RARE]:
                feat_name = "%i::%s" % (pos - pos_current, word)
                feat_id = self.add_feature(feat_name)
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)
                rare_ort = False
            else:
                for i, pat in enumerate(ORT):
                    if pat.search(word):
                        rare_ort = False
                        feat_name = "%i::%s" % (pos - pos_current, DEBUG_ORT[i])
                        feat_id = self.add_feature(feat_name)
                        # Append feature.
                        if feat_id != -1:
                            features.append(feat_id)
            if rare_ort:
                feat_name = "%i::%s" % (pos - pos_current, "OTHER_ORT::")
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)

            """
            if low_word != RARE:
                ##Suffixes
                low_word = word.lower()
                max_suffix = 3
                for i in range(max_suffix):
                    if (len(low_word) > i + 1):
                        suffix = low_word[-(i + 1):]
                        # Generate feature name.
                        feat_name = "%i::suffix::%s" % (pos - pos_current, suffix)
                        # Get feature ID from name.
                        feat_id = self.add_feature(feat_name)
                        # Append feature.
                        if feat_id != -1:
                            features.append(feat_id)
                ##Prefixes
                max_prefix = 3
                for i in range(max_prefix):
                    if (len(low_word) > i + 1):
                        prefix = low_word[:i + 1]
                        # Generate feature name.
                        feat_name = "%i::prefix::%s" % (pos - pos_current, prefix)
                        # Get feature ID from name.
                        feat_id = self.add_feature(feat_name)
                        # Append feature.
                        if feat_id != -1:
                            features.append(feat_id)
            """
        return features