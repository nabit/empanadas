from lcluster import *
from utils import *
from utils_new import *
from sklearn.cluster import KMeans, MiniBatchKMeans, AffinityPropagation, AgglomerativeClustering


"""
if __name__ == "__main__":

    reader = JobDBCorpus()
    dataset = reader.read_sequence_list(target='ALL', START_END_TAGS=False, entities=['CARR', 'AREA', 'FUN', 'REQ', 'JOB'])

    train, test, val = reader.TTCV_data(test_size=0.2, cv_size=0.2)
    train = dataset
    # sys.exit("Leyo data")

    matrix, analize = vectorize_documents(train, classes='pwn', no_entities=['req', 'fun', 'area', 'job'], words_in=['fun', 'req'])

    svd = SVD(n_components=100)
    svd.fit(matrix)
    matrix = svd.transform(matrix)

    print("DOCUMENTS MODELING FINISHED...")
    print("Matrix:", matrix.shape)

    save_matrix(matrix)
    sys.exit("Matrix saved for R as matrix.gzip")

    kmeans = KMeans(n_clusters=20, n_jobs=3, n_init=15, max_iter=500)
    kmeans.fit(matrix)

    affp = AffinityPropagation()
    affp.fit(matrix)

    agglo = AgglomerativeClustering(n_clusters=20, affinity="cosine", linkage="average")
    agglo.fit(matrix)

    '''
    etiquetas = ["cluster "+str(i) for i in range(kmeans.n_clusters)]

    plot2D(matrix, kmeans.labels_, etiquetas)
    plt.show()
    '''

    clusters_affp = create_cluster_list(train, affp.labels_, analize)
    clusters_kmean = create_cluster_list(train, kmeans.labels_, analize)
    clusters_agglo = create_cluster_list(train, agglo.labels_, analize)

    '''
    etiquetas = ["cluster "+str(i) for i in range(affp.cluster_centers_.shape[0])]
    plot2D(matrix, affp.labels_, etiquetas)
    plt.show()
    '''

    DOC_NAME_AFFP = 'AFFP_cluster_' + str(affp.cluster_centers_.shape[0]) + '_features_' + str(svd.n_components) + '.json'
    DOC_NAME_KMEANS = 'KMEANS_cluster_' + str(kmeans.n_clusters) + '_features_' + str(svd.n_components) + '.json'

    # affp_json = cluster_json(clusters_affp, DOC_NAME_AFFP)
    kmeans_json = cluster_json(clusters_kmean, DOC_NAME_KMEANS)
    agglo_json = cluster_json(clusters_agglo, 'clusterAgglo.json')
"""

mode = 'by_doc'
train, test, val = getData(test=0, val=0, tags=['CARR', 'FUN'], mode=mode, filter_empty=True, target='TODO')

vocab = get_vocabulary(train)
dt_list = get_document_term(train, vocab)

export_dt_vocab(dt_list, vocab)