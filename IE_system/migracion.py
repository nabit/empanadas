__author__ = 'ronald'
''' Migracion de SQL a MongoDB de tablas core_description, core_description_jobs & core_details
    LOG
    * 6.07 s para query SELECT * FROM DETAILS + django_mysql_driver
'''

import os, sys
import time
import pymongo
from datetime import date, datetime
from pymongo import MongoClient
import re
import hashlib
import pdb,ipdb


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
WWW_DIR = os.path.join(BASE_DIR, 'www')
SETTINGS_DIR = os.path.join(WWW_DIR, 'project')

UTIL_DIR = os.path.join(BASE_DIR, 'crawler/nlp scripts/')
sys.path.append(UTIL_DIR)
sys.path.append(SETTINGS_DIR)
sys.path.append(WWW_DIR)

#ipdb.set_trace()

os.environ['DJANGO_SETTINGS_MODULE'] = 'project.dev'

#from utilities import *
from core_col.models import Details, Description

purge_patterns = [
  re.compile(ur'[Ii]ndefinid.',      re.UNICODE),
  re.compile(ur'^[Nn]o [Dd]efinid.',  re.UNICODE),
  re.compile(ur'^[Nn]o .specificad.', re.UNICODE),
  re.compile(ur'^[x.]+$', re.UNICODE),
]

def clean_space(text):
    import re
    text = re.sub('[ \r\t\f\v]+', ' ', text)
    text = re.sub('(\n+ *)+', '\n', text)
    return text


def time_usage(func):
    '''
    :param func: funcion a pasar al decorator
    :return: funcion que imprime tiempo ejecutado en funcion argumento :param
     uso
          @time_usage
    '''
    def wrapper(*args, **kwargs):
        beg_ts = time.time()
        func(*args, **kwargs)

        end_ts = time.time()
        print("elapsed time: %f" % (end_ts - beg_ts))
    return wrapper


## MongoDB authentication and connection
uri = 'mongodb://ronald@localhost/'
client = MongoClient()
# conexion a nueva base de datos
db = client.JobDB
# Coleccion de trabajos
job_posts = db.core_col


data = Details.objects.all()

print len(data)
count = 0

for job in data:
    det_pk = job.hash
    hash_descrip_list = job.description_set.all()
    jobD = job.__dict__

    for desc in hash_descrip_list:
        des = clean_space(desc.description)

        if len(des) <= 300:
            hdes = des
        else:
            delta = (len(des) - 250) / 2
            hdes = des[delta: delta + 250]

        # Description Hash
        description_hash = hashlib.sha256()
        description_hash.update(hdes.encode('utf8'))
        description_hash = description_hash.hexdigest()

        descD = desc.__dict__
        item = {'_id':{'details':det_pk ,'description':description_hash}}
        for (k,v) in jobD.items():
            if any([
               k=='hash',
               k=='_state',
               v=='',
               v==u'',
            ]):
                continue

            if k =='date':
                # convert to datetime | mongodb no acepta date
                v = datetime.combine(v,datetime.min.time())
            elif k != 'url':
              match = None
              for pat in purge_patterns:
                try:
                  match = pat.search(v.strip(' '))
                except:
                  ipdb.set_trace()
                if match:
                  break
              if match:
                  continue
            item[k] = v

        for (k,v) in descD.items():
            if any([
               k=='hash',
               k=='_state',
               v=='',
               v==u'',
            ]):
                continue
            if k=='area':
              match = None
              for pat in purge_patterns:
                try:
                  match = pat.search(v.strip(' '))
                except:
                  ipdb.set_trace()
                if match:
                  break
              if match:
                  continue
            item[k] = v

        # agregarlo a coleccion
        try:
            job_posts.insert(item)
        except:
            print "REPETIDO en mongoDB:", sys.exc_info()[0]

        if count % 1000==0:
            print "-> ",count
        count += 1

