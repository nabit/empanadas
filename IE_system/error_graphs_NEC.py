import sequences.confusion_matrix as cm
import matplotlib.pyplot as plt
import numpy as np
import ipdb, pdb

from utils import *

from sklearn.decomposition import TruncatedSVD as SVD
from sklearn import svm, grid_search
from sklearn import cross_validation
from sklearn.learning_curve import learning_curve
from sklearn.learning_curve import validation_curve


def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0,5), score='accuracy'):
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator=estimator, X=X, y=y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes,scoring=score)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    return plt


def plot_validation_curve(estimator, title, X, y, ylim=None, parameter = 'gamma', cv=None,
                        n_jobs=1, param_range=np.linspace(.001, 1.0, 100), score='f1'):
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_scores, test_scores = validation_curve(
        estimator, X, y, param_name=parameter, param_range=param_range, cv=cv, scoring=score, n_jobs=n_jobs)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.xlabel("$\%s$" % parameter)
    plt.ylabel("Score")
    plt.semilogx(param_range, train_scores_mean, label="Training score", color="r")
    plt.fill_between(param_range, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.2, color="r")
    plt.semilogx(param_range, test_scores_mean, label="Cross-validation score",
                 color="g")
    plt.fill_between(param_range, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.2, color="g")
    plt.legend(loc="best")
    plt.show()
    return plt


if __name__ == '__main__':
    X_train,Y_train,X_test,Y_test,chunks_train = getData_NEC()
    train_size = X_train.shape[0]
    classes = max(Y_train) + 1

    print("Dimesionality reduction SVD...")
    svd = SVD(n_components=100, n_iter = 10)
    svd.fit(X_train)

    X_train = svd.transform(X_train)
    X_test = svd.transform(X_test)

    ###############################################################################
    ## PLOT LEARNING AND VALIDATION CURVES
    print("Plotting Learning and Validation curves")
    title = "Learning Curves (SVM, RBF kernel, $\gamma=0.001$)"
    #cv = cross_validation.ShuffleSplit(train_size, n_iter=10, test_size=0.2)
    #cv = cross_validation.KFold(train_size, n_folds=5)
    cv = cross_validation.StratifiedKFold(Y_test, n_folds=classes)
    estimator = svm.SVC(gamma=0.1, C=100)

    #ipdb.set_trace()

    plot_learning_curve(estimator, title, X_train, Y_test, (0.7, 1.01), cv=cv)

    plot_validation_curve(svm.SVC(C=100), title, X_train, Y_test, (0.0, 1.1), parameter='gamma', cv=cv, n_jobs=4, score='accuracy')
    plt.show()
