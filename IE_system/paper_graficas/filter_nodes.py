import json
import ipdb

name2eng = json.loads(open("names_eng.json").read())
# temp = json.loads(open('ident_names.json', 'r').read())
hierarchy = json.loads(open('carreras.json', 'r').read())

circle_data = json.loads(open('adjmatrix.json', 'r').read())

fileByName = {}
# for k, v in temp.items():
#    fileByName[v] = k

if __name__ == '__main__':
    names_ingenieria = []
    treemap = {'name': 'root', 'children': []}
    engineering = {'name': 'Engineering', 'children': []}
    for level1 in hierarchy["children"]:
        if level1["name"] == 'Ingeniería':
            for career in level1["children"]:
                name = career["name"]
                size = career["size"]
                names_ingenieria.append(name)
                engineering['children'].append({'name': name2eng[name], 'size': size})

    treemap['children'].append(engineering)
    open("Treemap_careers_peru.json", 'w').write(json.dumps(treemap, ensure_ascii=False, indent=2).encode('utf-8').
                                          decode('utf-8'))

    filtered_circle = []
    for career in circle_data:
        if career["name"] in names_ingenieria:
            item = dict()
            item["name"] = name2eng[career["name"]]
            item["size"] = career["size"]
            item["imports"] = []
            for imp in career["imports"]:
                if imp["name"] in names_ingenieria:
                    imp['name'] = name2eng[imp['name']]
                    item["imports"].append(imp)

            filtered_circle.append(item)

    open("ing_adjmatrix_peru.json", 'w').write(json.dumps(filtered_circle, ensure_ascii=False, indent=2).encode('utf-8').
                                          decode('utf-8'))
	