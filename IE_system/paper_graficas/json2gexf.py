import json
import ipdb
import numpy as np

name2eng = json.loads(open("names_eng.json").read())
source_graph = json.loads(open("ing_adjmatrix.json").read())


header = '''<?xml version="1.0" encoding="UTF−8"?>
<gexf xmlns="http://www.gexf.net/1.2draft"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema−instance"
      xsi:schemaLocation="http://www.gexf.net/1.2draft
                          http://www.gexf.net/1.2draft/gexf.xsd"
      version="1.2">
	<meta lastmodifieddate="2015−04−23">
		<creator>Ronald C.</creator>
		<description>Major interrelationship graph</description>
	</meta>
	<graph defaultedgetype="undirected">
'''

GEXF = open("graph.gexf",'w')

n = len(source_graph)
matrix = np.zeros((n,n))

idByName = {}
nameById = {}
node_sizes = np.zeros(n)

for i,item in enumerate(source_graph):
	name = item["name"]
	idByName[name] = i
	nameById[i] = name
	node_sizes[i] = item["size"]

for i,item in enumerate(source_graph):
	source = item["name"]
	u = idByName[source]
	for child in item["imports"]:
		target = child["name"]
		v = idByName[target]
		matrix[u,v] = child["weight"]
		matrix[v,u] = child["weight"]

GEXF.write(header)

# ATRIBUTOS
GEXF.write('\t\t<attributes class="node">\n')
GEXF.write('\t\t\t<attribute id="0" title="node-size" type="float" />\n')
GEXF.write('\t\t</attributes>\n')

# Nodes
GEXF.write("\t\t<nodes>\n")
for i in range(n):
	GEXF.write('\t\t\t<node id="%i" label="%s" >\n' % (i,name2eng[nameById[i]]))
	GEXF.write('\t\t\t\t<attvalues>\n')
	GEXF.write('\t\t\t\t\t<attvalue for="0" value="%.1f" />\n' % node_sizes[i])
	GEXF.write('\t\t\t\t</attvalues>\n')
	GEXF.write('\t\t\t</node>\n' )
GEXF.write("\t\t</nodes>\n")


# Edges
n_edg = 0
GEXF.write("\t\t<edges>\n")
for i in range(n):
	for j in range(n):
		if matrix[i,j] != 0 and i<j:
			GEXF.write('\t\t\t<edge id="%i" source="%i" target="%i" weight="%i"/>\n' % (n_edg,i,j,matrix[i,j]) )
			n_edg += 1
GEXF.write("\t\t</edges>\n")

footer = '''	</graph>
</gexf>'''

GEXF.write(footer)