import os,sys
import json
import ipdb

pais = 'peru'
vh_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname( os.path.abspath(__file__) ))), 'crawler/nlp scripts/vector_hash_'+pais)

vh_dir2 = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname( os.path.abspath(__file__) ))), 'crawler/nlp scripts/vector_hash')

ipdb.set_trace()

eng_files = [
  "higiene",
  "industrial",
  "petroleo",
  "sistemas",
  "electrica",
  "industrias_alimentarias",
  "geologica",
  "petroquimica",
  "agricola",
  "textil",
  "zootecnia",
  "arquitectura",
  "agronomia",
  "electronica",
  "metalurgica",
  "mecanica_electrica",
  "civil",
  "mecanica_fluidos",
  "telecomunicaciones",
  "pesquera",
  "minas",
  "informatica",
  "forestal",
  "ambiental",
  "mecatronica",
  "naval",
  "sanitaria",
  "mecanica",
  "estadistica",
  "economica",
  "quimica",
]
eng_files = ['vh_'+w for w in eng_files]

if __name__ == '__main__':
  """
  total = set()
  for root, dirs, filenames in os.walk(vh_dir):
    for f in filenames:
      if f[-1]!='~' and f in eng_files:
        for line in open(os.path.join(vh_dir,f)):
          line = line.strip('\n')
          if line!='':
            total.add(line)

  for root, dirs, filenames in os.walk(vh_dir2):
    for f in filenames:
      if f[-1]!='~' and f in eng_files:
        for line in open(os.path.join(vh_dir,f)):
          line = line.strip('\n')
          if line!='':
            total.add(line)
  print("Total ingenierias:",len(total))
  """

  total_sum = 0
  hierarchy = json.loads(open('Treemap_careers_peru.json', 'r').read())
  for level1 in hierarchy["children"]:
    if level1["name"] == 'Engineering':
      for career in level1["children"]:
        size = career["size"]
        total_sum += size

  print(total_sum)