from utils import *
from utils_new import *
import numpy as np
from nltk.stem.snowball import SpanishStemmer
import unicodedata
import matplotlib.pyplot as plt
import operator

CRAWLER_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'crawler')
IDENTIFIERS_STEM_DIR = os.path.join(CRAWLER_DIR, 'Identifiers_stem')
EXTERNAL_GAZZETER_DIR = os.path.join(os.path.dirname(__file__), 'external_gazetters')

identifiers_dict = dict()
for filename in os.listdir(IDENTIFIERS_STEM_DIR):
    if filename != 'prof_todo.txt' and filename != 'no_tecnico':
        identifiers_dict[filename] = [line.strip() for line in open(os.path.join(IDENTIFIERS_STEM_DIR, filename))]

stemmer = SpanishStemmer()

stopwords = getStopwords()

NO_ENTITIES = ['fun', 'req']
WORDS_IN_ENTITIES = ['fun', 'req', 'job', 'area']

"""
LIBRARY FOR LDA IN R
"""


def get_vocabulary(dataset):
    all_words = dict()
    index = 0
    for sequence in dataset.seq_list:
        for (id_word, id_pos, id_nec) in zip(sequence.x, sequence.pos, sequence.y):
            word = dataset.x_dict.get_label_name(id_word).lower()
            pos = dataset.pos_dict.get_label_name(id_pos).lower()
            nec = dataset.y_dict.get_label_name(id_nec).lower()
            # if pos[0] in ['n', 'r', 'a', 'v', 'z']:
            #print(nec[2:])
            if word not in stopwords and not is_punct.match(word) and '_' not in word and '<' not in word and '>' not in\
                    word and nec[2:] in ['fun', 'req']:
                # word = stemmer.stem(word)  # USANDO STEMMING PARA LAS PALABRAS
                if all_words.get(word) is None:
                    if pos[0] in ['r', 'd', 'p', 'c', 'i', 's', 'f', 'z', 'w']:
                        print(word)
                    all_words[word] = index
                    index += 1
                # else:
                    # all_words[word].add(sequence.file_id)
    return all_words


def get_document_term(dataset, vocab):
    ans = list()
    f_time = True
    for sequence in dataset.seq_list:
        first_time = True
        matrix_doc = np.array([])
        index = 0
        freq = dict()

        for (id_word, id_pos, id_nec) in zip(sequence.x, sequence.pos, sequence.y):
            word = dataset.x_dict.get_label_name(id_word).lower()
            if vocab.get(word):
                # word = stemmer.stem(word)  # USANDO STEMMING PARA LAS PALABRAS
                if freq.get(word) is None:
                    freq[word] = 1
                else:
                    freq[word] += 1
        for word in freq:
            if first_time:
                matrix_doc = np.array([[vocab[word]], [freq[word]]])
                first_time = False
            else:
                matrix_doc = np.hstack([matrix_doc, [[vocab[word]], [freq[word]]]])
        if f_time:
            ans.append(matrix_doc)
            f_time = False
        else:
            ans.append(matrix_doc)

    return ans


def export_dt_vocab(dt_list, vocab):
    with open('DT_DATA_R.dat', 'w') as ff:
        for mat in dt_list:
            for row in mat:
                for num in row:
                    ff.write(str(num) + ' ')
            ff.write('\n')

    with open('VOCAB_DATA_R.dat', 'w') as ff:
        for key in vocab:
            ff.write(key)
            ff.write('\n')


"""
STEMMING FUNCTIONS AND NORMALIZERS
"""


def stemmer_carr(entity):
    entity = " ".join("".join(entity.split('.')).split())
    no_stem = [word.strip('\n') for word in open(os.path.join(EXTERNAL_GAZZETER_DIR, 'no_stemming'))]
    entity_norm = unicodedata.normalize("NFKD", entity)
    entity = " ".join(entity_norm.encode('ascii', 'ignore').decode('ascii').split())
    entity_stemmed = ""
    for word in entity.split():
        if word[:3] == 'ing':
            entity_stemmed += ' ing'
        else:
            found_stem = False
            for no_w_steam in no_stem:
                if no_w_steam in word:
                    entity_stemmed += ' ' + no_w_steam
                    found_stem = True
                    break
            if found_stem is False:
                entity_stemmed += ' ' + stemmer.stem(stemmer.stem(word))
    entity_stemmed = entity_stemmed.strip()

    return entity_stemmed


def match_carr_ident(entity):
    entity_stemmed = stemmer_carr(entity)
    found_ident = False
    ident_ans = ""
    for key in identifiers_dict:
        if found_ident is True:
            break
        for ident in identifiers_dict[key]:
            if entity_stemmed == ident:
                found_ident = True
                ident_ans = key
                break
            elif 'ing ' + entity_stemmed == ident:
                found_ident = True
                ident_ans = key
                break

    # if found_ident is False:

    return ident_ans

"""
FUNCTIONS FOR GETTING DATA OF THE FORM : (NAME, TYPE)
"""
no_wanted = ['ano', 'año', 'hombre', 'mujer', 'experiencia', 'mese', 'minim', 'mínin', 'máx', 'vivir', 'zonas', 'sexo',
             'cv', 'rapid', 'rápid', 'mayor', 'menor', 'edad']


def get_idf_pairs(dataset, dict_name_nec):
    n_doc = len(dataset)
    idf = np.array(np.zeros(len(dict_name_nec)))
    for (idx, pair) in enumerate(dict_name_nec):
        idf[idx] = np.log10(n_doc / len(dict_name_nec[pair]))

    return idf


def get_pairs_data(dataset, no_entities=NO_ENTITIES):
    """
    return a dictionary containing items of the form: (name, type): {id_documents}
    :param dataset: list of sequences
    :return: the dictionary called entities
    """
    chunks = ChunkSet(dataset)
    entities = dict()

    for chunk in chunks.chunk_list:
        entity = ""
        nec = ""
        ndoc = chunk.sequence_id
        for pos in range(chunk.pos, chunk.pos + chunk.length):
            id_word = dataset[ndoc].x[pos]
            word = dataset.x_dict.get_label_name(id_word)
            id_nec = dataset[ndoc].y[pos]
            nec = dataset.y_dict.get_label_name(id_nec)[2:]
            entity += " " + word

        entity = entity.lower().strip()

        nec = nec.lower().strip()
        if nec == 'carr':
            carr_ident = match_carr_ident(entity)
            entity = carr_ident
        if entity == '' or nec in no_entities or ([ww in entity for ww in no_wanted].count(True) is not 0):
            continue
        pair = (entity, nec)
        if entities.get(pair) is None:
            entities[pair] = {ndoc}
        else:
            entities[pair].add(ndoc)

    return entities


def get_pairs_seq(dataset, sequence, no_entities=NO_ENTITIES):
    """
    return a dictionary containing of the form: (name, type) : number of times the pair appears in the sequence
    :param dataset: list of sequences required in order to get the label_name
    :param sequence: the sequence to be processed
    :return: the dictionary called counts
    """
    pos = 0
    open = False
    n = len(sequence.x)
    ne = ''
    counts = dict()
    for (i, w) in enumerate(sequence.x):
        tag = dataset.y_dict.get_label_name(sequence.y[i])
        if len(tag) > 1:
            ne = tag[2:]
        else:
            ne = tag

        prev_ne = ne

        if i > 0:
            prev_tag = dataset.y_dict.get_label_name(sequence.y[i - 1])
            if len(prev_tag) > 1:
                prev_ne = prev_tag[2:]
        if tag.find('B') == 0:
            if open and i > 0:
                posi = pos
                length = i - pos
                entity = ""
                nec = prev_ne
                for pos_i in range(posi, posi + length):
                    id_word = sequence.x[pos_i]
                    word = dataset.x_dict.get_label_name(id_word)
                    entity += " " + word
                entity = entity.lower().strip()
                nec = nec.lower().strip()
                if nec == 'carr':
                    entity = match_carr_ident(entity)
                if entity == '' or nec in no_entities or ([ww in entity for ww in no_wanted].count(True) is not 0):
                    pos = i
                    open = True
                    continue
                if counts.get((entity, nec)) is None:
                    counts[(entity, nec)] = 1
                else:
                    counts[(entity, nec)] += 1

            pos = i
            open = True
        elif tag.find('I') != 0 and open:
            open = False
            posi = pos
            length = i - pos
            entity = ""
            nec = prev_ne
            for pos_i in range(posi, posi + length):
                id_word = sequence.x[pos_i]
                word = dataset.x_dict.get_label_name(id_word)
                entity += " " + word
            entity = entity.lower().strip()
            nec = nec.lower().strip()
            if nec == 'carr':
                entity = match_carr_ident(entity)
            if entity == '' or nec in no_entities or ([ww in entity for ww in no_wanted].count(True) is not 0):
                continue
            if counts.get((entity, nec)) is None:
                counts[(entity, nec)] = 1
            else:
                counts[(entity, nec)] += 1

    if open:
        posi = pos
        length = n - pos
        entity = ""
        nec = ne
        for pos_i in range(posi, posi + length):
            id_word = sequence.x[pos_i]
            word = dataset.x_dict.get_label_name(id_word)
            entity += " " + word
        entity = entity.lower().strip()
        nec = nec.lower().strip()
        if nec == 'carr':
            entity = match_carr_ident(entity)
        if entity != '' and nec not in no_entities and ([ww in entity for ww in no_wanted].count(True) is 0):
            if counts.get((entity, nec)) is None:
                counts[(entity, nec)] = 1
            else:
                counts[(entity, nec)] += 1

    return counts


def vectorize_pair_seq(dataset, sequence, no_entities=NO_ENTITIES,  dict_name_nec=None, idf=None):
    """
    return a vector of scores of the pair(name, type) weigthed with the tfidf score
    :param dataset: the list of sequences
    :param sequence: the sequence to be processed
    :param dict_name_nec: the dict of all the pairs(name, type) in the dataset (optional)
    :param idf: the vector of the idf scores of all the pairs(name, type) in the dataset (optional)
    :return:
    """
    dic_counts = get_pairs_seq(dataset, sequence, no_entities)
    if dict_name_nec is None:
        dict_name_nec = get_pairs_data(dataset, no_entities)

    # tf calculation
    tf = np.array(np.zeros(len(dict_name_nec)))
    for (idx, pair) in enumerate(dict_name_nec):
        if dic_counts.get(pair) is not None:
            tf[idx] = 1 + np.log10(dic_counts[pair])

    # idf calculation
    if idf is not None:
        return tf * idf
    idf = get_idf_pairs(dataset, dict_name_nec)

    return tf * idf

"""
FUNCTIONS FOR GETTING DATA OF THE FORM : KEYWORD VSM
"""


def get_idf_words(dataset, all_words):
    n_doc = len(dataset)
    idf = np.array(np.zeros(len(all_words)))
    for (idx, word) in enumerate(all_words):
        idf[idx] = np.log10(n_doc / len(all_words[word]))

    return idf


def get_words_data(dataset, words_in=WORDS_IN_ENTITIES):
    all_words = dict()
    for sequence in dataset.seq_list:
        for (id_word, id_pos, id_nec) in zip(sequence.x, sequence.pos, sequence.y):
            word = dataset.x_dict.get_label_name(id_word).lower()
            pos = dataset.pos_dict.get_label_name(id_pos).lower()
            nec = dataset.y_dict.get_label_name(id_nec).lower()
            if ([ww in word for ww in no_wanted].count(True) is 0) and pos[0] in ['n', 'r', 'a', 'v', 'z'] and (nec[2:] in words_in):
                word = stemmer.stem(word)  # USANDO STEMMING PARA LAS PALABRAS
                if all_words.get(word) is None:
                    all_words[word] = {sequence.file_id}
                else:
                    all_words[word].add(sequence.file_id)

    return all_words


def get_words_seq(dataset, sequence, words_in=WORDS_IN_ENTITIES):
    counts = dict()
    for (id_word, id_pos, id_nec) in zip(sequence.x, sequence.pos, sequence.y):
        word = dataset.x_dict.get_label_name(id_word).lower()
        pos = dataset.pos_dict.get_label_name(id_pos).lower()
        nec = dataset.y_dict.get_label_name(id_nec).lower()
        if ([ww in word for ww in no_wanted].count(True) is 0) and pos[0] in ['n', 'r', 'a', 'v', 'z'] and (nec[2:] in words_in):
            word = stemmer.stem(word)  # USANDO STEMMING PARA LAS PALABRAS
            if counts.get(word) is None:
                counts[word] = 1
            else:
                counts[word] += 1

    return counts


def vectorize_keyword_seq(dataset, sequence, words_in=WORDS_IN_ENTITIES, all_words=None, idf=None):
    dic_counts = get_words_seq(dataset, sequence, words_in)
    if all_words is None:
        all_words = get_words_data(dataset, words_in)

    # tf calculation
    tf = np.array(np.zeros(len(all_words)))
    for (idx, word) in enumerate(all_words):
        if dic_counts.get(word) is not None:
            tf[idx] = 1 + np.log10(dic_counts[word])

    # idf calculation
    if idf is not None:
        return tf * idf
    idf = get_idf_words(dataset, all_words)
    return tf * idf

"""
FUNCTIONS FOR GETTING DATA OF THE FORM : NAME VSM
"""


def get_idf_names(dataset, all_names):
    n_doc = len(dataset)
    idf = np.array(np.zeros(len(all_names)))
    for (idx, name) in enumerate(all_names):
        idf[idx] = np.log10(n_doc / len(all_names[name]))
    return idf


def get_name_data(dataset, no_entities=NO_ENTITIES):
    chunks = ChunkSet(dataset)
    entities = dict()
    for chunk in chunks.chunk_list:
        entity = ""
        nec = ''
        ndoc = chunk.sequence_id
        for pos in range(chunk.pos, chunk.pos + chunk.length):
            id_word = dataset[ndoc].x[pos]
            word = dataset.x_dict.get_label_name(id_word)
            id_nec = dataset[ndoc].y[pos]
            nec = dataset.y_dict.get_label_name(id_nec)[2:]
            entity += " " + word
        entity = entity.lower().strip()
        nec = nec.lower().strip()

        if nec == 'carr':
            entity = match_carr_ident(entity)

        if entity == '' or nec in no_entities or ([ww in entity for ww in no_wanted].count(True) is not 0):
            continue

        if entities.get(entity) is None:
            entities[entity] = {ndoc}
        else:
            entities[entity].add(ndoc)

    return entities


def get_name_seq(dataset, sequence, no_entities=NO_ENTITIES):
    pos = 0
    open = False
    n = len(sequence.x)
    ne = ''
    counts = dict()
    for (i, w) in enumerate(sequence.x):
        tag = dataset.y_dict.get_label_name(sequence.y[i])
        if len(tag) > 1:
            ne = tag[2:]
        else:
            ne = tag

        prev_ne = ne

        if i > 0:
            prev_tag = dataset.y_dict.get_label_name(sequence.y[i - 1])
            if len(prev_tag) > 1:
                prev_ne = prev_tag[2:]
        if tag.find('B') == 0:
            if open and i > 0:
                posi = pos
                nec = prev_ne
                length = i - pos
                entity = ""
                for pos_id in range(posi, posi + length):
                    id_word = sequence.x[pos_id]
                    word = dataset.x_dict.get_label_name(id_word)
                    entity += " " + word
                entity = entity.lower().strip()
                nec = nec.lower().strip()
                if nec == 'carr':
                    entity = match_carr_ident(entity)
                if entity == '' or nec in no_entities or ([ww in entity for ww in no_wanted].count(True) is not 0):
                    pos = i
                    open = True
                    continue
                if counts.get(entity) is None:
                    counts[entity] = 1
                else:
                    counts[entity] += 1
            pos = i
            open = True
        elif tag.find('I') != 0 and open:
            open = False
            posi = pos
            length = i - pos
            entity = ""
            nec = prev_ne
            for pos_id in range(posi, posi + length):
                id_word = sequence.x[pos_id]
                word = dataset.x_dict.get_label_name(id_word)
                entity += " " + word
            entity = entity.lower().strip()
            nec = nec.lower().strip()
            if nec == 'carr':
                entity = match_carr_ident(entity)
            if entity == '' or nec in no_entities or ([ww in entity for ww in no_wanted].count(True) is not 0):
                continue
            if counts.get(entity) is None:
                counts[entity] = 1
            else:
                counts[entity] += 1

    if open:
        posi = pos
        length = n - pos
        entity = ""
        nec = ne
        for pos_id in range(posi, posi + length):
            id_word = sequence.x[pos_id]
            word = dataset.x_dict.get_label_name(id_word)
            entity += " " + word

        entity = entity.lower().strip()
        nec = nec.lower().strip()

        if nec == 'carr':
            entity = match_carr_ident(entity)
        if entity != '' and nec not in no_entities and ([ww in entity for ww in no_wanted].count(True) is 0):
            if counts.get(entity) is None:
                counts[entity] = 1
            else:
                counts[entity] += 1

    return counts


def vectorize_name_seq(dataset, sequence, no_entities=NO_ENTITIES, all_names=None, idf=None):
    dic_counts = get_name_seq(dataset, sequence, no_entities)
    if all_names is None:
        all_names = get_name_data(dataset, no_entities)

    # tf calculation
    tf = np.array(np.zeros(len(all_names)))
    for (idx, pair) in enumerate(all_names):
        if dic_counts.get(pair) is not None:
            tf[idx] = 1 + np.log10(dic_counts[pair])

    # idf calculation
    if idf is not None:
        return tf * idf
    idf = get_idf_names(dataset, all_names)

    return tf * idf

'''
FINAL FUNCTION FOR MODELING DOCUMENTS
'''


def vectorize_documents(dataset, classes='pnw', no_entities=NO_ENTITIES, words_in=WORDS_IN_ENTITIES):

    all_names = get_name_data(dataset, no_entities)
    all_pairs = get_pairs_data(dataset, no_entities)
    all_words = get_words_data(dataset, words_in)
    first_time = True
    matrix = np.array([])
    idf_name = get_idf_names(dataset, all_names)
    idf_pair = get_idf_pairs(dataset, all_pairs)
    idf_word = get_idf_words(dataset, all_words)
    analize = []

    for idx, sequence in enumerate(dataset.seq_list):
        vector_name = vectorize_name_seq(dataset, sequence, no_entities, all_names, idf_name)
        vector_pair = vectorize_pair_seq(dataset, sequence, no_entities, all_pairs, idf_pair)
        vector_word = vectorize_keyword_seq(dataset, sequence, words_in, all_words, idf_word)
        vector_doc = np.array([])

        if 'p' in classes:
            vector_doc = np.hstack([vector_doc, vector_pair])
        if 'n' in classes:
            vector_doc = np.hstack([vector_doc, vector_name])
        if 'w' in classes:
            vector_doc = np.hstack([vector_doc, vector_word])

        if vector_doc.any():
            analize.append(idx)
        else:
            continue

        if first_time is True:
            matrix = np.hstack([matrix, vector_doc])
            first_time = False
        else:
            matrix = np.vstack([matrix, vector_doc])

    matrix = sparse.csr_matrix(matrix)
    matrix = normalize(matrix, copy=False)

    return matrix, analize

"""
FUNCTION FOR VISUALIZATION
"""


def create_cluster_list(dataset, labels, analize):
    clusters = dict()
    nveces = dict()

    all_entities = get_pairs_data(dataset, no_entities=['fun', 'job', 'area'])

    for (idx, id_cluster) in zip(analize, labels):
        entities = get_pairs_seq(dataset, dataset[idx], no_entities=['fun'])  # , 'job', 'area'])
        if len(entities) == 0:
            continue
        max_freq = max([freq for freq in entities.values()])
        max_freq = 1
        for entity in entities:
            entities[entity] = (entities[entity] / max_freq) # * np.log10((len(train) / len(all_entities[entity])))
            if nveces.get(entity) is None:
                nveces[entity] = {id_cluster: {idx}}
            else:
                dic = nveces[entity]
                if dic.get(id_cluster) is None:
                    dic[id_cluster] = {idx}
                else:
                    dic[id_cluster].add(idx)
                nveces[entity] = dic
        if clusters.get(id_cluster) is None:
            clusters[id_cluster] = entities
        else:
            dic = clusters[id_cluster]
            for entity in entities:
                if dic.get(entity) is None:
                    dic[entity] = entities[entity]
                else:
                    dic[entity] += entities[entity]
            clusters[id_cluster] = dic

    '''
    for idx in clusters:
        cluster = clusters[idx]
        for name in cluster:
            cluster[name] /= len(nveces[name][idx])
        clusters[idx] = cluster
    '''
    return clusters


def plot2d(X, Y, labels, title_text=''):
    n_classes = len(labels)

    plt.figure()
    colors = 'rgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgbmkyrgb' \
             'mkyrgbmkyrgbmky'
    colors = colors[:n_classes]

    markers = 'o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*o^vs+d*' \
              'o^vs+d*o^vs+d*'

    markers = markers[:n_classes]
    for c, m, i, class_name in zip(colors, markers, range(n_classes), labels):
        plt.scatter(X[Y == i, 0], X[Y == i, 1], c=c, marker=m, label=class_name, alpha=0.8)
    plt.legend()
    plt.title(title_text)


def cluster_json(clusters, doc_name=None):
    import json
    data_json = []

    for key in clusters:
        nes = clusters[key]
        sorted_nes = sorted(nes.items(), key=operator.itemgetter(1))
        sorted_nes.reverse()

        data_json.append(dict())
        data_json[key]['cluster'] = "Cluster " + str(key)
        data_json[key]['NE'] = []

        treshold = len(sorted_nes) * 1
        counts_per_class = {'carr': 0, 'req': 0, 'job': 0, 'area': 0, 'fun': 0}
        for (idx, ne) in enumerate(sorted_nes):
            ne_name = ne[0][0]
            ne_type = ne[0][1]
            ne_score = ne[1]
            if idx > treshold:
                break
            if counts_per_class[ne_type] < 15:
                data_json[key]['NE'].append({'name': ne_name, 'class': ne_type, 'w': ne_score})
                counts_per_class[ne_type] += 1
    if doc_name is None:
        return data_json
    with open(doc_name, 'w') as f:
        json.dump(data_json, f, ensure_ascii=False)

    return data_json


def save_matrix(matrix):
    from rpy2.robjects import r
    import pandas.rpy.common as com
    from pandas import DataFrame
    df = DataFrame(matrix)
    df = com.convert_to_r_dataframe(df)
    r.assign('matrix', df)
    r("save(matrix, file='matrix.gzip', compress=TRUE)")


def create_cluster_list_r(dataset, analize, labels_file='CLUSTER_ID_FROM_R.txt'):
    with open(labels_file, 'r') as ff:
        labels = ff.read()
    labels = labels.split()
    labels = [int(num)-1 for num in labels]

    return create_cluster_list(dataset, labels, analize)





