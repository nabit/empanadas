from utils_new import *
import sequences.structured_perceptron as spc
#import sequences.id_feature_bigram as idfc
import sequences.id_feature as idfc
import sequences.extended_feature as exfc

if __name__ == '__main__':
    print("Reading data...")

    mode = 'by_sent'
    #mode = 'by_doc'
    #train,test,val = getData(tags = ['CARR'], mode=mode)
    train,test,val = getData(extended_filter=True, mode=mode)
    
    #ipdb.set_trace()

    print("Building features...")
    #feature_mapper = idfc.IDFeatures(train)
    feature_mapper = exfc.ExtendedFeatures(train,mode=mode)
    feature_mapper.build_features()

    #pdb.set_trace()
    
    print("Init Struc Perceptron")
    epochs = 20
    learning_rate = 1
    sp = spc.StructuredPerceptron(train.x_dict, train.y_dict, feature_mapper, num_epochs=epochs, learning_rate=learning_rate)
    sp.regularization_param = 0

    print("Training...")
    sp.train_supervised_bigram(train)
    #sp.train_supervised(train)

    print("Predicting...")
    #pred_train = sp.viterbi_decode_corpus(train)
    #pred_val   = sp.viterbi_decode_corpus(val)
    #pred_test  = sp.viterbi_decode_corpus(test)

    pred_train = sp.viterbi_decode_corpus_bigram(train)
    pred_val   = sp.viterbi_decode_corpus_bigram(val)
    pred_test  = sp.viterbi_decode_corpus_bigram(test)

    print("Evaluating...")
    eval_train = sp.evaluate_corpus(train, pred_train)
    eval_val = sp.evaluate_corpus(val, pred_val)
    eval_test = sp.evaluate_corpus(test, pred_test)
    print("Structured Perceptron - Features Accuracy Train: %.4f | Val: %.4f | Test: %.4f" % (eval_train, eval_val, eval_test) )

    """
    print("Metrics: Training data")
    cs_train = MyChunkScore(train)
    #cs_train = MyChunkScore(train, mode = 'TODO')     ######
    cs_train.evaluate(train,pred_train)
    print(cs_train)

    print("---------------")

    print("Metrics: Validation data")
    cs_val = MyChunkScore(val)
    #cs_test = MyChunkScore(test, mode = 'TODO')      ######
    cs_val.evaluate(val,pred_val)
    print(cs_val)
    #ipdb.set_trace()
    """
    ###################################################################################################################
    print("=============================")
    print("Confussion Matrix: sapeee!") # DICT HAS TO BE FROM THE SAME PART (TRAIN, VAL OR TEST)
    conf_matrix = cm.build_confusion_matrix(val.seq_list, pred_val, sp.get_num_states())
    for id,_dict in conf_matrix.items():
        name = val.y_dict.get_label_name(id)
        print("::",name)
        for k,v in _dict.items():
            name_in = val.y_dict.get_label_name(k)
            print("  %s: %i" % (name_in,v))

    cm.plot_confusion_bar_graph(conf_matrix, val.y_dict, range(sp.get_num_states()), 'Confusion matrix')

    ###################################################################################################################
    model = 'sp_%i_%s' % (epochs,mode)
    print("Saving model with name:",model)
    saveObject(sp,model)
    
    ###################################################################################################################

    temp = [(v,k) for k,v in train.y_dict.items() if k!=START_TAG and k!=END_TAG]
    temp.sort()
    names_train = [k for v,k in temp]

    temp = [(v,k) for k,v in val.y_dict.items() if k!=START_TAG and k!=END_TAG]
    temp.sort()
    names_val = [k for v,k in temp]

    Y_train = join_data_tags(train.seq_list)
    Y_val = join_data_tags(val.seq_list)

    Y_train_pred = join_data_tags(pred_train)
    Y_val_pred = join_data_tags(pred_val)

    print("Metrics: Training data")
    print(classification_report(Y_train, Y_train_pred, target_names=names_train))

    print("Metrics: Validation data")
    print(classification_report(Y_val, Y_val_pred, target_names=names_val))

    yg = [val.y_dict.get_label_name(id) for id in Y_val]
    yp = [val.y_dict.get_label_name(id) for id in Y_val_pred]

    #cm = confusion_matrix(Y_val, Y_val_pred, labels = names_val)
    cm = confusion_matrix(yg, yp, labels = names_val)
    print(cm)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(cm)
    plt.title('Confusion matrix of the classifier')
    fig.colorbar(cax)
    ax.set_xticklabels([''] + names_val)
    ax.set_yticklabels([''] + names_val)
    plt.xlabel('Predicted')
    plt.ylabel('True')
    plt.show()

    

    print("Debugging!!!")
    #ipdb.set_trace()

    print("Sapee!!!")        
