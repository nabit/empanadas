#!/usr/bin/python
# -*- coding: latin-1 -*-

__author__ = 'ronald'

from sequences.label_dictionary import *
import pdb, ipdb
import os
import unicodedata
import string, re
from nltk.stem.snowball import SpanishStemmer

WINDOW = 5

LONG_WORD_THR = 15
PUNCTUATION = string.punctuation + 'º¡¿ª°'

START = '_START_'
END = '_END_'
START_TAG = '<START>'
END_TAG = '<STOP>'
BR = '**'
RARE = "<RARE>"

# ####################################################
ORT = [

    re.compile(r'^[A-Z]+$'),                        # ALL CAPS
    re.compile(r'^[A-Z][a-z]+'),                    # CAPITALIZED
    re.compile(r'^[A-Z][a-z]{1,2}\.?$'),            # POSSIBLE ACRONYM
    re.compile(r'[A-Z]'),                           # HAS ANY UPPERCASE CHAR
    re.compile(r'^[a-z]+$'),                        # ALL LOWERCASE
    re.compile(r'[a-z]'),                           # ANY LOWERCASE CHAR
    re.compile(r'^[a-zA-Z]$'),                      # SINGLE CHAR
    re.compile(r'^[0-9]$'),                         # SINGLE DIGIT
    re.compile(r'^.{%i,}$' % LONG_WORD_THR),        # LONG WORD >= THRESHOLD

    re.compile(r'^9[0-9]{8}([%s]9[0-9]{8})*$' % PUNCTUATION),  # MOBILE PHONE NUMBER
    re.compile(r'^[0-9]{7}([%s][0-9]{7})*$' % PUNCTUATION),  # OFFICE PHONE NUMBER
    re.compile(r'^\d+$'),                           # ALL NUMBERS
    re.compile(r'^\d+([%s]+\d*)+$' % PUNCTUATION),  # NUMBER PUNCT NUMBER PUNCT?

    re.compile(r'^(([sS$]/\.?)|\$)[0-9a-z]+([.,]\d+)?([.,]\d+)?([.,]\d+)?([.,]\d+)?$'),    # IS MONEY

    re.compile(r'^\d{1,2}(:\d{1,2})?[ªaApP]\.?[mM]\.?$'),      # HOUR 9am, 9:00am, 9no, ...
    re.compile(r'\d{1,2}:\d{1,2}([-/]\d{1,2}:\d{1,2})*'),      # RANGO HORARIO U HORA SIMPLE

    re.compile(r'\d'),  # HAS DIGIT
    re.compile(r'^\w+$'),  # ALPHA-NUMERIC
    re.compile(r'^[a-zA-Z]+[%s]+[a-zA-Z]+([%s]+[a-zA-Z]+)?$' % (PUNCTUATION, PUNCTUATION)),  # alpha + PUNCT + alpha
        
    re.compile(r'^[xX]+([%s][xX]+)?$' % PUNCTUATION),  # ONLY X's - for emails, websites
    re.compile(r'(www)|(https?)|(gmail)|(WWW)|(HTTPS?)|(GMAIL)|(^com$)|(^COM$)|(php)|(PHP)'),  # urls

    re.compile(r'-+'),  # CONTAINS HYPENS
    re.compile(r'^[%s]+$' % PUNCTUATION),  # IS PUNCTUATION
    re.compile(r'[%s]' % PUNCTUATION),  # HAS PUNCTUATION
]

DEBUG_ORT = [
    'ALL_CAPS',
    'CAPITALIZED',
    'ACRONYM',
    'ANY_UPPERCASE',
    'ALL_LOWERCASE',
    'ANY_LOWERCASE',
    'SINGLE CHAR',
    'SINGLE DIGIT',
    'LONG WORD',
    'MOB_NUMBER',
    'OFFICE_NUMBER',    
    'ALL_NUMBERS',
    'NUMBER_PUNCT',
    'MONEY',
    'HOUR',
    'HOUR',
    'HAS_DIGIT',
    'ALPHA_NUMERIC',
    'ALPHA_PUNCT_ALPHA',
    'URL',
    'URL',
    'HAS_HYPENS',
    'PUNCTUATION',
    'HAS_PUNCTUATION',

    'OTHERS',
]

NUMBER = [
    re.compile(r'^9[0-9]{8}([%s]9[0-9]{8})*$' % PUNCTUATION),  # MOBILE PHONE NUMBER
    re.compile(r'^[0-9]{7}([%s][0-9]{7})*$' % PUNCTUATION),  # OFFICE PHONE NUMBER
    re.compile(r'^\d+$'),                           # ALL NUMBERS
    re.compile(r'^\d+([%s]+\d*)+$' % PUNCTUATION),  # NUMBER PUNCT NUMBER PUNCT?
    re.compile(r'^(([sS$]/\.?)|\$)[0-9a-z]+([.,]\d+)?([.,]\d+)?([.,]\d+)?([.,]\d+)?$'),    # IS MONEY
    re.compile(r'^\d{1,2}(:\d{1,2})?[ªaApP]\.?[mM]\.?$'),      # HOUR 9am, 9:00am, 9no, ...
    re.compile(r'\d{1,2}:\d{1,2}([-/]\d{1,2}:\d{1,2})*'),      # RANGO HORARIO U HORA SIMPLE
]

# ####################################################

EXTERNAL_GAZZETER_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)),'external_gazetters')
stemmer = SpanishStemmer()

#################
class IDFeatures:
    def __init__(self, dataset, chunkset):
        '''dataset is a sequence list.'''
        self.feature_dict = LabelDictionary()
        self.feature_list = []

        self.add_features = False
        self.dataset = dataset
        self.chunkset = chunkset

        self.outer_trigger_words= {}
        self.inner_trigger_words= {}

        self.outer_trigger_pos= {}
        self.inner_trigger_pos = {}
        self.FILTER_WORDS = 0.1
        self.FILTER_POS = 0.2

        ## Leer lista no_stemming
        self.no_stem_words = [word.strip('\n') for word in open(os.path.join(EXTERNAL_GAZZETER_DIR,'no_stemming'))]

    def get_num_features(self):
        return len(self.feature_dict)

    def stemAugmented(self,word):
        for pref in self.no_stem_words:
            if word.find(pref) == 0:
                return pref
        return stemmer.stem(word)

    def filter_common(self, _dict):
        '''
        :param _dict:  { NE1 : [], NE2 : [], ...}
        :return:
        '''
        res = {}
        tokens = []
        for tw in _dict.values():
            tokens.extend(tw)
        common = set(tokens)
        for tw in _dict.values():
            common &= set(tw)
        for ne,tw in _dict.items():
            res[ne] = list(set(tw) - common)
        # Otra por si alguno se queda vacio
        common = set(tokens)
        for tw in res.values():
            if tw == []:
                continue
            common &= set(tw)
        new_res = {}
        for ne,tw in res.items():
            new_res[ne] = list(set(tw) - common)
        return new_res

    def filter_unfrequent(self,_dict, threshold):
        res = {}
        for ne,tw_dict in _dict.items():
            mn = min([val for key,val in tw_dict.items()])
            mx = max([val for key,val in tw_dict.items()])
            rg = (mx - mn)
            tw = [trigger for trigger,value in tw_dict.items() if 1.0*value/rg >= threshold]
            res[ne] = tw
        res = self.filter_common(res)
        return res

    def include_gazzeter(self):
        careers_gazzeter = open(os.path.join(EXTERNAL_GAZZETER_DIR,'carreras'),'r')

        for carrera in careers_gazzeter:
            carrera = unicodedata.normalize('NFKD', carrera.lower().strip('\n')).encode('ascii','ignore').decode('unicode_escape')
            self.inner_trigger_words['CARR'].append(carrera)
        self.inner_trigger_words['CARR'] = list(set(self.inner_trigger_words['CARR']))


    def build_features(self):
        '''
        Generic function to build features for a given dataset.
        Iterates through all sentences in the dataset and extracts its features,
        '''

        self.add_features = True

        # Find trigger words & pos
        for chunk in self.chunkset.chunk_list:
            self.update_tw(chunk)

        # Filter unfrequent ones
        self.outer_trigger_words = self.filter_unfrequent(self.outer_trigger_words,self.FILTER_WORDS)
        self.inner_trigger_words = self.filter_unfrequent(self.inner_trigger_words, self.FILTER_WORDS)
        self.include_gazzeter()

        self.inner_trigger_pos = self.filter_unfrequent(self.inner_trigger_pos, self.FILTER_POS)
        self.outer_trigger_pos = self.filter_unfrequent(self.outer_trigger_pos, self.FILTER_POS)
        #ipdb.set_trace()

        for chunk in self.chunkset.chunk_list:
            features = self.get_features(chunk, self.dataset)
        self.add_features = False

        #ipdb.set_trace()

    def get_features(self, chunk, data):
        '''
        :param chunk: Chunk object
        :return: array of features id
        '''
        sequence = data[chunk.sequence_id]
        length = len(sequence.x)
        fin = chunk.pos + chunk.length - 1
        extremos = list(range(max(0,chunk.pos - WINDOW),chunk.pos)) + list(range(fin+1, min(fin + WINDOW + 1,length)))
        features = []

        ################################  EXTERN/CONTEXT FEATURES
        for pos in extremos:
            pos_pivote = 0
            if pos < chunk.pos:
                pos_pivote = chunk.pos
            else:
                pos_pivote = fin

            x = sequence.x[pos]
            word = sequence.sequence_list.x_dict.get_label_name(x)
            stem = stemmer.stem(word.lower())
            if stem not in self.dataset.stem_vocabulary:
                word = RARE
            # Get tag name from ID.
            pos_id = sequence.pos[pos]
            pos_tag = sequence.sequence_list.pos_dict.get_label_name(pos_id)
            if self.dataset.pos_dict.get_label_id(pos_tag) == -1:
                pos_tag = RARE
            # LOW WORD
            low_word = ''
            if word == RARE:
                low_word = word
            else:
                low_word = word.lower()

            # WINDOW WORD FORM + POSITION
            feat_name = "%i::id:%s" % (pos - pos_pivote, low_word)
            feat_id = self.add_feature(feat_name)
            if feat_id != -1:
                features.append(feat_id)

            ## BAG OF WORDS AS EXTERN CONTEXT
            feat_name = "context::id:%s" % (low_word)
            feat_id = self.add_feature(feat_name)
            if feat_id != -1:
                features.append(feat_id)

            # BAG OF POS_tag AS EXTERN CONTEXT
            feat_name = "context::pos:%s" % pos_tag
            feat_id = self.add_feature(feat_name)
            if feat_id != -1:
                features.append(feat_id)
            
            # STEM FEATURES + POSITION      # NO APORTAN!!
            #features = self.get_stem_features(low_word, prefix='outer_stem', pos=pos - pos_pivote, features=features)
            #features = self.get_stem_features(low_word, prefix='outer_stem', pos=-1, features=features)
            
            ## TRIGGER WORDS FEATURES
            features = self.get_trigger_features(word=low_word, prefix='outerTW', _dict=self.outer_trigger_words,
                                                 pos = pos-pos_pivote, features=features)

            ## TRIGGER POS FEATURES
            features = self.get_trigger_features(word=low_word, prefix='outerTP', _dict=self.outer_trigger_pos,
                                                 pos = pos-pos_pivote, features=features)


        ############################################ INNER FEATURES
        chunk_length = fin - chunk.pos + 1
        feat_name = "len:%i" % (chunk_length)
        feat_id = self.add_feature(feat_name)
        if feat_id != -1:
            features.append(feat_id)

        for pos in range(chunk.pos, fin+1):
            x = sequence.x[pos]
            word = sequence.sequence_list.x_dict.get_label_name(x)
            stem = stemmer.stem(word.lower())
            if stem not in self.dataset.stem_vocabulary:
                word = RARE
            # Get tag name from ID.
            pos_id = sequence.pos[pos]
            pos_tag = sequence.sequence_list.pos_dict.get_label_name(pos_id)
            if self.dataset.pos_dict.get_label_id(pos_tag) == -1:
                pos_tag = RARE
            # LOW WORD
            low_word = ''
            if word == RARE:
                low_word = word
            else:
                low_word = word.lower()

            ## BAG OF WORDS INNER
            feat_name = "inner::id:%s" % low_word
            feat_id = self.add_feature(feat_name)
            if feat_id != -1:
                features.append(feat_id)

            # BAG OF POS_tag INNER
            feat_name = "inner::pos:%s" % (pos_tag)
            feat_id = self.add_feature(feat_name)
            if feat_id != -1:
                features.append(feat_id)

            # ORTOGRAPHIC FEATURES + POSITION
            features = self.get_ortographic_features(word,pos=pos - pos_pivote,features=features)

            # BAG OF ORTOGRAPHIC FEATURES       -> NUEVO
            features = self.get_ortographic_features(word,pos=-1,features=features)

            # STEM FEATURES + POSITION
            #features = self.get_stem_features(low_word, prefix='inner_stem', pos=pos - pos_pivote, features=features)
            #features = self.get_stem_features(low_word, prefix='inner_stem', pos=-1, features=features)

            ## TRIGGER WORDS FEATURES
            features = self.get_trigger_features(word='id::'+low_word, prefix='innerTW', _dict=self.inner_trigger_words,
                                                 pos = pos-pos_pivote, features=features)
                # BAG OF TRIGGER WORDS
            features = self.get_trigger_features(word='id::'+low_word, prefix='innerTW', _dict=self.inner_trigger_words,
                                                 pos = -1, features=features)
            ## TRIGGER POS FEATURES
            features = self.get_trigger_features(word='pos::'+pos_tag, prefix='innerTP', _dict=self.inner_trigger_pos,
                                                 pos = pos-pos_pivote, features=features)
                # BAG OF TRIGGER POS
            features = self.get_trigger_features(word='pos::'+pos_tag, prefix='innerTP', _dict=self.inner_trigger_pos,
                                                 pos = -1, features=features)

        ## FIRST WORD FEATURES
        rare_ort = True
        x = sequence.x[chunk.pos]
        first_word = sequence.sequence_list.x_dict.get_label_name(x)
        stem = stemmer.stem(word.lower())
        if stem not in self.dataset.stem_vocabulary:
            first_word = RARE
        # Get tag name from ID.
        pos_id = sequence.pos[chunk.pos]
        pos_tag = sequence.sequence_list.pos_dict.get_label_name(pos_id)
        if self.dataset.pos_dict.get_label_id(pos_tag) == -1:
            pos_tag = RARE
        # LOW WORD
        low_word = ''
        if first_word == RARE:
            low_word = first_word
        else:
            low_word = first_word.lower()

        # POS
        feat_name = "first::pos::%s" % pos_tag
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)

        if low_word != RARE:
            ##Suffixes
            max_suffix = 3
            for i in range(max_suffix):
                if (len(low_word) > i + 1):
                    suffix = low_word[-(i + 1):]
                    # Generate feature name.
                    feat_name = "first::suffix::%s" % (suffix)
                    # Get feature ID from name.
                    feat_id = self.add_feature(feat_name)
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)
            ##Prefixes
            max_prefix = 3
            for i in range(max_prefix):
                if (len(low_word) > i + 1):
                    prefix = low_word[:i + 1]
                    # Generate feature name.
                    feat_name = "first::prefix::%s" % (prefix)
                    # Get feature ID from name.
                    feat_id = self.add_feature(feat_name)
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)

        return features

    def get_ortographic_features(self, word, pos = -1, features = []):
        name_pattern = 'ort::'
        if pos != -1:
            name_pattern = str(pos) + "::" + name_pattern

        rare_ort = True
        if word in [START,BR,END,RARE]:
            feat_name = name_pattern + word
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)
            rare_ort = False
        else:
            for i, pat in enumerate(ORT):
                if pat.search(word):
                    rare_ort = False
                    feat_name = name_pattern  +  DEBUG_ORT[i]
                    feat_id = self.add_feature(feat_name)
                    if feat_id != -1:
                        features.append(feat_id)
        if rare_ort:
            feat_name = name_pattern + "OTHER_ORT"
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)
        return features

    def get_trigger_features(self, word, prefix, _dict, pos=-1, features=[]):
        name_pattern = prefix + '::'
        if pos != -1:
            name_pattern += str(pos) + ':'

        for ne in range(len(self.chunkset.entity_classes)):
            ne_name = self.chunkset.entity_classes.get_label_name(ne)
            word_ascii = unicodedata.normalize('NFKD', word).encode('ascii','ignore').decode('unicode_escape')
            if word not in _dict[ne_name] and word_ascii not in _dict[ne_name]:
                continue
            feat_name = name_pattern + ne_name
            feat_id = self.add_feature(feat_name)
            if feat_id != -1:
                features.append(feat_id)
        return features

    def get_stem_features(self, word, prefix, pos=-1, features=[]):
        name_pattern = prefix + '::'
        if pos != -1:
            name_pattern = str(pos) + "::" + name_pattern
        feat_name = name_pattern + self.stemAugmented(word)
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)
        return features

    def update_tw(self, chunk):
        sequence = self.dataset[chunk.sequence_id]
        length = len(sequence.x)
        fin = chunk.pos + chunk.length - 1
        extremos = list(range(max(0,chunk.pos - WINDOW),chunk.pos)) + list(range(fin+1, min(fin + WINDOW + 1,length)))
        ne = chunk.entity

        ## outer TRIGGER WORD & POS
        for pos in extremos:
            x = sequence.x[pos]
            word = sequence.sequence_list.x_dict.get_label_name(x).lower()
            stem = stemmer.stem(word)
            if stem not in self.dataset.stem_vocabulary:
                word = RARE
            low_word = ''
            if word == RARE:
                low_word = word
            else:
                low_word = word.lower()

            pos_id = sequence.pos[pos]
            pos_tag = sequence.sequence_list.pos_dict.get_label_name(pos_id)
            if self.dataset.pos_dict.get_label_id(pos_tag) == -1:
                pos_tag = RARE

            if any([pos_tag[0] =='s',   # PREPOS
                    pos_tag[0] =='c',   # CONJ
                    pos_tag[0] =='d',   # DETERM
                    ]):
                continue

            if ne not in self.outer_trigger_words:
                self.outer_trigger_words[ne] = {}
            if ne not in self.outer_trigger_pos:
                self.outer_trigger_pos[ne] = {}

            # TRIGGER WORD
            if word not in self.outer_trigger_words[ne]:
                self.outer_trigger_words[ne][word] = 0
            self.outer_trigger_words[ne][word] += 1
            # TRIGGER POS
            if pos_tag not in self.outer_trigger_pos[ne]:
                self.outer_trigger_pos[ne][pos_tag] = 0
            self.outer_trigger_pos[ne][pos_tag] += 1

        ## INNER TRIGGER WORD & POS
        for pos in range(chunk.pos,fin+1):
            x = sequence.x[pos]
            word = sequence.sequence_list.x_dict.get_label_name(x).lower()
            stem = stemmer.stem(word)
            if stem not in self.dataset.stem_vocabulary:
                word = RARE
            low_word = ''
            if word == RARE:
                low_word = word
            else:
                low_word = word.lower()

            pos_id = sequence.pos[pos]
            pos_tag = sequence.sequence_list.pos_dict.get_label_name(pos_id)
            if self.dataset.pos_dict.get_label_id(pos_tag) == -1:
                pos_tag = RARE

            if any([pos_tag[0] =='s',   # PREPOS
                    pos_tag[0] =='c',   # CONJ
                    pos_tag[0] =='d',   # DETERM
                    ]):
                continue

            if ne not in self.inner_trigger_words:
                self.inner_trigger_words[ne] = {}
            if ne not in self.inner_trigger_pos:
                self.inner_trigger_pos[ne] = {}
            # TRIGGER WORD
            if word not in self.inner_trigger_words[ne]:
                self.inner_trigger_words[ne][word] = 0
            self.inner_trigger_words[ne][word] += 1
            # TRIGGER POS
            if pos_tag not in self.inner_trigger_pos[ne]:
                self.inner_trigger_pos[ne][pos_tag] = 0
            self.inner_trigger_pos[ne][pos_tag] += 1

    def add_feature(self, feat_name):
        """
        Builds a dictionary of feature name to feature id
        If we are at test time and we don't have the feature
        we return -1.
        """
        # Check if feature exists and if so, return the feature ID.
        if(feat_name in self.feature_dict):
            return self.feature_dict[feat_name]
        # If 'add_features' is True, add the feature to the feature
        # dictionary and return the feature ID. Otherwise return -1.
        if not self.add_features:
            return -1
        return self.feature_dict.add(feat_name)
