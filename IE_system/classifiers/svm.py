import sys
import numpy as np
import classifiers.linear_classifier as lc
from sequences.my_math_utils import *


START = '_START_'
END = '_END_'
START_TAG = '<START>'
END_TAG = '<STOP>'

BR = '**'
RARE = "<RARE>"


### MULTICLASS SVM CLASSIFIER
class SVM_MULTI(lc.LinearClassifier):

    def __init__(self,nr_epochs = 10, initial_step = 1.0, alpha = 1.0,regularizer = 1.0):
        lc.LinearClassifier.__init__(self)
        self.trained = False
        self.nr_epochs = nr_epochs
        self.params_per_round = []
        self.initial_step = initial_step
        self.alpha = alpha
        self.regularizer = regularizer
        
    def train(self,x,y):
        self.params_per_round = []
        x_orig = x[:,:]
        x = self.add_intercept_term(x)
        nr_x,nr_f = x.shape
        nr_c = np.unique(y).shape[0]
        w = np.zeros((nr_f,nr_c))
        ## Randomize the examples
        perm = np.random.permutation(nr_x)
        #print "Starting Loop"
        t = 0
        for epoch_nr in range(self.nr_epochs):
            objective = 0.0
            for nr in range(nr_x):
                t = t+1
                learning_rate = self.initial_step*np.power(t,-self.alpha)
                inst = perm[nr]
                scores = self.get_scores(x[inst:inst+1,:],w)
                y_true = y[inst:inst+1,0]
                cost_augmented_loss = scores + 1
                cost_augmented_loss[:,y_true] -= 1 
                y_hat = np.argmax(cost_augmented_loss,axis=1).transpose()
                #if(y_true != y_hat):
                objective += 0.5 * self.regularizer * l2norm_squared(w) - scores[:,y_true] + cost_augmented_loss[:,y_hat]
                w = (1 - self.regularizer*learning_rate)*w
                w[:,y_true] += learning_rate*x[inst:inst+1,:].transpose()
                w[:,y_hat] -= learning_rate*x[inst:inst+1,:].transpose()
            self.params_per_round.append(w.copy())   
            self.trained = True
            objective /= nr_x
            y_pred = self.test(x_orig,w)
            acc = self.evaluate(y,y_pred)
            self.trained = False
            print("Epochs: %i Objective: %f" %( epoch_nr,objective))
            print("Epochs: %i Accuracy: %f" %( epoch_nr,acc))
        self.trained = True
        return w


# BINARY CLASS SVM CLASSIFIER
class SVM(lc.LinearClassifier):

    def __init__(self, class_labels, feature_mapper, nr_epochs = 10, initial_step = 1.0, alpha = 1.0,regularizer = 1.0):
        lc.LinearClassifier.__init__(self)
        self.trained = False
        self.num_epochs = nr_epochs
        self.params_per_round = []
        self.parameters = []

        self.initial_step = initial_step
        self.alpha = alpha
        self.regularizer = regularizer
        self.feature_mapper = feature_mapper
        self.class_labels = class_labels        # ne_dict de dataset
        self.num_classes = len(class_labels)



    def train_BIO(self,dataset):
        self.params_per_round = []
        num_examples = dataset.size()
        num_feat = self.feature_mapper.get_num_features()
        num_words = sum([ len(sequence.x)-3 for sequence in dataset.seq_list])

        self.parameters = np.zeros([num_feat + 1, self.num_classes])

        perm = np.random.permutation(num_examples)
        acc = 0
        t = 0
        for epoch in range(self.num_epochs):
            objective = 0.0
            num_errors = 0
            for i in range(num_examples):
                sequence = dataset.seq_list[perm[i]]
                length = len(sequence.y)
                # recorrer secuencia START STOP ACTIVADO
                for pos in range(2,length-1):
                    t = t+1
                    learning_rate = self.initial_step*np.power(t,-self.alpha)
                    y_true = sequence.y[pos]

                    features = self.feature_mapper.get_emission_features(sequence,pos,y_true)
                    features.extend( self.feature_mapper.get_context_features(sequence,pos) )

                    # build X
                    x_m = np.zeros(num_feat)
                    x_m[features] = 1
                    x_m = np.hstack(([1],x_m))

                    scores = self.get_scores(x_m,self.parameters)

                    cost_augmented_loss = scores + 1
                    cost_augmented_loss[y_true] -= 1
                    y_hat = np.argmax(cost_augmented_loss)
                    if y_hat != y_true:
                        num_errors += 1

                    objective += 0.5 * self.regularizer * l2norm_squared(self.parameters) - scores[y_true] + cost_augmented_loss[y_hat]
                    self.parameters = (1 - self.regularizer*learning_rate) * self.parameters
                    self.parameters[:,y_true] += learning_rate * x_m
                    self.parameters[:,y_hat] -= learning_rate * x_m
            #self.params_per_epoch.append(self.parameters.copy())

            objective /= num_words
            acc = 1.0  - 1.0*num_errors / num_words
            print("Epochs: %i Objective: %f" %( epoch,objective))
            print("        %i Accuracy: %f" %( epoch,acc))
        self.trained = True


    def test_corpus(self, dataset):
        '''
        Classifies the points based on a weight vector.
        MODIFICADO PARA SOPORTAR SEQUENCE_LIST OBJECTS
        '''
        num_feat = self.feature_mapper.get_num_features()
        res = []
        for sequence in dataset.seq_list:
            predicted = sequence.copy_sequence()
            pred_y = [START_TAG,START_TAG]
            length = len(sequence.x)
            for pos in range(2,length-1):
                y_true = sequence.y[pos]
                features = self.feature_mapper.get_emission_features(sequence,pos,y_true)
                features.extend( self.feature_mapper.get_context_features(sequence,pos) )
                x_m = np.zeros(num_feat)
                x_m[features] = 1

                y_hat = self.get_label(x_m,self.parameters)
                pred_y.append(y_hat)
            pred_y.append(END_TAG)
            predicted.y = pred_y
            res.append(predicted)
        return res