__author__ = 'ronald'
from sequences.label_dictionary import *
import pdb, ipdb
import string, re



WINDOW = 3

LONG_WORD_THR = 15
PUNCTUATION = string.punctuation + 'º¡¿ª°'

START = '_START_'
END = '_END_'
START_TAG = '<START>'
END_TAG = '<STOP>'
BR = '**'

# ####################################################
ORT = [

    re.compile(r'^[A-Z]+$'),  # ALL CAPS
    re.compile(r'^[A-Z][a-z]+'),  # CAPITALIZED
    re.compile(r'^[A-Z][a-z]{1,3}\.?$'),  # POSSIBLE ACRONYM
    re.compile(r'[A-Z]'),  # HAS ANY UPPERCASE CHAR
    re.compile(r'^[a-z]+$'),  # ALL LOWERCASE
    re.compile(r'[a-z]'),  # ANY LOWERCASE CHAR
    re.compile(r'^.$'),  # SINGLE CHAR
    re.compile(r'^.{%i,}$' % LONG_WORD_THR),  # LONG WORD >= THRESHOLD
    re.compile(r'^\d+$'),  # ALL NUMBERS
    re.compile(r'^\d+([.,]\d+)?([.,]\d+)?$'),  # NUMBER W/ COMMA OR PERIOD
    re.compile(r'^((S/\.?)|\$)\s*\d+([.,]\d+)?([.,]\d+)?$'),  # IS MONEY
    re.compile(r'^\d+(:\d{2})?[a-zA-Zª]\.?[a-zA-Zª]\.?$'),  # HOUR 9am, 9:00am, 9no, ...
    re.compile(r'^\d+:\d{2}$'),  # HOUR 10:00
    re.compile(r'\d'),  # HAS DIGIT
    re.compile(r'^\w+$'),  # ALPHA-NUMERIC
    re.compile(r'^\w+[%s]+\w+[%s]?$' % (PUNCTUATION, PUNCTUATION)),  # alphanum + PUNCT + alphanum
    re.compile(r'^[xX]+([%s][xX]+)?$' % PUNCTUATION),  # ONLY X's - for emails, websites
    re.compile(r'(www)|(https?)|(com)|(WWW)|(HTTPS?)|(COM)'),  # urls
    re.compile(r'\.+'),  # CONTAINS DOTS
    re.compile(r'-+'),  # CONTAINS HYPENS
    re.compile(r'^[%s]$' % PUNCTUATION),  # IS PUNCTUATION
    re.compile(r'[%s]' % PUNCTUATION),  # HAS PUNCTUATION
]

DEBUG_ORT = [
    'ALL_CAPS',
    'CAPITALIZED',
    'ACRONYM',
    'ANY_UPPERCASE',
    'ALL_LOWERCASE',
    'ANY_LOWERCASE',
    'SINGLE CHAR',
    'LONG WORD',
    'ALL_NUMBERS',
    'NUMBER_COMMA_PERIOD',
    'MONEY',
    'HOUR_ALPHANUM',
    'HOUR_NUM',
    'HAS_DIGIT',
    'ALPHA_NUMERIC',
    'ALPHANUM_PUNCT_ALPHANUM',
    'XXXX',
    'URL',
    'HAS_DOTS',
    'HAS_HYPENS',
    'PUNCTUATION',
    'HAS_PUNCTUATION',

    'OTHERS',
]

NUMBER = [
    re.compile(r'^\d+$'),  # ALL NUMBERS
    re.compile(r'^\d+([.,]\d+)?([.,]\d+)?$'),  # NUMBER W/ COMMA OR PERIOD
    re.compile(r'^((S/\.?)|\$)\s*\d+([.,]\d+)?([.,]\d+)?$'),  # IS MONEY
    re.compile(r'^\d+:\d{2}$'),  # HOUR 10:00
]

# ####################################################


#################
class IDFeatures:
    '''
        Base class to extract features from a particular dataset.

        feature_dic --> Dictionary of all existing features maps feature_name (string) --> feature_id (int)
        feature_names --> List of feature names. Each position is the feature_id and contains the feature name
        nr_feats --> Total number of features
        feature_list --> For each sentence in the corpus contains a pair of node feature and edge features
        dataset --> The original dataset for which the features were extracted

        Caches (for speedup):
        initial_state_feature_cache -->
        node_feature_cache -->
        edge_feature_cache -->
        final_state_feature_cache -->
    '''

    def __init__(self, dataset, order = 3):
        '''dataset is a sequence list.'''
        self.feature_dict = LabelDictionary()
        self.feature_list = []

        self.add_features = False
        self.dataset = dataset

        # look back IBO
        self.order = order

        #Speed up
        self.context_feature_cache = {}
        self.word_feature_cache = {}


    def get_num_features(self):
        return len(self.feature_dict)


    def build_features(self):
        '''
        Generic function to build features for a given dataset.
        Iterates through all sentences in the dataset and extracts its features,
        saving the node/edge features in feature list.
        '''
        self.add_features = True
        for sequence in self.dataset.seq_list:
            word_features = []
            word_features = self.generate_word_features(sequence,word_features)
            self.feature_list.append(word_features)
        self.add_features = False

        #ipdb.set_trace()


    def generate_word_features(self, sequence, features):
        length = len(sequence.y)
        sequence_features = []
        for pos in range(length):
            features = self.get_emission_features(sequence, pos,0)
            features.extend( self.get_context_features(sequence, pos) )
            sequence_features.append(features)
        return sequence_features


    def get_emission_features(self, sequence, pos, y):
        """
        # CON INFORMACION DE TAG ACTUAL
        ########    SOLO PALABRA ACTUAL : EMISSION ORIGINAL
        features = []
        # Get tag name from ID.
        x = sequence.x[pos]
        pos_id = sequence.pos[pos]
        y_name = self.dataset.y_dict.get_label_name(y)
        pos_tag = self.dataset.pos_dict.get_label_name(pos_id)
        # Get word name from ID.
        word = self.dataset.x_dict.get_label_name(x)
        low_word = word.lower()

        # CURRENT WORD FORM
        feat_name = "id:%s::%s" % (low_word, y_name)
        # Get feature ID from name.
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)

        # CURRENT POS
        feat_name = "pos:%s::%s" % (pos_tag, y_name)
        # Get feature ID from name.
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)

        # ORTOGRAPHIC FEATURES
        rare_ort = True
        if word == START or word == BR or word == END:
            feat_name = word + "::" + y_name
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)
            rare_ort = False
        else:
            for i, pat in enumerate(ORT):
                if pat.search(word):
                    rare_ort = False
                    feat_name = DEBUG_ORT[i] + "::" + y_name
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)
        if rare_ort:
            feat_name = "OTHER_ORT::" + y_name
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)

        ##Suffixes
        max_suffix = 3
        for i in range(max_suffix):
            if (len(low_word) > i + 1):
                suffix = low_word[-(i + 1):]
                # Generate feature name.
                feat_name = "suffix:%s::%s" % (suffix, y_name)
                # Get feature ID from name.
                feat_id = self.add_feature(feat_name)
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)

        ##Prefixes
        max_prefix = 3
        for i in range(max_prefix):
            if (len(low_word) > i + 1):
                prefix = low_word[:i + 1]
                # Generate feature name.
                feat_name = "prefix:%s::%s" % (prefix, y_name)
                # Get feature ID from name.
                feat_id = self.add_feature(feat_name)
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)
        """

        # SIN INFORMACION DE TAG ACTUAL
        ########    SOLO PALABRA ACTUAL : EMISSION ORIGINAL
        features = []
        # Get tag name from ID.
        x = sequence.x[pos]
        pos_id = sequence.pos[pos]
        y_name = self.dataset.y_dict.get_label_name(y)
        pos_tag = self.dataset.pos_dict.get_label_name(pos_id)
        # Get word name from ID.
        word = self.dataset.x_dict.get_label_name(x)
        low_word = word.lower()

        ##Suffixes
        max_suffix = 3
        for i in range(max_suffix):
            if (len(low_word) > i + 1):
                suffix = low_word[-(i + 1):]
                # Generate feature name.
                feat_name = "suffix:%s" % (suffix)
                # Get feature ID from name.
                feat_id = self.add_feature(feat_name)
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)

        ##Prefixes
        max_prefix = 3
        for i in range(max_prefix):
            if (len(low_word) > i + 1):
                prefix = low_word[:i + 1]
                # Generate feature name.
                feat_name = "prefix:%s" % (prefix)
                # Get feature ID from name.
                feat_id = self.add_feature(feat_name)
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)
        return features


    def get_context_features(self,sequence,pos_current):
        ## LOCAL TRANSITION
        local = []
        features = []
        for pos in range(max(0,pos_current - self.order),pos_current):
            y_name = self.dataset.y_dict.get_label_name(sequence.y[pos])
            local.append(y_name)

        feat_name = "trans::" + '::'.join(local)
        # Get feature ID from name.
        feat_id = self.add_feature(feat_name)
        # Append feature.
        if feat_id != -1:
            features.append(feat_id)

        # CONTEXTUAL FEATURES
        length = len(sequence.x)

        for pos in range(max(0, pos_current-WINDOW), min(pos_current + WINDOW + 1, length)):
            x = sequence.x[pos]
            pos_id = sequence.pos[pos]
            # Get pos name from ID
            pos_tag = self.dataset.pos_dict.get_label_name(pos_id)
            # Get word name from ID.
            word = self.dataset.x_dict.get_label_name(x)
            low_word = word.lower()

            # WINDOW WORD FORM
            #feat_name = "%i::id:%s::%s" % (pos - pos_current, word, y_name)
            feat_name = "%i::id:%s" % (pos - pos_current, low_word)
            # Get feature ID from name.
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)

            # WINDOW POS_tag
            #feat_name = "%i::pos:%s::%s" % (pos - pos_current, pos_tag, y_name)
            feat_name = "%i::pos:%s" % (pos - pos_current, pos_tag)
            # Get feature ID from name.
            feat_id = self.add_feature(feat_name)
            # Append feature.
            if feat_id != -1:
                features.append(feat_id)

            # WINDOW ORTOGRAPHIC FEATURES
            rare_ort = True
            if word == START or word == BR or word == END:
                feat_name = "%i::%s" % (pos - pos_current, word)
                feat_id = self.add_feature(feat_name)
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)
                rare_ort = False
            else:
                for i, pat in enumerate(ORT):
                    if pat.search(word):
                        rare_ort = False
                        feat_name = "%i::%s" % (pos - pos_current, DEBUG_ORT[i])
                        # Append feature.
                        if feat_id != -1:
                            features.append(feat_id)
            if rare_ort:
                feat_name = "%i::%s" % (pos - pos_current, "OTHER_ORT::")
                # Append feature.
                if feat_id != -1:
                    features.append(feat_id)

            """
            ##Suffixes
            low_word = word.lower()
            max_suffix = 3
            for i in range(max_suffix):
                if (len(low_word) > i + 1):
                    suffix = low_word[-(i + 1):]
                    # Generate feature name.
                    feat_name = "%i::suffix::%s" % (pos - pos_current, suffix)
                    # Get feature ID from name.
                    feat_id = self.add_feature(feat_name)
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)

            ##Prefixes
            max_prefix = 3
            for i in range(max_prefix):
                if (len(low_word) > i + 1):
                    prefix = low_word[:i + 1]
                    # Generate feature name.
                    feat_name = "%i::prefix::%s" % (pos - pos_current, prefix)
                    # Get feature ID from name.
                    feat_id = self.add_feature(feat_name)
                    # Append feature.
                    if feat_id != -1:
                        features.append(feat_id)
            """
        return features


    def add_feature(self, feat_name):
        """
        Builds a dictionary of feature name to feature id
        If we are at test time and we don't have the feature
        we return -1.
        """
        # Check if feature exists and if so, return the feature ID.
        if(feat_name in self.feature_dict):
            return self.feature_dict[feat_name]
        # If 'add_features' is True, add the feature to the feature
        # dictionary and return the feature ID. Otherwise return -1.
        if not self.add_features:
            return -1
        return self.feature_dict.add(feat_name)
