"""
Preprocessing part for Coherence Metrics
"""

import os,sys
import numpy as np
import ipdb
from utils_new import stemAugmented as stemmer
from utils_new import getStopwords, is_punct

from multiprocessing import Pool


BASE_DIR = os.path.dirname( os.path.dirname(os.path.abspath(__file__)) )
wiki_dir = os.path.join( os.path.dirname(BASE_DIR),'wikicorpus' )

stopwords = getStopwords()
MODE = 'doc'
#MODE = 'sent'

def process_doc(filename):
	n_tokens = 0
	tokens = []
	res = []
	for line in open(os.path.join(wiki_dir,filename),encoding='utf8',errors='replace'):
		line = line.strip('\n')
		if any([ '<doc' in line,
				 '</doc' in line
				]):
			continue
		if MODE=='doc':
			if line=='':
				continue
			word = line.split(' ')[0].lower()	#take word
			if not is_punct.match(word) and word not in stopwords:
				stem = stemmer(word)
				tokens.append(stem)
		elif MODE=='sent':
			if line=='':
				res.append( tuple([' '.join(tokens),len(tokens)]) )
				tokens = []
				continue
			word = line.split(' ')[0].lower()	#take word
			if not is_punct.match(word) and word not in stopwords:
				stem = stemmer(word)
				tokens.append(stem)
		"""
		mode paragraph not implemented, wikicorpus tokenized by sentences, not paragraphs
		"""
	if MODE=='doc':
		n_tokens = len(tokens)
		res = tuple([' '.join(tokens),n_tokens])
	elif MODE=='sent':
		res = np.array(res)
	return res


if __name__ == '__main__':
	wiki_files = list(os.walk(wiki_dir))[0][2]
	
	#pool = Pool(processes=1)
	#res = pool.map(process_doc,wiki_files[:1])
	res = process_doc(wiki_files[0])

	ipdb.set_trace()

	if MODE=='sent':
		n,m = res.shape
		res = np.resize(res,(1,n*m))
