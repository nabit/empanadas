from utils_new import getData, filter_names, START, END, TAGS

BORDERS = [START,END]
mode = 'by_sent'
train,_,_ = getData(tags = TAGS + ['FUN'], mode=mode)

output = open('input_liang_' + mode,'w')

for sent in train.seq_list:
	n = len(sent.x)
	x_dict = sent.sequence_list.x_dict
	words = []
	for i in range(n):
		word = x_dict.get_label_name(sent.x[i])
		if word not in filter_names and word not in BORDERS:
			word = word.lower()
		words.append(word)
	text = ' '.join(words) + '\n'
	output.write(text)
