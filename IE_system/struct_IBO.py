import nltk
import pdb, ipdb
import numpy as np
from sklearn.cross_validation import train_test_split
from nltk.corpus.reader.util import concat

from utils import *
from metrics import *
import re
import string

import sequences.structured_perceptron as spc
#import sequences.id_feature_bigram as idfc
import sequences.id_feature as idfc
import sequences.extended_feature as exfc

import sequences.confusion_matrix as cm
import matplotlib.pyplot as plt


if __name__ == '__main__':
    reader = JobDBCorpus()
    dataset = reader.read_sequence_list(target='BIO')
    #dataset = reader.read_sequence_list(target='TODO')

    #np.seterr(all='print')

    train_seq,test,val = reader.TTCV_data(test_size=0.2,cv_size=0.2)
    #train_seq = reader.trimTrain(train_seq, percentage=0.48)
    #feature_mapper = idfc.IDFeatures(train_seq)
    feature_mapper = exfc.ExtendedFeatures(train_seq)
    feature_mapper.build_features()

    print("Perceptron Exercise")
    sp = spc.StructuredPerceptron(reader.word_dict, reader.ne_dict, feature_mapper)
    sp.num_epochs = 10
    #sp.learning_rate = 1
    sp.regularization_param = 0

    #sp.train_supervised_bigram(train_seq)
    sp.train_supervised(train_seq)

    pred_train = sp.viterbi_decode_corpus(train_seq)
    pred_val = sp.viterbi_decode_corpus(val)
    pred_test = sp.viterbi_decode_corpus(test)

    #pred_train = sp.viterbi_decode_corpus_bigram(train_seq)
    #pred_val = sp.viterbi_decode_corpus_bigram(val)
    #pred_test = sp.viterbi_decode_corpus_bigram(test)

    eval_train = sp.evaluate_corpus(train_seq, pred_train)
    eval_val = sp.evaluate_corpus(val, pred_val)
    eval_test = sp.evaluate_corpus(test, pred_test)
    print("Structured Perceptron - Features Accuracy Train: %.4f | Test: %.4f" % (eval_train, eval_test) )

    print("Metrics: Training data")
    cs_train = MyChunkScore(dataset)
    #cs_train = MyChunkScore(dataset, mode = 'TODO')     ######
    cs_train.evaluate(train_seq,pred_train)
    print(cs_train)

    print("---------------")

    print("Metrics: Testing data")
    cs_test = MyChunkScore(dataset)
    #cs_test = MyChunkScore(dataset, mode = 'TODO')      ######
    cs_test.evaluate(test,pred_test)
    print(cs_test)
    #ipdb.set_trace()


    print("Confussion Matrix: sapeee!")
    confusion_matrix = cm.build_confusion_matrix(val.seq_list, pred_val, sp.get_num_states())

    pdb.set_trace()

    cm.plot_confusion_bar_graph(confusion_matrix, dataset.y_dict, range(sp.get_num_states()), 'Confusion matrix')
    plt.show()

