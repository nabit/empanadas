import os,sys

path_utils = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(path_utils)

from utils_new import *
from utils_pos import *
import ipdb

import sequences.structured_perceptron as spc
import ext2 as exfc2
from sklearn.metrics import classification_report, accuracy_score
from sklearn.metrics import confusion_matrix


if __name__ == '__main__':
    print("Reading data...")
    mode = 'by_sent'
    train,test,val = getData(test=0.1, val=0.1, mode=mode)

    print("Building features...")
    feature_mapper = exfc2.ExtendedFeatures(train,mode=mode)
    feature_mapper.build_features()

    #ipdb.set_trace()
    
    print("Init Struc Perceptron")
    epochs = 5
    learning_rate = 1
    reg = 0
    sp = spc.StructuredPerceptron(  train.x_dict, train.y_dict, feature_mapper,
                                    num_epochs=epochs, learning_rate=learning_rate,
                                    reg_param=reg)
    print("Training...")
    sp.train_supervised_bigram(train)

    print("Predicting...")
    print("::Training...")
    pred_train = sp.viterbi_decode_corpus_bigram(train)
    print("::Validation...")
    pred_val   = sp.viterbi_decode_corpus_bigram(val)
    print("::Testing...")
    pred_test  = sp.viterbi_decode_corpus_bigram(test)

    print("Evaluating...")
    eval_train = sp.evaluate_corpus(train, pred_train)
    eval_val   = sp.evaluate_corpus(val, pred_val, DEBUG=False)
    eval_test  = sp.evaluate_corpus(test, pred_test)
    print("Structured Perceptron - Features Accuracy Train: %.4f | Val: %.4f | Test: %.4f" % (eval_train, eval_val, eval_test) )

    print()
    print("Metrics: Training data")
    cs_train = MyChunkScore(train)
    cs_train.evaluate(train,pred_train)
    print(cs_train)

    print()

    print("Metrics: Testing data")
    cs_test = MyChunkScore(test)
    cs_test.evaluate(test,pred_test)
    print(cs_test)

    print()

    print("Metrics: Validation data")
    cs_val = MyChunkScore(val)
    cs_val.evaluate(val,pred_val)
    print(cs_val)

    

    ###################################################################################################################
    print("==========================================================")
    print("Confussion Matrix: sapeee!") # DICT HAS TO BE FROM THE SAME PART (TRAIN, VAL OR TEST)
    conf_matrix = cm.build_confusion_matrix(val.seq_list, pred_val, sp.get_num_states())
    for id,_dict in conf_matrix.items():
        name = val.y_dict.get_label_name(id)
        print("::",name)
        for k,v in _dict.items():
            name_in = val.y_dict.get_label_name(k)
            print("  %s: %i" % (name_in,v))

    

    ###################################################################################################################
    model = 'sp_%i_%s' % (epochs,mode)
    print("Saving model with name:",model)
    saveObject(sp,model)
    
    ###################################################################################################################

    temp = [(v,k) for k,v in train.y_dict.items()]# if k!=START_TAG and k!=END_TAG]
    temp.sort()
    names_train = [k for v,k in temp]

    temp = [(v,k) for k,v in val.y_dict.items()]# if k!=START_TAG and k!=END_TAG]
    temp.sort()
    names_val = [k for v,k in temp]

    Y_train = join_data_tags(train.seq_list)
    Y_val   = join_data_tags(val.seq_list)

    Y_train_pred = join_data_tags(pred_train)
    Y_val_pred   = join_data_tags(pred_val)

    print("Metrics: Training data")
    print(classification_report(Y_train, Y_train_pred, target_names=names_train))

    print("Metrics: Validation data")
    print(classification_report(Y_val  , Y_val_pred  , target_names=names_val))

    cm.plot_confusion_bar_graph(conf_matrix, val.y_dict, range(sp.get_num_states()), 'Confusion matrix')