import nltk
import os,sys
import pdb, ipdb
import pickle
import numpy as np
from sklearn.cross_validation import train_test_split
from nltk.corpus.reader.util import concat

#UTILS PATH
path_utils = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(path_utils)

from utils_new import *

from sequences.label_dictionary import *
from sequence import *
from sequence_list import *

import numpy as np
from metrics import *
import re, string, unicodedata

from multiprocessing import Pool
from functools import partial
from datetime import datetime

def makeCorpus_wrapper(params=[]):
    raw_data,train_idx,mode,START_END_TAGS,stem_vocab,pos_vocab,filter_empty,extended_filter = params# [raw_data,train_idx,mode,START_END_TAGS,stem_vocab,pos_vocab,filter_empty,extended_filter]

    def makeCorpus(data,idx,mode='by_sent',START_END_TAGS=True, stem_vocab=[], pos_vocab=[], filter_empty=False, extended_filter = True):
        # El output sale ya filtrado para cualquier corpus
        corpus = Corpus()
        if mode=='by_doc':
            corpus.ne_dict.add(BR)
        if START_END_TAGS:
            corpus.word_dict.add(START)
            corpus.word_dict.add(END)
            corpus.ne_dict.add(START_TAG)
            corpus.ne_dict.add(END_TAG)
            corpus.pos_dict.add(START_TAG)
            corpus.pos_dict.add(END_TAG)
            
        seq_list = []
        file_ids = []
        name_folder = ''
        corpus.stem_vocabulary = stem_vocab

        for id in idx:
            doc = data[id]
            if id < 400:
                name_folder='car_tag_' + str(id+1)
            else:
                name_folder='random_' + str(id-400+1)

            if mode=='by_sent':
                for i,sent in enumerate(doc):
                    sent_x = []
                    sent_pos = []
                    if START_END_TAGS:
                        sent_x   = [START    , START]
                        sent_pos = [START_TAG, START_TAG]
                    for tup in sent:
                        x,pos,y = tup
                        if extended_filter:
                            x = permanentFilter(x)
                        stem = stemAugmented(x.lower())
                        if x not in filter_names and stem_vocab!=[] and stem not in stem_vocab:
                            x = assignFilterTag(x)
                        if pos_vocab!=[] and pos not in pos_vocab:
                            pos = NOUN
                        #if y != 'O':    # si oracion no solo tiene O
                        #    empty_sample = False
                        if x not in corpus.word_dict:
                            corpus.word_dict.add(x)
                        if pos not in corpus.pos_dict:
                            corpus.pos_dict.add(pos)
                        sent_x.append(x)
                        sent_pos.append(pos)
                    if START_END_TAGS:
                        sent_x.append(END)
                        sent_pos.append(END_TAG)
                    seq_list.append([sent_x,sent_pos])
                    file_ids.append(name_folder)
            else:
                sent_x = []
                sent_pos = []
                if START_END_TAGS:
                    sent_x   = [START    , START]
                    sent_pos = [START_TAG, START_TAG]
                for tup in doc:
                    x,pos,y = tup
                    if x != BR:
                        if extended_filter:
                            x = permanentFilter(x)
                        stem = stemAugmented(x.lower())
                        if x not in filter_names and stem_vocab!=[] and stem not in stem_vocab:
                            x = assignFilterTag(x)
                    if pos_vocab!=[] and pos not in pos_vocab:
                        pos = NOUN
                    if x not in corpus.word_dict:
                        corpus.word_dict.add(x)
                    if pos not in corpus.pos_dict:
                        corpus.pos_dict.add(pos)
                    sent_x.append(x)
                    sent_pos.append(pos)
                if sent_x[-1] == BR:
                    sent_x.pop()
                    sent_pos.pop()
                if START_END_TAGS:
                    sent_x.append(END)
                    sent_pos.append(END_TAG)

                seq_list.append([sent_x,sent_pos])
                file_ids.append(name_folder)
        
        sequence_list = SequenceList(corpus.word_dict, corpus.pos_dict,corpus.stem_vocabulary)

        for i,(x,pos) in enumerate(seq_list):
            sequence_list.add_sequence(x, pos,file_ids[i])
        return sequence_list
    return makeCorpus(data=raw_data, idx=train_idx, mode=mode, START_END_TAGS=START_END_TAGS,
                        stem_vocab=stem_vocab, pos_vocab=pos_vocab, filter_empty=filter_empty, extended_filter=extended_filter)



def getData(test=0.1, val=0.1, mode='by_sent', tags=TAGS, target='BIO', START_END_TAGS=True, extended_filter=True, filter_empty=False):
    print(":: Reading parameters POStagger")
    print("Mode  : ",mode)
    print("Extended filter tags: ",extended_filter)
    print("=======================================")
    startTime = datetime.now()

    idx = range(800)
    val_size = 0
    if val+test != 0.0:
        val_size = val/(val+test)

    #train_idx,temp = train_test_split(idx ,test_size = test+val, random_state=RANDOM_STATE)
    train_idx,temp = custom_train_test(test_size = test+val)
    try:
        test_idx,val_idx = train_test_split(temp,test_size = val_size, random_state=RANDOM_STATE)
    except:
        test_idx = val_idx = []

    raw_data = reader(tags=tags, mode=mode,path=careers_dir, target=target)
    temp     = reader(tags=tags, mode=mode,path=random_dir , target=target)
    raw_data.extend(temp)

    stem_vocab,pos_vocab = make_word_counts(raw_data,train_idx,mode=mode, extended_filter=extended_filter)

    param1 = (tags,mode,careers_dir,target)
    param2 = (tags,mode,random_dir ,target)

    ## PARALELIZANDO MAKE_CORPUS: SAPEEE!
    makeCorpus_wrapper.parallel = parallel_function(makeCorpus_wrapper)
    train_param = [raw_data,train_idx,mode,START_END_TAGS,stem_vocab,pos_vocab,filter_empty,extended_filter]
    test_param  = [raw_data,test_idx ,mode,START_END_TAGS,stem_vocab,pos_vocab,False,extended_filter]
    val_param   = [raw_data,val_idx ,mode,START_END_TAGS,stem_vocab,pos_vocab,False,extended_filter]
    

    train,test,val = makeCorpus_wrapper.parallel([train_param,test_param,val_param])

    print("Dataset analysis:")
    print(":: Training set")
    print("Size training set: ",len(train.seq_list))
    print_tag_proportion(train)
    print("---------------------------------------")
    print(":: Testing set")
    print("Size testing set: ",len(test.seq_list))
    print_tag_proportion(test)
    print("---------------------------------------")
    print(":: Validation set")
    print("Size validation set: ",len(val.seq_list))
    print_tag_proportion(val)
    print("---------------------------------------")
    print("Execution time: ",datetime.now()-startTime)
    print("=======================================")

    return train,test,val
