#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from django import template
from django.conf import settings
from django.utils.html import strip_tags
import unicodedata

import pdb

register = template.Library()

@register.filter(name = 'open_content')
def open_content(value):
    f = value.read().decode('utf8')
    value.seek(0,0)     # hace que el puntero de lectura vuelva al inicio del archivo
        
    return f

@register.filter(name = 'split_title')
def split_title(title):
    res = {}
    pref1 = 'Requerimientos más solicitados en la carrera universitaria de '.decode('utf8')
    idx = title.find(pref1)
    pref2 = 'Requerimientos más solicitados en '.decode('utf8')
    idx2 = title.find(pref2)

    if idx == 0:
        res['pref'] = pref1
        res['carrera'] = title[idx + len(pref1):]
    elif idx2 == 0:
        res['pref'] = pref2
        res['carrera'] = title[idx + len(pref2):]
    else:
        res['pref'] = title
        res['carrera'] = ''

    return res

@register.filter(name = 'range')
def split_title(number):
    return range(number)

@register.filter(name = 'dict_key')
def dict_key(_dict,key):
    return _dict[key]