from django.shortcuts import render_to_response,redirect,render,get_object_or_404
from django.template import RequestContext, loader
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

### Setup para que DATETIME reconozca meses en espanol
import locale
import pdb
locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')

from blog.models import Post

class Render_allBlogs(TemplateView):
    template_name = 'blog/allBlogs.html'
    todo = Post.objects.all().order_by('-date')
    posts = {}
    month_names = []
    for i,p in enumerate(todo):
        month = p.date.strftime("%B")
        if month not in posts:
            posts[month] = []
        posts[month].append(p)
        #pdb.set_trace()

        if i>0 and month == month_names[-1]:
            continue
        month_names.append(month)



    def dispatch(self, *args, **kwargs):
        return super(Render_allBlogs, self).dispatch(*args, **kwargs)

    def get_context_data(self,  **kwargs):
        ctx = {'posts': self.todo,
               'posts_by_month':self.posts,
               'month_names' : self.month_names
               }
        return ctx

def make_static_path(value):
    return str(value.lstrip('/media/'))

class Render_Post(Render_allBlogs):
    template_name = 'blog/post.html'
        
    def dispatch(self, *args, **kwargs):
        return super(Render_Post, self).dispatch(*args, **kwargs)

    def get_context_data(self, sku,  **kwargs):
        post = get_object_or_404(Post,sku=sku)
        tags = []
        url_post = ''
        try:
            post = get_object_or_404(Post,sku=sku)
            tags = [tag.strip(" ") for tag in post.tags.split(":")]
            url_post = make_static_path(post.content_file.url)
            
        except:
            tags = []
            
        ctx= {'query_post': post,
              'posts': self.todo,
              'posts_by_month':self.posts,
              'month_names' : self.month_names,
              'tags': tags,
              'url_post':url_post}
        return ctx
