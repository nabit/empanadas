##encoding:utf-8

from django.db import models
from django.template.defaultfilters import slugify
import os,sys
from django.conf import settings


class Post(models.Model):
    title         = models.CharField(max_length = 300, blank = False)
    title_url     = models.CharField(max_length = 160, blank = False)
    sku           = models.SlugField(max_length = 160, null=True, blank=True)  
    content_file  = models.FileField(upload_to='blog', blank = True)
    tags          = models.CharField(max_length = 100) # separador de tags ':'
    date          = models.DateField();
    list_img      = models.ImageField(upload_to='blog/images',blank=True)
    
    def __unicode__(self):
        return u"%s - %s" % (self.title, self.sku)

    def url(self):
        return u"/peru/blog/post/%s" % self.sku
    
    def save(self,*args, **kwargs):
        '''Overwrite save method to send message_from_visitor signal,
        only when confirm was ticked on'''
        # siempre chanca el sku con el nuevo titulo
        self.sku = slugify(self.title_url)
        post_folder = os.path.join(settings.SITE_ROOT,'media/blog/images/' + self.sku)
        if not os.path.exists(post_folder):
            os.makedirs(post_folder)
        super(Post, self).save(*args, **kwargs)