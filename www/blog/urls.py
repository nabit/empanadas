from django.conf.urls import patterns, url
from django.views.generic.base import RedirectView

from blog import views

urlpatterns = patterns(
    'blog.views',
    url(r'^blog/$',RedirectView.as_view(url = '/peru/blog')),
    url(r'^peru/blog/$',views.Render_allBlogs.as_view(),name = 'blog-list'),
    url(r'^peru/blog/post/(?P<sku>.*)$', views.Render_Post.as_view(), name='post'),
)