# Django settings for project project.
import os
import djcelery
from django.core.urlresolvers import reverse_lazy

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
    ('Ronald C', 'roncardenasacosta@gmail.com'),
    ('Kevin B', 'ksbm190493@gmail.com'),
)

MANAGERS = ADMINS


#Base directory of the project
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SITE_ROOT = os.path.dirname(os.path.realpath(__file__ + '/../'))
#WEB_ENDPOINT = os.environ.get('JOBT_WEB_ENDPOINT').rstrip('/') if os.environ.get('JOBT_WEB_ENDPOINT') else None
#SESSION_COOKIE_DOMAIN = os.environ.get('JOBT_COOKIE_DOMAIN')
#CSRF_COOKIE_DOMAIN = os.environ.get('JOBT_COOKIE_DOMAIN')
#SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
#SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'



# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Lima'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'es-es'

LANGUAGES = (
    ('en', 'English'),
    ('en-gb', 'British English'),
    ('fr', 'French'),
    ('es-es','Spanish'),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True
LOCALE_PATHS = (
    os.path.join(SITE_ROOT, 'settings/locale'),
)

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

ROOT_URLCONF = 'project.urls'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(SITE_ROOT, 'static').replace('\\', '/')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

ADMIN_MEDIA_PREFIX = '/media/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(SITE_ROOT,'website/static'),
    #os.path.join(SITE_ROOT,'account/static'),
    os.path.join(SITE_ROOT,'proyecto/static'),
    os.path.join(SITE_ROOT,'browser/static'),
    #os.path.join(SITE_ROOT,'browser/templates'),
    #os.path.join(SITE_ROOT,'app_linkedin/static'),
    #os.path.join(SITE_ROOT,'media'),
)

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
FONTS_DIR = (
    os.path.join(SITE_ROOT, 'website/fonts'),
)


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = (
    os.path.join(SITE_ROOT,'website/templates'),
    os.path.join(SITE_ROOT,'browser/templates'),
    #os.path.join(SITE_ROOT,'account/templates'),
    os.path.join(SITE_ROOT,'media'),
    #mail verification
    os.path.join(SITE_ROOT,'zohoverify'),
    # future
    os.path.join(SITE_ROOT,'proyecto/templates'),
    os.path.join(SITE_ROOT,'app_linkedin/templates'),
)


# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wsgi.application'


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap3',
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'social.apps.django_app.default',
    # Uncomment the next line to enable the admin:
    #'djcelery',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

JOBT_APPS = (
    'core',
#    'core_ve',
#    'core_urug',
#    'core_repdom',
#    'core_panama',
#    'core_mx',
#    'core_ecuador',
#    'core_cr',
    'core_cl',
#    'core_ar',
    'core_col',
#    'core_brazil',
#    'core_usa',
#    'account',
    'website',
    'blog',
    'img_gallery',

    'proyecto',
    'app_linkedin',
    'browser',
)
INSTALLED_APPS += JOBT_APPS

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'social.apps.django_app.context_processors.backends',
    'social.apps.django_app.context_processors.login_redirect',
)

# Auticacion personalizada con modelo de usuarios propio // verificar nombre en backends.py
AUTHENTICATION_BACKENDS = (
    #'account.backends.MyBackend',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'django.contrib.auth.backends.ModelBackend',
    'social.backends.linkedin.LinkedinOAuth',
)

#AUTH_USER_MODEL = 'account.User'
#LOGIN_URL = reverse_lazy('signin')
#LOGOUT_URL = reverse_lazy('signout')
LOGIN_REDIRECT_URL = '/acuerdo-uso-de-datos/'
#LOGGED_IN_HOME = 'root'
#ACCOUNT_SESSION_VALIDITY_HOURS = 1 * 24 * 30  # 30 days
#ACCOUNT_ACTIVATION_DAYS = 2
#ACCOUNT_ACTIVATED = 'ALREADY_ACTIVED'

# En desarrolo no se necesitara activacion
#ACCOUNT_ACTIVATION_REQUIRED = False


SOCIAL_AUTH_LINKEDIN_KEY = '758ikjlknawm9l' # linkedin calls this the "API Key"
SOCIAL_AUTH_LINKEDIN_SECRET = 'R0R68EncvRDOaien' # "Secret Key"

SOCIAL_AUTH_LINKEDIN_SCOPE = ['r_basicprofile','r_fullprofile']

SOCIAL_AUTH_LINKEDIN_FORCE_PROFILE_LANGUAGE = 'es-ES'

# Field selectors determine what data social_auth will get from linkedin
SOCIAL_AUTH_LINKEDIN_FIELD_SELECTORS = [
    'headline',
    'industry',
    'summary',
    'specialties',
    'positions',
    'projects',
    'educations',
    'skills',
    'languages',
    'summary',
    'three-past-positions',
    'three-current-positions',
    'courses',
    'certifications',
    'interests',
    'recommendations-received',
    'job-bookmarks',
    'publications',
    'patents',
]

SOCIAL_AUTH_LINKEDIN_EXTRA_DATA = [('id', 'id'),
                       ('first-name', 'first_name'),
                       ('last-name', 'last_name'),] + [
                           (field, field.replace('-', '_'), True)
                           for field in SOCIAL_AUTH_LINKEDIN_FIELD_SELECTORS
                       ]

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',

    'app_linkedin.tasks.tweak',  # <--- set the import-path to the function
)
