from project.base import *


DEBUG = True
TASTYPIE_FULL_DEBUG = True
#SOUTH_AUTO_FREEZE_APP = True

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'mr2w$!h2x7cbsdr#15(p-o%cuo)!y%*3sq+0--bgrxkz0)+-+1'

# Site information
SITE_URL = "https://www.empleatron.com"
SITE_NAME = "Empleatron"

# Local database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'localhost',
        'NAME': 'JobDB',
        'USER': 'root',
        'PASSWORD': '1234',
        'PORT':'',
    }
}


MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    'debug_toolbar',
#    'south',
)

ALLOWED_HOSTS = ['*']

# Configuracion de debug-toolbar para WSGI
DEBUG_TOOLBAR_PATCH_SETTINGS = False

# Importar de locale

try:
    from project.local import *
except ImportError:
    pass


