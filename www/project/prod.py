from base import *


DEBUG = False

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'mr2w$!h2x7cbsdr#15(p-o%cuo)!y%*3sq+0--bgrxkz0)+-+1'


# production database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'localhost',
        'NAME': 'JobDB',
        'USER': 'root',
        'PASSWORD': '1234',
        'PORT':'',
    }
}

#ALLOWED_HOSTS = ['www.empleatron.com']
ALLOWED_HOSTS = ['*']
# Site information
SITE_URL = "https://www.empleatron.com"
SITE_NAME = "Empleatron App"

try:
    from project.local import *
except ImportError:
    pass

