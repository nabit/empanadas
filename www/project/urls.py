from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from django.utils.translation import ugettext_lazy as _

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

handler400 = 'website.views.error_404'
handler403 = 'website.views.error_403'
handler404 = 'website.views.error_404'
handler500 = 'website.views.error_500'

from website import views as website_views

from proyecto import views as proy_mec_views
from app_linkedin import views as app_linkedin_views

from browser import views as browser_views

urlpatterns = patterns('',
  url(r'^admin/', include(admin.site.urls)),
  #url(r'',include('account.urls')),
  url(r'',include('blog.urls')),

  url(r'^$',RedirectView.as_view(url = '/peru')),
  url(r'^peru/$',website_views.Index.as_view(),name='index'),
  url(r'^peru/nosotros/$',website_views.Quienes_somos.as_view(),name='quienes_somos'),
  url(r'^peru/panorama/$',website_views.Panorama,name='panorama'),
  url(r'^peru/panorama/demanda-laboral/$',website_views.treemap,name='treemap'),
  url(r'^peru/panorama/interrelacion-carreras',website_views.circleplot,name='circleplot'),

  url(r'^peru/soluciones/',website_views.Servicios,name='servicios'),
  url(r'^peru/contacto/',website_views.Contacto,name='contacto'),

  url(r'^peru/graficas/$',website_views.Graficas.as_view(),name='graficas'),
  url(r'^peru/graficas/barchart_careers$',website_views.barchart,name='barchart'),

  url(r'^proyecto-mecatronico/$', proy_mec_views.clustering),

  # verificacion de servidor smtp
  url(r'^zohoverify/verifyforzoho.html',website_views.mail_verify),

  ## NEW_DATA URLS
  url(r'^peru/nueva-data/circleplot$',website_views.new_data_circleplot),
  url(r'^peru/nueva-data/treemap$',website_views.new_data_treemap),

  ## PERU_ING
  url(r'^peru/engineering/circleplot$',website_views.peru_ing_circleplot),
  url(r'^peru/engineering/treemap$'   ,website_views.peru_ing_treemap),


  ## PERU | TOPIC BROWSER
  url(r'^peru/browser/$'                           ,browser_views.topic_presence ,name='topic-presence'),
  url(r'^peru/browser/topic-list/$'                ,browser_views.topic_list     ,name='topic-list'),
  url(r'^peru/browser/topic-graph/$'               ,browser_views.topic_graph    ,name='topic-graph'),
  url(r'^peru/browser/term-list/$'                 ,browser_views.term_list      ,name='term-list'),
  url(r'^peru/browser/term-graph/$'                ,browser_views.term_graph     ,name='term-graph'),
  url(r'^peru/browser/doc-graph/$'                 ,browser_views.doc_graph      ,name='doc-graph'),
  url(r'^peru/browser/docs/(?P<title>[a-z0-9]+)/$' ,browser_views.doc            ,name='doc'),
  url(r'^peru/browser/terms/(?P<term>.*)/$'        ,browser_views.term           ,name='term'),
  url(r'^peru/browser/topics/(?P<topic>.*)/$'      ,browser_views.topic          ,name='topic'),


  ## CHILE_ING
  url(r'^chile/engineering/circleplot$',website_views.chile_ing_circleplot),
  url(r'^chile/engineering/treemap$'   ,website_views.chile_ing_treemap),

  ## CHILE_ING
  url(r'^colombia/engineering/circleplot$',website_views.col_ing_circleplot),
  url(r'^colombia/engineering/treemap$'   ,website_views.col_ing_treemap),


  ## SOCIAL DATA
  url(r'^acuerdo-uso-de-datos/$', app_linkedin_views.home, name='social-home'),
  url(r'^acuerdo-uso-de-datos/', include('social.apps.django_app.urls', namespace='social')),
  url(r'^acuerdo-uso-de-datos/', include('django.contrib.auth.urls', namespace='auth')),
)

from django.conf import settings
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )
