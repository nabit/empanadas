from django.shortcuts import render_to_response
from django.template.context import RequestContext

import pdb

# Create your views here.

def home(request):
	#pdb.set_trace()

	context = RequestContext(request,
                           {'user': request.user})
	return render_to_response('index.html',
                             context_instance=context)