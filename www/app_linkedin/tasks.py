#from models import Profile
import pdb
import pymongo
from pymongo import MongoClient

## MongoDB authentication and connection
client = MongoClient()
db = client.JobDB
# Coleccion de trabajos
job_posts = db.profiles


def tweak(user, *args, **kwargs):
    username = user.username
    data = dict(kwargs['social'].extra_data)
    data['username'] = username
    del data['access_token']
    del data['first_name']
    del data['last_name']
    id = data['id']
    del data['id']
    data['_id'] = id

    try:
        data['educations'] = data['educations'].get('values',[])
    except:
        pass
    
    try:
        data['projects']  = data['projects'].get('values',[])
    except:
        pass
    
    try:
        data['positions'] = data['positions'].get('values',[])
    except:
        pass

    try:
        data['specialties'] = data['specialties'].get('values',[])
    except:
        pass

    try:
        skills = [skill['skill']['name'] for skill in data['skills'].get('values',[])]
        data['skills'] = skills
    except:
        pass

    try:
        languages = [lang['language']['name'] for lang in data['languages'].get('values',[])]
        data['languages'] = languages
    except:
        pass

    try:
        job_posts.insert(data)
    except:
        print("duplicated key")