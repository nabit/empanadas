from django.shortcuts import render_to_response,redirect,render


# Create your views here.

def topic_presence(request):
	return render_to_response('browser/browse/topic-presence.html')

def topic_list(request):
	return render_to_response('browser/browse/topic-list.html')

def topic_graph(request):
	return render_to_response('browser/browse/topic-graph.html')

def term_list(request):
	return render_to_response('browser/browse/term-list.html')

def term_graph(request):
	return render_to_response('browser/browse/term-graph.html')

def doc_graph(request):
	return render_to_response('browser/browse/doc-graph.html')

def doc(request,title):
	return render_to_response('browser/docs/%s.html' % title)

def term(request,term):
	return render_to_response('browser/terms/%s.html' % term)

def topic(request,topic):
	return render_to_response('browser/topics/%s.html' % topic)