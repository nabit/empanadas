import sys, os
import json
import hashlib
import datetime

#util_folder = '/home/ronald/project/crawler/nlp scripts'
util_folder = '/home/bmk/Workspace/Django/jobadvisor-app/crawler/nlp scripts'
sys.path.append(util_folder)

os.environ['DJANGO_SETTINGS_MODULE'] = 'project.dev'

MESES = {'enero':'01','febrero':'02', 'marzo':'03','abril':'04',
         'mayo':'05','junio':'06','julio':'07','agosto':'08',
         'setiembre':'09','septiembre':'09','octubre':'10','noviembre':'11','diciembre':'12',
         1:'enero',2:'febrero', 3:'marzo',4:'abril',
         5:'mayo',6:'junio',7:'julio',8:'agosto',
         9:'setiembre',9:'septiembre',10:'octubre',11:'noviembre',12:'diciembre'
         }


from utilities import *
from core.models import Details, Description
from django.core.exceptions import ObjectDoesNotExist


def loadInitialJSON(json_name):
    """ input: nombre de archivo json a leer
        output: lista de jobs, formato json & modelo django
        description: obtiene data de modelo pasado y arma json en baso a modelo actual
    """
    core = json.loads(open(json_name).read())
    # Dict a imprimir en nuevo json
    new_core = []
    # arma data total
    data = {}
    # separando tablas, uniques
    details = {}
    descrip = {}
    
    for job in core:
        if job['model'] == 'core.details':
            insert(data,job['pk'],job['fields'])
            # agregar pais
            if 'peru' not in data[job['pk']]['place']: 
                data[job['pk']]['place'] = data[job['pk']]['place'] + ', peru'
        else:
            pk_job = job['fields']['job']
            if pk_job in data:
                insert(data[pk_job],'requirements',job['fields']['requirements'])
                insert(data[pk_job],'functions',job['fields']['functions'])
                insert(data[pk_job],'area',job['fields']['area'])
    
    
    # para cada trabajo, armar diccionario p/c tablas
    for (pk,fields) in data.iteritems():
        if 'requirements' not in fields:
            continue
    
        title   = fields['title']
        date    = fields['date']
        place   = fields['place']
        company = fields['company']
        req     = fields['requirements']
        func    = fields['functions']
        
        date = date.split(' ')
        
        # Caso especial de data pasada (core.json)
        if len(date)<3:
            temp = []
            if 'ero' in date[0]:
                temp.append(date[0][:2])
                temp.append('enero')
                temp.append(date[1])
            date = temp
        
        
        date = str(datetime.date(int(date[2]), int(MESES[date[1]]), int(date[0]) ))
        
        req.replace('\r','')
        req = ' '.join([w for w in req.split(' ') if len(w)>0])
        
        func.replace('\r','')
        func = ' '.join([w for w in func.split(' ') if len(w)>0])
        
        detail_hash = hashlib.sha256()
        detail_hash.update((title + company + place + date).encode('latin1'))
        detail_hash = detail_hash.hexdigest()
        
        str_req = ""
        if len(req) > 100:
            str_req = req[:50] + req[-50:]
        else:
            str_req = req
        
        str_func = ""
        if len(func) > 100:
            str_func = func[:50] + func[-50:]
        else:
            str_func = func
        
        description_hash = hashlib.sha256()
        description_hash.update((str_req + str_func).encode('latin1'))
        description_hash = description_hash.hexdigest()
        
        if detail_hash not in details:
            temp = dict(fields)
            del temp['hash']
            del temp['requirements']
            del temp['functions']
            del temp['area']
            # DateField
            temp['date'] = date
            details[detail_hash] = temp
        
        if description_hash not in descrip:
            temp = {}
            temp['requirements'] = req
            temp['functions']    = func
            temp['area']         = fields['area']
            temp['job']          = [detail_hash]
            descrip[description_hash] = temp
        else:
            descrip[description_hash]['job'].append(detail_hash)    
    
    
    # Armar tablas
    for (pk,fields) in details.iteritems():
        job = {}
        job['pk'] = pk
        job['model'] = 'core.details'
        job['fields'] = fields
        new_core.append(job)
    
    for (pk,fields) in descrip.iteritems():
        job = {}
        job['pk'] = pk
        job['model'] = 'core.description'
        job['fields'] = fields
        new_core.append(job)    
    return new_core


def updateDataBase(json_name):
    """ input  : nombre de json a cargar
        descrip: arma modelo de Job y actualiza DB
    """
    core = json.loads(open(json_name).read())
    
    # arma data total, sirve para mapear Descrip a partir de Job
    data = {}
    
    for job in core:
        if job['model'] == 'core.details':
            insert(data,job['pk'],job['fields'])
        else:
            pk_job = job['fields']['job']
            if pk_job in data:
                insert(data[pk_job],'requirements',job['fields']['requirements'])
                insert(data[pk_job],'functions',job['fields']['functions'])
                insert(data[pk_job],'area',job['fields']['area'])
    
    # para cada trabajo, hallar hash y actualizar DB
    for (pk,fields) in data.iteritems():
        if 'requirements' not in fields:
            continue
    
        title   = fields['title']
        date    = fields['date']
        place   = fields['place']
        company = fields['company']
        req     = fields['requirements']
        func    = fields['functions']
        area    = fields['area']
        
        salary  = fields['salary']
        url     = fields['url']
        typeJob = fields['typeJob']
        
        #################################################################################
        ### Cambios a campos de tablas
        # agregar pais
        if 'peru' not in place: 
            place = place + ', peru'
        
        ## Cambios a fechas
        date = date.split(' ')
        
        # Caso especial de data pasada (core.json)
        if len(date)<3:
            temp = []
            if 'ero' in date[0]:
                temp.append(date[0][:2])
                temp.append('enero')
                temp.append(date[1])
            date = temp
            
        date = datetime.date(int(date[2]), int(MESES[date[1]]), int(date[0]) )
        
        req.replace('\r','')
        req = ' '.join([w for w in req.split(' ') if len(w)>0])
        
        func.replace('\r','')
        func = ' '.join([w for w in func.split(' ') if len(w)>0])
        ######################################################################################


        ############################################
        ## Ver si se repite aviso
        # Query: mismos detalles, dif de 14 dias
        repeated_jobs = Details.objects.filter(
                            company = company
                        ).filter(
                            date__gte = date - datetime.timedelta(days=14)
                        ).exclude(
                            date__gte = date + datetime.timedelta(days=1)
                        ).filter(
                            place = place
                        ).filter(
                            title = title
                        )
        # Query: misma descripcion
        repeated = False
        for job in repeated_jobs:
            descs = job.description_set.filter(
                        functions = func
                    ).filter(
                        requirements = req
                    )
            if len(descs) > 0:
                repeated = True
                break
        
        if repeated:
            continue                    # Se repite, continuar
        
        #############################################################################################################################################################################################################################################
        ## Make PK as hash
        # escoge 50 primeras y 50 ultimas para hacer el hash de descripcion
        str_req = ""
        if len(req) > 100:
            str_req = req[:50] + req[-50:]
        else:
            str_req = req
        
        str_func = ""
        if len(func) > 100:
            str_func = func[:50] + func[-50:]
        else:
            str_func = func
        
        # Details Hash
        detail_hash = hashlib.sha256()
        detail_hash.update((title + company + place + str(date)).encode('latin1'))
        detail_hash = detail_hash.hexdigest()
        
        # Description Hash
        description_hash = hashlib.sha256()
        description_hash.update((str_req + str_func).encode('latin1'))
        description_hash = description_hash.hexdigest()
        
        ###############################################################################################
        ## Agregando el Job a la DB
        # Primero se crea el q no tiene M2M field
        details = Details(hash  = detail_hash, title   = title  , company = company, date = date,
                          place = place      , typeJob = typeJob, salary  = salary , url  = url)
        details.save()
        
        # Buscar si descripcion ya existe
        descrip = None
        try:
            # existe, Agregar relacion descripcion - detalles
            descrip = Description.objects.get(pk = description_hash)
        except ObjectDoesNotExist:
            # Crear description entry
            descrip = Description(hash = description_hash, requirements = req,
                                  functions = func, area = area)
            descrip.save()
        descrip.job.add(details)
        
def unifiedJSON(json_name):
    """ input: nombre de archivo json a leer
        output: json con modelos actual
        description: une funciones y requqrimientos y arma json en base a modelo actual
    """
    core = json.loads(open(json_name).read())
    # Dict a imprimir en nuevo json
    new_core = []
    # arma data total
    data = {}
    # separando tablas, uniques
    details = {}
    descrip = {}

    for job in core:
        if job['model'] == 'core.details':
            new_core.append(job)
        else:
            newJob = {}
            newJob['pk'] = job['pk']
            newJob['model'] = job['model']
            newJob['fields'] = {}
            newJob['fields']['job'] = job['fields']['job']
            newJob['fields']['area'] = job['fields']['area']
            if job['fields']['functions'] == 'no especificado':
                newJob['fields']['description'] = u'funciones:\n' + u'requerimientos:\n' + job['fields']['requirements']
            else:
                newJob['fields']['description'] = u'funciones:\n' + job['fields']['functions'] + u'\nrequerimientos:\n' + job['fields']['requirements']
            new_core.append(newJob)

    return new_core

if __name__=="__main__":
    # Get updated model in json
    new_core = unifiedJSON('core2_mod.json')
    # write json in file
    open('data1_2.json','w').write(json.dumps(new_core))

    # Get DB data

    #updateDataBase('core15.json')
