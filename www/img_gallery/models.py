from django.db import models
from project.thumbs import ImageWithThumbsField

# Create your models here.

class Gallery(models.Model):
  title = models.CharField(max_length = 500, blank = False)
  photo = ImageWithThumbsField(upload_to='img_gallery', sizes=((100,100),(500,400)))  

  def __unicode__(self):
      return u"%s" % self.title

