# VENEZUELA
from django.db import models
import datetime
from django.utils import timezone


class Details(models.Model):
    hash = models.CharField(max_length=250, primary_key=True)

    title = models.CharField(max_length=300)
    typeJob = models.CharField(max_length=100)
    date = models.DateField()
    salary = models.CharField(max_length=50)
    company = models.CharField(max_length=100)
    url = models.URLField()
    # Incluir pais en lugar escalamiento a varios paises
    place = models.CharField(max_length=100)


    def __unicode__(self):
        return self.title

class Description(models.Model):
    hash = models.CharField(max_length=250, primary_key=True)

    # lista de jobs con esta descripcion
    job = models.ManyToManyField(Details)
    description = models.CharField(max_length=20000)
    # Posiblemente el descriptor hoja en la jerarquia
    area = models.CharField(max_length=100)

    def __unicode__(self):
        return self.description[:15]


