from django.views.generic import TemplateView
from django.shortcuts import render_to_response,redirect,render
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound, HttpResponseServerError
from django.views.decorators.cache import cache_page
from blog.models import Post
from img_gallery.models import Gallery


def select_country(request):
    return redirect(to='peru/',permanent=True)


#@cache_page(60 * 60)
class Index(TemplateView):
    template_name = 'website/index.html'
    posts = Post.objects.all().order_by("-date")
    last = ''
    if len(posts) != 0:
        last = posts[0]
    else:
        last = None

    def dispatch(self, *args, **kwargs):
        return super(Index, self).dispatch(*args, **kwargs)

    def get_context_data(self,  **kwargs):
        ctx = {'last': self.last,
               'posts':self.posts[1:min(4,len(self.posts))]}
        return ctx

########################################################################################################################################
## ERRORES
def error_403(request):
    t = loader.get_template('website/base/403.html')
    return HttpResponseForbidden(t.render(RequestContext(request)))


def error_404(request):
    t = loader.get_template('website/base/404.html')
    return HttpResponseNotFound(t.render(RequestContext(request)))


def error_500(request):
    t = loader.get_template('website/base/500.html')
    return HttpResponseServerError(t.render(RequestContext(request)))


def mail_verify(request):
    return render_to_response('verifyforzoho.html')

########################################################################################################################################

class Graficas(TemplateView):
    template_name = 'website/graficas.html'
    graficas = Gallery.objects.all()

    def dispatch(self, *args, **kwargs):
        return super(Graficas, self).dispatch(*args, **kwargs)

    def get_context_data(self,  **kwargs):
        ctx = {'graficas':self.graficas,
               }
        return ctx


def barchart(request):
    return render_to_response('website/bar.html')

def treemap(request):
    return render_to_response('website/treemap.html')

def circleplot(request):
    return render_to_response('website/circleplot.html')


########################################################################################################################################
class Quienes_somos(TemplateView):
    template_name = 'website/quienes_somos.html'
    graficas = Gallery.objects.all()
    banner = []
    for img in graficas:
        if any([
            'Requerimiento' in img.title,
            'Interrela' in img.title
        ]):
            banner.append(img)

    def dispatch(self, *args, **kwargs):
        return super(Quienes_somos, self).dispatch(*args, **kwargs)

    def get_context_data(self,  **kwargs):
        ctx = {'graficas':self.banner,
               }
        return ctx

def Panorama(request):
    return render_to_response('website/panorama.html')

def Servicios(request):
    return render_to_response('website/servicios.html')

def Contacto(request):
    return render_to_response('website/contacto.html')


################################################################################################

def new_data_circleplot(request):
    return render_to_response('website/new_data_circleplot.html')

def new_data_treemap(request):
    return render_to_response('website/new_data_treemap.html')


################################################################################################
#######################################################################
def peru_ing_circleplot(request):
    title = 'Relationship among engineering majors in Peru'
    total_ads = 45000
    country = 'peru'
    ctx = { 'title':title,
            'total_ads':total_ads,
            'country':country}
    return render(request,'website/asme/circleplot.html',ctx)

def peru_ing_treemap(request):
    title = 'Panorama of the Peruvian market demand'
    total_ads = 45000
    country = 'peru'
    ctx = { 'title':title,
            'total_ads':total_ads,
            'country':country}
    return render(request,'website/asme/treemap.html',ctx)

#######################################################################
def chile_ing_circleplot(request):
    title = 'Relationship among engineering majors in Chile'
    total_ads = 10000
    country = 'chile'
    ctx = { 'title':title,
            'total_ads':total_ads,
            'country':country}
    return render(request,'website/asme/circleplot.html',ctx)

def chile_ing_treemap(request):
    title = 'Panorama of the Chilean market demand'
    total_ads = 10000
    country = 'chile'
    ctx = { 'title':title,
            'total_ads':total_ads,
            'country':country}
    return render(request,'website/asme/treemap.html',ctx)

#######################################################################
#######################################################################
def col_ing_circleplot(request):
    title = 'Relationship among engineering majors in Colombia'
    total_ads = 30000
    country = 'col'
    ctx = { 'title':title,
            'total_ads':total_ads,
            'country':country}
    return render(request,'website/asme/circleplot.html',ctx)

def col_ing_treemap(request):
    title = 'Panorama of the Colombian market demand'
    total_ads = 30000
    country = 'col'
    ctx = { 'title':title,
            'total_ads':total_ads,
            'country':country}
    return render(request,'website/asme/treemap.html',ctx)
