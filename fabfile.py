from fabric.api import *
from fabric.colors import green,red
 
# remote server info
env.hosts = ['userjob@162.243.239.234:12345']

def push(message):
    """ commit changes locally """
    print(green("Add, commit and push..."))
    with settings(warn_only=True):
        local('git add .')
        local('git commit -a -m "%s" ' % message)      # fabric won't abort if there is nothing to commit
        local('git push')

def pushpull():
    """ push changes to repository and pull them to the server """
    print(green("Pushing and Pulling - master branch at GitHub..."))
    local('git pull origin master')
    local('git push origin master')
    print(green("Pulling master from GitHub - server..."))
    run('cd /home/userjob/;git checkout .;git pull origin master')

def restart():
    """ restart server remotely """
    sudo('systemctl restart httpd')
    print(red("RESTARTED!"))
    
def revert():
    """ Revert last pull to the working state the server should have been in before we forced a pull  """
    with cd(env['dir']):
        run('git reset --hard @{1}')
        restart() # restarts server 

def update_static():
    """ Update static files
        activate virtual env
        run grunt
        compile compass from website
        run collect static files from django
    """
    path = '/home/userjob'
    
    with cd("%s" % path):       
        print(green("Running Grunt : compiling CSS files, minimizing, uglifying..."))
        sudo('source %s/bin/activate && grunt build-dev' % path)

        print(green("Collecting static files..."))
        
        run('cd %s/www; source ../bin/activate && python manage.py collectstatic --noinput' % path)
    
    print(red("DONE!"))


def deploy(mess):
    commit(mess)
    pushpull()
    update_static()
    restart()

