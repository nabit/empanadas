import multiproccessing

bind = "127.0.0.1:8001" # dirección a donde accederá Nginx

# dirección donde estarán los logs de la aplicación
logfile = "/var/www/logs/nombreApp/gunicorn.log"

# dependerá en medida de la carga de trabajo que tenga la aplicación, también depende del hardware con que se cuente
workers = multiprocessing.cpu_count() * 2 + 1 

# tipo de logging
loglevel = 'info'
