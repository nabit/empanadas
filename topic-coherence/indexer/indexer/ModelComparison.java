package indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.aksw.palmetto.Coherence;
import org.aksw.palmetto.corpus.CorpusAdapter;

public class ModelComparison extends RunPalmetto {

	public static void main(String[] args) {
		//String user = "/home/ronotex/";
		String user = "/home/empleatron/";
		
		String indexPath = user+"empanadas/topic-coherence/index";
        String pref = "full";
		//String pref = "ne";
        String docs_path = user+"empanadas/dataR/topic words/"+pref+"/";
        String results_dir = user+"empanadas/topic-coherence/results/k_comparison/" + pref+"/";

		Integer k1 = 26;
		Integer k2 = 14;
		ArrayList<String> inf_methods = new ArrayList<String>();
		inf_methods.add("Gibbs");
		inf_methods.add("VEM_fix_umass_10_sa");	// FULL
		//inf_methods.add("VEM_fix_cp_10_sa");	// NE
		inf_methods.add("VEM_est");
		
		
        ArrayList<String> calcTypes = new ArrayList<String>();
        calcTypes.add("umass");
        calcTypes.add("uci");
        calcTypes.add("npmi");
        calcTypes.add("c_a");
        calcTypes.add("c_p");
        calcTypes.add("c_v");

        //								  Gibbs k_id  k  n_tw
        String[][][][] word_table = new String[3][2][30][20];
        
        // reading specific topic lists
        System.out.println("Reading topics words...");
        ExtendedWordSetReader reader = new ExtendedWordSetReader();
        final File docs_folder = new File(docs_path);
        
        for (File doc_name : docs_folder.listFiles()){
        	String str = doc_name.getName();
        	String[] splitted = str.split("_");
        	int k=-1;
        	String inf_meth = "";
        	if ( splitted[0].equals("Gibbs") && 
        		( splitted[1].equals(k1.toString()) || splitted[1].equals(k2.toString()) ) 
        		){
        		k = Integer.parseInt( splitted[1]);
        		inf_meth = "Gibbs";
        	}
        	if ( splitted[0].equals("VEM") ){
        		if (splitted[1].equals("est")){
        			k = Integer.parseInt(splitted[2]);
        			inf_meth = "VEM_est";
        		}else{
        			String reconstruct = splitted[2]+"_"+splitted[3]+"_"+splitted[4];
        			reconstruct = "VEM_fix_"+reconstruct;
        			if (inf_methods.contains(reconstruct)){
        				inf_meth = reconstruct;
        				k = Integer.parseInt(splitted[5]);
        			}
        		}
        	}
        	if(k!=k1 && k!=k2)
        		continue;
        	int inf_idx = inf_methods.indexOf(inf_meth);
        	int k_idx = k==k1?0:1;
        	
        	String raw[][] = reader.readWordSets(doc_name.getAbsolutePath(),20); // 20 is default
        	for (int i=0;i<k;++i){
        		for (int j=0;j<20;++j)
        			word_table[inf_idx][k_idx][i][j] = raw[i][j];
        	}
        }//END-FOR-READ-FILES
        
        DecimalFormat df = new DecimalFormat("#.#####");
        System.out.println("Calculating coherences by topic");
        
        for (int inf_id=0;inf_id<inf_methods.size();++inf_id){
        	for (int k_id=0;k_id<2;++k_id){
        		Integer k = k_id==0?k1:k2;
        		String output_filename = inf_methods.get(inf_id)+"_"+k.toString();
        		System.out.println("processing file "+output_filename);
        		try {
    				PrintWriter writer = new PrintWriter(results_dir+output_filename);
	        		for(String calcType : calcTypes ){
	        			CorpusAdapter corpusAdapter = getCorpusAdapter(calcType, indexPath);
	                    
	                    String[][] subtable = new String[k][10];
	                    for (int i=0;i<k;++i)
	                    	for(int j=0;j<10;++j)
	                    		subtable[i][j] = word_table[inf_id][k_id][i][j];
	                    // get coherences by topic
	                    Coherence coherence = getCoherence(calcType, corpusAdapter);
	                    double coherences[] = coherence.calculateCoherences(subtable);
	                    // write into file
	                    writer.print(calcType+"=[");
	                    for (int i=0;i<coherences.length;++i){
	                    	if (i>0)
	                    		writer.print(",");
	                    	writer.print(df.format(coherences[i]));
	                    }
	                    writer.print("]\n");
	                    corpusAdapter.close();	                    
	        		}//END-SCORES
                    
                    writer.close();
        		} catch (FileNotFoundException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}//END-TRY
        		
        	}//END-K-ID
        	
        }//END-FOR-INF-METHODS
	}

}
