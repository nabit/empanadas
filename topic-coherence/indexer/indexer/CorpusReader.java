package indexer;
import java.util.*;
import java.io.*;
import java.text.Normalizer;

import org.aksw.palmetto.corpus.lucene.creation.IndexableDocument;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import com.mongodb.client.FindIterable;


public class CorpusReader {
	private String docsPath="";
	private ArrayList<String> docs_list = new ArrayList<String>();
	private ArrayList<IndexableDocument> indexable_docs_list = new ArrayList<IndexableDocument>();
	
	public CorpusReader(){	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String docsPath = "/home/ronotex/mini_wiki/";
		//new CorpusReader().read_wikipedia(docsPath);
		new CorpusReader().read_jobdb();
		
	}
	
	public void read_wikipedia(String docsPath){
		final File folder = new File(docsPath);
		for (File doc_name : folder.listFiles()){
			//get absolute name+path of file
			doc_name = doc_name.getAbsoluteFile();
			
			//define doc String
			String doc = "";
			int doc_init = 0;
			int n_tokens = 0;
			
			// encoding ES-europe: ISO-8859-15
			try (BufferedReader br =
	                   new BufferedReader(new InputStreamReader(
	                           					new FileInputStream(doc_name), "ISO-8859-15"))){
				// read file
				String line;
				while((line = br.readLine()) != null){
					// discard newlines and headers
					if ( line.startsWith("<doc")  ||
						 line.startsWith("</doc") ||
						 line.length()==0
							)
						continue;
					// stop processing after ENDOFARTICLE
					if ( line.startsWith("ENDOFARTICLE"))
						break;
					
					String[] splitted_line = line.split(" ");
					// add word to doc string
					if (doc_init==0)
						doc_init=1;
					else
						doc+=" ";
					String word = splitted_line[0].toLowerCase();
					word = Normalizer.normalize(word, Normalizer.Form.NFD);
					word = word.replaceAll("[^\\x00-\\x7F]", "");
					doc += word;
					n_tokens++;
					//System.out.println(splitted_line[0]+" - "+word);
				}
				
			}catch (UnsupportedEncodingException e) 
		    {
				System.out.println(e.getMessage());
		    } 
		    catch (IOException e) 
		    {
				System.out.println(e.getMessage());
		    }
		    catch (Exception e)
		    {
				System.out.println(e.getMessage());
		    }
			// Add doc to doc_list
			//this.docs_list.add(doc);
			
			// Add doc to indexable_doc list
			this.indexable_docs_list.add(new IndexableDocument(doc,n_tokens) );
			
			//System.out.println(doc);
		}//END-FOR
	}//END-METHOD
	
	
	public void read_jobdb(){
		MongoClient mongoClient = new MongoClient();
		MongoDatabase db = mongoClient.getDatabase("JobDB");
		
		FindIterable<Document> iterable = db.getCollection("core_tokenized").find();
		for(Document doc : iterable){
			ArrayList tokens = (ArrayList)doc.get("tokens");
			
			String joined_doc = "";
			int doc_init = 0;
			int n_tokens = 0;
			
			for (Object sent : tokens){
				//sent = (ArrayList<String>)sent;
				for (Object w : (ArrayList)sent){
					String word = (String)w;
					//basic preprocessing
					word = word.toLowerCase();
					word = Normalizer.normalize(word, Normalizer.Form.NFD);
					word = word.replaceAll("[^\\x00-\\x7F]", "");
					
					if (doc_init==0)
						doc_init=1;
					else
						joined_doc+=" ";
					joined_doc+=word;
					n_tokens++;
				}
				// Add sentence separator
				if (joined_doc.substring(joined_doc.length()-1)==".")
					joined_doc+=" .";
			}//END-FOR-TOKENS
			
			// Add doc to indexable_doc list
			this.indexable_docs_list.add(new IndexableDocument(joined_doc,n_tokens) );

		}//END-FOR-DOCS
		
		mongoClient.close();
	}
	
	
	public ArrayList<String> get_doc_list(){
		return this.docs_list;
	}
	
	public ArrayList<IndexableDocument> get_indexable_doc_list(){
		return this.indexable_docs_list;
	}
}
