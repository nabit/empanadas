package indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.aksw.palmetto.Coherence;
import org.aksw.palmetto.corpus.CorpusAdapter;


public class RunPalmetto_VEMfixed_full extends RunPalmetto {
	public static void main(String[] args) {
		//String user = "/home/ronotex/";
		String user = "/home/empleatron/";
		
		String indexPath = user+"empanadas/topic-coherence/index";
        String pref = "full";
        String docs_path = user+"empanadas/dataR/topic words/"+pref+"/";
        String results_dir = user+"empanadas/topic-coherence/results/" + pref+"-vemfixed/";
        
        Integer[] temp = {5, 10, 14, 18, 22, 26, 30, 34, 38, 42, 46, 50, 60, 70, 80, 90, 100, 110, 120, 150, 200};
        ArrayList<Integer> ks = new ArrayList<Integer>(Arrays.asList(temp));
        
        String[] temp2 = {
        	"cp_15_sa",
        	"cp_avg_sm",
        	"uci_10_sa",
        	"ca_10_sa",
        	"umass_10_sa"
        };
        ArrayList<String> unique_confs = new ArrayList<String>(Arrays.asList(temp2));
        
        String[] conf_data_source = {	// fila de la cual leer la data
            	"cp_15_sa",
            	"cp_avg_sm",
            	"uci_10_sa",
            	"uci_10_sa",
            	"cp_avg_sm",
            	"ca_10_sa",
            	"umass_10_sa"
            };
        String[] confs = {
        	"cp_15_sa",
        	"cp_avg_sm",
        	"uci_10_sa",
        	"npmi_10_sa",
        	"cv_10_sa",
        	"ca_10_sa",
        	"umass_10_sa"
        };
        
        String[][][][] word_table = new String[5][ks.size()][201][21]; 
        
        System.out.println("Reading files");
        
        ExtendedWordSetReader reader = new ExtendedWordSetReader();
        
        final File docs_folder = new File(docs_path);
        for (File doc_name : docs_folder.listFiles()){
        	String str = doc_name.getName();
        	if (!str.startsWith("VEM_fix"))
        		continue;
        	
        	String[] splitted = str.split("_");
        	String curr_conf = splitted[2]+"_"+splitted[3]+"_"+splitted[4];
        	int k = Integer.parseInt(splitted[5]);
        	int k_idx = ks.indexOf(k);
        	
        	int conf_idx = unique_confs.indexOf(curr_conf);
        	
        	// read words
        	String wordsets[][] = reader.readWordSets(doc_name.getAbsolutePath(),20); // 20 is default
        	// fill table
        	for (int i=0;i<k;++i){
        		for (int j=0;j<20;++j){
        			word_table[conf_idx][k_idx][i][j] = wordsets[i][j];
        		}
        	}
        }//END-FOR-FILES
        
        DecimalFormat df = new DecimalFormat("#.#####");
        System.out.println("Calculating coherences by configuration");
        
        for (int conf_id=0;conf_id<confs.length;++conf_id){        	
        	System.out.println("source conf: "+conf_data_source[conf_id]+", target conf: "+confs[conf_id]);
        	String[] splitted = confs[conf_id].split("_"); 
        	
        	int source_conf_idx = unique_confs.indexOf(conf_data_source[conf_id]);
        	String score = splitted[0];
        	String str_tw = splitted[1];
        	int tw = 20;
        	String agg = splitted[2];
        	
        	if (score.startsWith("c"))
        		score = "c_"+score.substring(1);
        	if (!str_tw.equals("avg"))
        		tw = Integer.parseInt(str_tw);
        	if (agg.equals("sa"))
        		agg = "sigma_a";
        	else
        		agg = "sigma_m";
        	
        	CorpusAdapter corpusAdapter = getCorpusAdapter(score, indexPath);
            Coherence coherence = getCoherence(score, corpusAdapter);
            ArrayList<Double> cohs = new ArrayList<Double>();
            
            for (int k_idx=0;k_idx<ks.size();++k_idx){
            	double coh_model = 0.0;
            	int k=ks.get(k_idx);
            	
            	System.out.println("  ->k: "+k);
            	
            	if (tw==20){ // AVERAGE
            		coh_model = get_average(word_table, source_conf_idx,k_idx, k, agg, coherence);
            	}else{
	            	String[][] sub_table = new String[k][tw];
	            	for (int i=0;i<k;++i)
	            		for (int j=0;j<tw;++j)
	            			sub_table[i][j] = word_table[source_conf_idx][k_idx][i][j];
	            	double coherences[] = coherence.calculateCoherences(sub_table);
	                coh_model = get_aggregated_score(coherences,agg);
            	}
                
                cohs.add(coh_model);
            }
            corpusAdapter.close();
            //write results
            String output_filename = "VEMfix_"+confs[conf_id];
            try {
				PrintWriter writer = new PrintWriter(results_dir+output_filename);
	        	writer.println("topic_words="+tw);
	        	writer.println("aggregation="+agg);
	        	writer.println("ks="+ks.toString());
	        	writer.print(score+"=[");
	        	
	        	for (int i=0;i<cohs.size();++i){
	        		if(i>0)
	        			writer.print(",");
	        		writer.print(df.format(cohs.get(i)));
	        	}
	        	writer.print("]\n");
	        	writer.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }//END-FOR-CONFS
        
	}
	
	public static double get_average(String[][][][] wordset, int conf_idx, int k_idx, int k, String agg, Coherence coherence){
		double coherences[];
		double coh_model;
		double res = 0.0;
		
		// 5-tw score
		String[][] sub_table1 = new String[k][5];
		for (int i=0;i<k;++i)
    		for (int j=0;j<5;++j)
    			sub_table1[i][j] = wordset[conf_idx][k_idx][i][j];
    	coherences = coherence.calculateCoherences(sub_table1);
        coh_model = get_aggregated_score(coherences,agg);
        res += coh_model;  

        // 10-tw score
		String[][] sub_table2 = new String[k][10];
		for (int i=0;i<k;++i)
    		for (int j=0;j<10;++j)
    			sub_table2[i][j] = wordset[conf_idx][k_idx][i][j];
		coherences = coherence.calculateCoherences(sub_table2);
        coh_model = get_aggregated_score(coherences,agg);
        res += coh_model;  
		
		// 15-tw score
		String[][] sub_table3 = new String[k][15];
		for (int i=0;i<k;++i)
    		for (int j=0;j<15;++j)
    			sub_table3[i][j] = wordset[conf_idx][k_idx][i][j];
		coherences = coherence.calculateCoherences(sub_table3);
        coh_model = get_aggregated_score(coherences,agg);
        res += coh_model; 
    	
        return res/3.0;
	}


}
