package indexer;

import java.util.*;
import java.io.*;

import org.aksw.palmetto.*;
import org.aksw.palmetto.corpus.lucene.creation.IndexableDocument;
import org.aksw.palmetto.corpus.lucene.creation.LuceneIndexHistogramCreator;
//import org.aksw.palmetto.corpus.lucene.creation.SimpleLuceneIndexCreator;
import org.aksw.palmetto.corpus.lucene.creation.PositionStoringLuceneIndexCreator;


public class Indexer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CorpusReader reader = new CorpusReader();
		
		String path = "/home/ronotex/wikicorpus/";
		System.out.println("Reading wikipedia...");
		reader.read_wikipedia(path);
		System.out.println("Reading JobDB...");
		reader.read_jobdb();
		
		ArrayList<IndexableDocument> docs = reader.get_indexable_doc_list();
		Iterator<IndexableDocument> docs_it = docs.iterator();
		
		// Palmetto vars
		System.out.println("Building index...");
		File indexDir = new File("/home/ronotex/index/");
		PositionStoringLuceneIndexCreator creator = new PositionStoringLuceneIndexCreator(
		        Palmetto.DEFAULT_TEXT_INDEX_FIELD_NAME, Palmetto.DEFAULT_DOCUMENT_LENGTH_INDEX_FIELD_NAME);
		
		if(creator.createIndex(indexDir, docs_it)) {
	        LuceneIndexHistogramCreator histogramCreator = new LuceneIndexHistogramCreator(
	                Palmetto.DEFAULT_DOCUMENT_LENGTH_INDEX_FIELD_NAME);
	        histogramCreator.createLuceneIndexHistogram(indexDir.getAbsolutePath());
		}
		
	}

}
