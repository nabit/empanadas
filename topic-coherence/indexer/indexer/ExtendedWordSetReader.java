package indexer;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aksw.palmetto.io.SimpleWordSetReader;


public class ExtendedWordSetReader extends SimpleWordSetReader{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SimpleWordSetReader.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public String[][] readWordSets(String inputFile, int num_topwords) {
        List<String[]> topics = new ArrayList<String[]>();
        FileReader reader = null;
        Scanner scanner = null;
        try {
            String[] wordset;
            reader = new FileReader(inputFile);
            scanner = new Scanner(reader);
            while (scanner.hasNextLine()) {
                wordset = parseWordSetFromLine(scanner.nextLine(),num_topwords);
                if ((wordset != null) && (wordset.length > 0)) {
                    topics.add(wordset);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error while creating Index. Aborting.", e);
        } finally {
            if (scanner != null) {
                try {
                    scanner.close();
                } catch (Exception e) {
                }
            } else {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return topics.toArray(new String[topics.size()][]);
    }

    private String[] parseWordSetFromLine(String line, int num_topwords) {
        List<String> topic = new ArrayList<String>();
        StringTokenizer tokenizer = new StringTokenizer(line);
        while ((tokenizer.hasMoreTokens()) && num_topwords>0) {
            String nextToken = tokenizer.nextToken();
            topic.add(nextToken.toLowerCase());
            num_topwords--;
        }
        return topic.toArray(new String[topic.size()]);
    }
}
