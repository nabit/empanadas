package indexer;

import java.io.*;
import java.util.*;
import java.text.DecimalFormat;

import org.aksw.palmetto.aggregation.ArithmeticMean;
import org.aksw.palmetto.calculations.direct.FitelsonConfirmationMeasure;
import org.aksw.palmetto.calculations.direct.LogCondProbConfirmationMeasure;
import org.aksw.palmetto.calculations.direct.LogRatioConfirmationMeasure;
import org.aksw.palmetto.calculations.direct.NormalizedLogRatioConfirmationMeasure;
import org.aksw.palmetto.calculations.indirect.CosinusConfirmationMeasure;
import org.aksw.palmetto.corpus.CorpusAdapter;
import org.aksw.palmetto.corpus.WindowSupportingAdapter;
import org.aksw.palmetto.corpus.lucene.LuceneCorpusAdapter;
import org.aksw.palmetto.corpus.lucene.WindowSupportingLuceneCorpusAdapter;
import org.aksw.palmetto.prob.bd.BooleanDocumentProbabilitySupplier;
import org.aksw.palmetto.prob.window.BooleanSlidingWindowFrequencyDeterminer;
import org.aksw.palmetto.prob.window.ContextWindowFrequencyDeterminer;
import org.aksw.palmetto.prob.window.WindowBasedProbabilityEstimator;
import org.aksw.palmetto.subsets.OneOne;
import org.aksw.palmetto.subsets.OnePreceding;
import org.aksw.palmetto.subsets.OneSet;
import org.aksw.palmetto.vector.DirectConfirmationBasedVectorCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.aksw.palmetto.*;



public class RunPalmetto {
	private static final Logger LOGGER = LoggerFactory.getLogger(RunPalmetto.class);

    public static final String DEFAULT_TEXT_INDEX_FIELD_NAME = "text";
    public static final String DEFAULT_DOCUMENT_LENGTH_INDEX_FIELD_NAME = "length";
    
    public static void main(String[] args) {
        String indexPath = "/home/ronotex/empanadas/topic-coherence/index";
        String pref = "ne/";
        String docs_path = "/home/ronotex/empanadas/dataR/topic words/"+pref;
        String results_dir = "/home/ronotex/empanadas/topic-coherence/results/" + pref;
        
        ArrayList<String> calcTypes = new ArrayList<String>();
        calcTypes.add("umass");
        calcTypes.add("uci");
        calcTypes.add("npmi");
        calcTypes.add("c_a");
        calcTypes.add("c_p");
        calcTypes.add("c_v");
        
        ArrayList<String> inf_methods = new ArrayList<String>();
        inf_methods.add("Gibbs");
        inf_methods.add("VEM_est");
        inf_methods.add("VEM_fix");
        
        System.out.println("Reading topics words...");
        final File docs_folder = new File(docs_path);
        
        // obtain all number of topics
        ArrayList<Integer> num_topics_list = new ArrayList<Integer>();
        
        for (File doc_name : docs_folder.listFiles()){
        	String str = doc_name.getName();
        	String[] splitted = str.split("_");
        	if (splitted[0].equals("Gibbs"))
        		num_topics_list.add(Integer.parseInt(splitted[1]));
        }
        // Sort Ks
        Collections.sort(num_topics_list);
        int n_ks = num_topics_list.size();
        int n_coh = calcTypes.size();

        // Define pipeline parameters
        Double[][][] coherence_vals = new Double[3][n_coh][n_ks];
        ExtendedWordSetReader reader = new ExtendedWordSetReader();
        
        /*
         * Aggregation method:
         * sigma_a : arithmetic mean
         * sigma_m : median
         * sigma_min: minimum
         * sigma_max: maximum
         */
        String aggregation_method = "sigma_a";
        int num_topicwords = 10;
        
        // cleaning table
        for (int i=0;i<3;++i)
        	for (int k=0;k<n_coh;k++)
        		for (int j=0;j<n_ks;++j)
        			coherence_vals[i][k][j] = Double.NEGATIVE_INFINITY;
        
        System.out.println("Calculating coherences");
        int count=0;
    	for (String calcType : calcTypes){
    		CorpusAdapter corpusAdapter = getCorpusAdapter(calcType, indexPath);
            Coherence coherence = getCoherence(calcType, corpusAdapter);
            // Iterate over models for given coherence
	        for (File doc_name : docs_folder.listFiles()){
	        	if (count%50==0)
	        		System.out.println("-->"+count);
	        	count++;
	        	
	        	String str = doc_name.getName();
	        	String[] splitted = str.split("_");
	        	String method = "";
	        	int num_topics = 0;
	        	if (splitted[0].equals("VEM")){
	        		method = splitted[0]+"_"+splitted[1];
	        		num_topics = Integer.parseInt(splitted[2]);
	        	}else{
	        		method = splitted[0];
	        		num_topics = Integer.parseInt(splitted[1]);
	        	}
	        	//read topic word sets
	        	String wordsets[][] = reader.readWordSets(doc_name.getAbsolutePath(),num_topicwords);
	            
	        	// Confirmation measure
	        	double coherences[] = coherence.calculateCoherences(wordsets);
	            double coh_model = get_aggregated_score(coherences,aggregation_method);
	            
	            int meth_idx = inf_methods.indexOf(method);
	            int coh_idx  = calcTypes.indexOf(calcType);
	            int k_idx    = num_topics_list.indexOf(num_topics);
	            
	            coherence_vals[meth_idx][coh_idx][k_idx] = coh_model;
	            /*
	            printCoherences(coherences, wordsets, System.out);
	            System.out.println(coh_model);
	            System.out.println("-----------------");
	            */
	        }//END-FOR-MODELS
	        corpusAdapter.close();
    	}//END-FOR-COH_MEASURES
        
    	System.out.println("Writing results");
    	DecimalFormat df = new DecimalFormat("#.#####");
        for (int i=0;i<3;++i){
        	String method = inf_methods.get(i);	// get inference method
        	// initialize output file
        	String output_filename = method+"_"+num_topicwords+"_"+aggregation_method;
        	try {
				PrintWriter writer = new PrintWriter(results_dir+output_filename);
	        	writer.println("topic_words="+num_topicwords);
	        	writer.println("aggregation="+aggregation_method);
	        	writer.println("ks="+num_topics_list.toString());
	        	
	        	for (int j=0;j<n_coh;++j){
	        		String coh_name = calcTypes.get(j);
	        		writer.print(coh_name+"=[");
	        		for (int k=0;k<n_ks;++k){
	        			if (k>0)
	        				writer.print(",");
	        			writer.print(df.format(coherence_vals[i][j][k]));
	        		}
	        		writer.print("]\n");
	        	}
	        	writer.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }//END-FOR-WRITING
    }   
    
    public static double get_aggregated_score(double[] coherences, String aggregation_method) {
    	double res = 0.0;
    	int N = coherences.length;
    	// Arithmetic mean
    	if (aggregation_method.equals("sigma_a")){
    		for (int i=0;i<N;++i)
    			res+=coherences[i];
    		res/=N;
    	// Median
    	}else if (aggregation_method.equals("sigma_m")){
    		Arrays.sort(coherences);
    		if ((N%2)==0){		// Ntopics = par
    			res = (coherences[N/2]+coherences[(N-1)/2]) / 2.0;
    		}else
    			res = coherences[(N+1)/2];
    	// Minimum
    	}else if (aggregation_method.equals("sigma_min")){
    		res = Double.POSITIVE_INFINITY;
    		for (int i=0;i<N;++i)
    			res = Math.min(res, coherences[i]);
    	}
    	// Maximum
    	else if (aggregation_method.equals("sigma_max")){
    		res = Double.NEGATIVE_INFINITY;
    		for (int i=0;i<N;++i)
    			res = Math.max(res, coherences[i]);
    	}
		return res;
	}
    
    public static double get_average(String[][][][] wordset, int k_idx, int a_idx, int k, String agg, Coherence coherence){
		double coherences[];
		double coh_model;
		double res = 0.0;
		
		// 5-tw score
		String[][] sub_table1 = new String[k][5];
		for (int i=0;i<k;++i)
    		for (int j=0;j<5;++j)
    			sub_table1[i][j] = wordset[k_idx][a_idx][i][j];
    	coherences = coherence.calculateCoherences(sub_table1);
        coh_model = get_aggregated_score(coherences,agg);
        res += coh_model;  

        // 10-tw score
		String[][] sub_table2 = new String[k][10];
		for (int i=0;i<k;++i)
    		for (int j=0;j<10;++j)
    			sub_table2[i][j] = wordset[k_idx][a_idx][i][j];
		coherences = coherence.calculateCoherences(sub_table2);
        coh_model = get_aggregated_score(coherences,agg);
        res += coh_model;  
		
		// 15-tw score
		String[][] sub_table3 = new String[k][15];
		for (int i=0;i<k;++i)
    		for (int j=0;j<15;++j)
    			sub_table3[i][j] = wordset[k_idx][a_idx][i][j];
		coherences = coherence.calculateCoherences(sub_table3);
        coh_model = get_aggregated_score(coherences,agg);
        res += coh_model; 
    	
        return res/3.0;
	}

	public static CorpusAdapter getCorpusAdapter(String calcType, String indexPath) {
        try {
            if ("umass".equals(calcType)) {
                return LuceneCorpusAdapter.create(indexPath, DEFAULT_TEXT_INDEX_FIELD_NAME);
            } else {
                return WindowSupportingLuceneCorpusAdapter.create(indexPath,
                        DEFAULT_TEXT_INDEX_FIELD_NAME, DEFAULT_DOCUMENT_LENGTH_INDEX_FIELD_NAME);
            }
        } catch (Exception e) {
            LOGGER.error("Couldn't open lucene index. Aborting.", e);
            return null;
        }
    }
    
    public static Coherence getCoherence(String calcType, CorpusAdapter corpusAdapter) {
        if ("umass".equals(calcType)) {
            return new DirectConfirmationBasedCoherence(new OnePreceding(),
                    BooleanDocumentProbabilitySupplier.create(corpusAdapter, "bd", true),
                    new LogCondProbConfirmationMeasure(), new ArithmeticMean());
        }

        if ("uci".equals(calcType)) {
            return new DirectConfirmationBasedCoherence(
                    new OneOne(), getWindowBasedProbabilityEstimator(10, (WindowSupportingAdapter) corpusAdapter),
                    new LogRatioConfirmationMeasure(), new ArithmeticMean());
        }

        if ("npmi".equals(calcType)) {
            return new DirectConfirmationBasedCoherence(
                    new OneOne(), getWindowBasedProbabilityEstimator(10, (WindowSupportingAdapter) corpusAdapter),
                    new NormalizedLogRatioConfirmationMeasure(), new ArithmeticMean());
        }

        if ("c_a".equals(calcType)) {
            int windowSize = 5;
            WindowBasedProbabilityEstimator probEstimator = new WindowBasedProbabilityEstimator(
                    new ContextWindowFrequencyDeterminer((WindowSupportingAdapter) corpusAdapter, windowSize));
            probEstimator.setMinFrequency(WindowBasedProbabilityEstimator.DEFAULT_MIN_FREQUENCY * windowSize);
            return new VectorBasedCoherence(
                    new OneOne(), new DirectConfirmationBasedVectorCreator(probEstimator,
                            new NormalizedLogRatioConfirmationMeasure()), new CosinusConfirmationMeasure(),
                    new ArithmeticMean());
        }

        if ("c_p".equals(calcType)) {
            return new DirectConfirmationBasedCoherence(
                    new OnePreceding(),
                    getWindowBasedProbabilityEstimator(70, (WindowSupportingAdapter) corpusAdapter),
                    new FitelsonConfirmationMeasure(), new ArithmeticMean());
        }

        if ("c_v".equals(calcType)) {
            return new VectorBasedCoherence(new OneSet(),
                    new DirectConfirmationBasedVectorCreator(
                            getWindowBasedProbabilityEstimator(110, (WindowSupportingAdapter) corpusAdapter),
                            new NormalizedLogRatioConfirmationMeasure()),
                    new CosinusConfirmationMeasure(), new ArithmeticMean());
        }

        StringBuilder msg = new StringBuilder();
        msg.append("Unknown calculation type \"");
        msg.append(calcType);
        msg.append("\". Supported types are:\nUMass\nUCI\nNPMI\nC_A\nC_P\nC_V\n\nAborting.");
        LOGGER.error(msg.toString());
        return null;
    }
    
    public static WindowBasedProbabilityEstimator getWindowBasedProbabilityEstimator(int windowSize,
            WindowSupportingAdapter corpusAdapter) {
        WindowBasedProbabilityEstimator probEstimator = new WindowBasedProbabilityEstimator(
                new BooleanSlidingWindowFrequencyDeterminer(
                        corpusAdapter, windowSize));
        probEstimator.setMinFrequency(WindowBasedProbabilityEstimator.DEFAULT_MIN_FREQUENCY * windowSize);
        return probEstimator;
    }

    public static void printCoherences(double[] coherences, String[][] wordsets, PrintStream out) {
        for (int i = 0; i < wordsets.length; i++) {
            out.format("%5d\t%3.5f\t%s%n", new Object[] { i, coherences[i], Arrays.toString(wordsets[i]) });
        }
    }    

}
