package indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.aksw.palmetto.Coherence;
import org.aksw.palmetto.corpus.CorpusAdapter;

public class AlphaTuning extends RunPalmetto {

	public static void main(String[] args) {
		//String user = "/home/ronotex/";
		String user = "/home/empleatron/";
		
		String indexPath = user+"empanadas/topic-coherence/index";
        String pref = "full/";
        String docs_path = user+"empanadas/dataR/topic words/alpha_tuning/"+pref;
        String results_dir = user+"empanadas/topic-coherence/results/alpha_tuning/" + pref;
        
        Double[] alphas = {1e-5,1e-4,1e-3,0.01,0.05,0.08,0.1,0.5,0.8,1.0,1.5,2.0,5.0,10.0};
        int n_alphas = alphas.length;
        
        /*
        Integer[] temp = {10,14,18,34,42};
        ArrayList<Integer> ks = new ArrayList<Integer>(Arrays.asList(temp) );
        String[][][][] word_table = new String[6][15][43][20];
        
        //States FULL
        String[] scores = {
                "umass",
                "uci",
                "npmi",
                "c_a",
                "c_p",
                "c_p",
                "c_v"
        };
        String[] aggs = {
        		"sigma_a",
        		"sigma_a",
        		"sigma_a",
        		"sigma_a",
        		"sigma_a",
        		"sigma_m",
        		"sigma_a"
        };
        Integer[] ntws = {
        		10,
        		10,
        		10,
        		10,
        		15,
        		0,
        		10
        };
        Integer[] nks = {
        		14,
        		14,
        		14,
        		42,
        		18,
        		34,
        		18
        };
        */
        
      //States NE
        Integer[] temp = {10,18,34,38,110};
        ArrayList<Integer> ks = new ArrayList<Integer>(Arrays.asList(temp) );
        //                                   n_k n_a max_k n_tw
        String[][][][] word_table = new String[6][15][111][20];
        
        String[] scores = {
                "umass",
                "uci",
                "npmi",
                "c_a",
                "c_a",
                "c_a",
                "c_p",
                "c_v"
        };
        String[] aggs = {
        		"sigma_a",
        		"sigma_m",
        		"sigma_a",
        		"sigma_a",
        		"sigma_m",
        		"sigma_m",
        		"sigma_a",
        		"sigma_m"
        };
        Integer[] ntws = {
        		10,
        		10,
        		10,
        		15,
        		15,
        		0,
        		10,
        		10
        };
        Integer[] nks = {
        		10,
        		18,
        		18,
        		38,
        		34,
        		18,
        		18,
        		110,
        };
        
        int n_states = scores.length;
        
        System.out.println("Reading topics words...");
        ExtendedWordSetReader reader = new ExtendedWordSetReader();
        final File docs_folder = new File(docs_path);
        
        for (File doc_name : docs_folder.listFiles()){
        	String str = doc_name.getName();
        	String[] splitted = str.split("_");
        	
        	int k = Integer.parseInt(splitted[1]);
        	int alpha_idx = Integer.parseInt(splitted[2]) - 1;
        	int k_idx = ks.indexOf(k);
        	// read words
        	String wordsets[][] = reader.readWordSets(doc_name.getAbsolutePath(),20); // 20 is default
        	// fill table
        	for (int i=0;i<k;++i){
        		for (int j=0;j<20;++j){
        			word_table[k_idx][alpha_idx][i][j] = wordsets[i][j];
        		}
        	}
        }
        
        DecimalFormat df = new DecimalFormat("#.#####");
        System.out.println("Calculating coherences by state");
        
        for (int state=0;state<n_states;++state){
        	System.out.println("state: "+state);
        	
        	CorpusAdapter corpusAdapter = getCorpusAdapter(scores[state], indexPath);
            Coherence coherence = getCoherence(scores[state], corpusAdapter);
            int k = nks[state];
            int tw = ntws[state];
            int k_idx = ks.indexOf(k);
            ArrayList<Double> cohs = new ArrayList<Double>();
            
            for (int alpha_idx=0;alpha_idx<n_alphas;++alpha_idx){
            	double coh_model = 0.0;
            	if (tw==0){ // AVERAGE
            		coh_model = get_average(word_table, k_idx, alpha_idx, k, aggs[state], coherence);
            	}else{
	            	String[][] sub_table = new String[k][tw];
	            	for (int i=0;i<k;++i)
	            		for (int j=0;j<tw;++j)
	            			sub_table[i][j] = word_table[k_idx][alpha_idx][i][j];
	            	double coherences[] = coherence.calculateCoherences(sub_table);
	                coh_model = get_aggregated_score(coherences,aggs[state]);
            	}
                
                cohs.add(coh_model);
            }
            corpusAdapter.close();
            //write results
            String output_filename = scores[state]+"_"+k+"_"+tw+"_"+aggs[state];
            try {
				PrintWriter writer = new PrintWriter(results_dir+output_filename);
	        	writer.println("alphas="+Arrays.asList(alphas).toString());
	        	writer.print(scores[state]+"=[");
	        	
	        	for (int i=0;i<cohs.size();++i){
	        		if(i>0)
	        			writer.print(",");
	        		writer.print(df.format(cohs.get(i)));
	        	}
	        	writer.print("]\n");
	        	writer.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }//END-FOR-STATES

	}

}
