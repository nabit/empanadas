
\documentclass{sig-alternate}
\usepackage{array}
\usepackage{tabularx}
\begin{document}
%
% --- Author Metadata here ---
\conferenceinfo{TM'15}{October 19 2015, Melbourne, VIC, Australia}
\CopyrightYear{2015} % Allows default copyright year (20XX) to be over-ridden - IF NEED BE.

\global\copyrightetc{Copyright \the\copyrtyr\ ACM \the\acmcopyr}
\crdata{ISBN 978-1-4503-3784-7/15/10...\$15.00.\\
http://dx.doi.org/10.1145/2809936.2809945}  % Allows default copyright data (0-89791-88-6/97/05) to be over-ridden - IF NEED BE.
% --- End of Author Metadata ---

\title{Labor market demand analysis for engineering majors in Peru using Shallow Parsing and Topic Modeling}
%
% You need the command \numberofauthors to handle the 'placement
% and alignment' of the authors beneath the title.
%
\numberofauthors{4} 
% For aesthetic reasons, we recommend 'three authors at a time'
% i.e. three 'name/affiliation blocks' be placed beneath the title.
%
% NOTE: You are NOT restricted in how many 'rows' of
% "name/affiliations" may appear. We just ask that you restrict
% the number of 'columns' to three.
%
% Because of the available 'opening page real-estate'
% we ask you to refrain from putting more than six authors
% (two rows with three columns) beneath the article title.
% More than six makes the first-page appear very cluttered indeed.
%
% Use the \alignauthor commands to handle the names
% and affiliations for an 'aesthetic maximum' of six authors.
% Add names, affiliations, addresses for
% the seventh etc. author(s) as the argument for the
% \additionalauthors command.
% These 'additional authors' will be output/set for you
% without further effort on your part as the last section in
% the body of your article BEFORE References or any Appendices.

\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor Ronald A. Cardenas\\
       \affaddr{Department of Mechanical Engineering}\\
       \affaddr{Universidad Nacional de Ingenier\'ia}\\
       \affaddr{Lima, Peru}\\
       \email{roncardenas-\\
       acosta@gmail.com}
% 2nd. author
\alignauthor
Kevin S. Bello\\
       \affaddr{Department of Mechanical Engineering}\\
       \affaddr{Universidad Nacional de Ingenier\'ia}\\
       \affaddr{Lima, Peru}\\
       \email{ksbm190493@gmail.com}
% 3rd. author
\alignauthor
Alberto M. Coronado\\
       \affaddr{Department of Mechanical Engineering}\\
       \affaddr{Universidad Nacional de Ingenier\'ia}\\
       \affaddr{Lima, Peru}\\
       \email{am.coronado@gmail.com}
\and  % use '\and' if you need 'another row' of author names
% 4th. author
\alignauthor Elizabeth R. Villota\\
       \affaddr{Section of Mechanical Engineering}\\
       \affaddr{Pontificia Universidad Cat\'olica del Peru}\\
       \affaddr{Lima, Peru}\\
       \email{elvillota@gmail.com}
}

\date{19 June 2015}
% Just remember to make sure that the TOTAL number of authors
% is the number that will appear on the first page PLUS the
% number that will appear in the \additionalauthors section.

\maketitle
\begin{abstract}
Talent shortage is a well-known problem that industry has been dealing with for several years now, with a large number of engineering vacant positions being difficult to fill. We present an analysis of the Peruvian labor market demand based on data collected from job ads available on the web.

We use topic models, a type of mixed-memberships models, in order to infer semantic categories in co-occurrence of words by focusing in the functions, requirements and professions requested in the job position. This information is extracted by applying shallow parsing over the ads before feeding them to the models. Models using the whole text from the ads are compared to models using only the text chunks extracted with shallow parsing.

Our analysis reveal that using the extracted text chunks, some redundant categories are joined while others are splitted into more skill-specific categories. Fine-grained categories observed in models using the whole text are preserved.
\end{abstract}

% A category with the (minimum) three required fields
\category{I.2.7}{Artificial Intelligence}{Natural Language Processing}[Language parsing and understanding, Text analysis]
%A category including the fourth, optional field follows...
\category{K.3.2}{Computers and Education}{Computer and Information Science Education}[curriculum]

\terms{Design, Experimentation}

\keywords{topic modeling, shallow parsing, education engineering}

\section{Introduction}
The economic growth many South American countries have experienced during the last years has boosted the demand of high-specialized job positions, which often require multidisciplinary expertise. Many local employers are having a hard time finding qualified workers, especially in technical fields \cite{manpower}.

For educational institutions around the world, and South America is not the exception, it is not always clear what majors they should offer or what competencies they should emphasize in their curricula \cite{educ1,educ2}. Xue [11] analyzes if there is in fact a ``STEM crisis or a STEM surplus" in the US, reporting that depending on the major and sector, it can be one or another.

This paper contributes an analysis of the Peruvian labor market demand, in an effort to unveil the influence of the academic background in the search for the ideal job applicant, regarding to what skills engineering professionals should have according to the industry.

To model the hidden relationship between academic background and skills required for a specific job vacancy, we extract requirements, functions for the job, and undergraduate background information by applying shallow parsing. The effects of using the above information versus using the whole text in the job ad as input for a carefully tuned topic model, are analyzed.

The machine learning techniques used in this analysis are explained in detail in sections 4 and 5. Section 6 explains the experiments performed and section 7 presents the discussion of results.


\section{Related work}
Mixed membership models, especially topic models, have received a lot of attention in the past few years regarding efficient inference algorithms and proper syntonization of hyperparameters \cite{asuncion}, as well as recommended experiments \cite{handbook}.

Latent Dirichlet Allocation, the initial topic model proposed by Blei et al. \cite{lda} has been extended and modified in several ways. Variations in the graphical model include the correlated model \cite{ctm},  the hierarchical Dirichlet process model \cite{hdp}, the hierarchical topic model \cite{htm}; and the dynamic topic model \cite{dtm}. Gruber et al. \cite{hmm_tm} modifies the original graphical model to include a Markov chain in the topic assignment of words, in the so called Hidden Topic Markov Model, leaving the bag-of-words approach and modeling the document as a Markov chain. As for distributed and parallelized solutions, Smola and  Narayanamurthy \cite{parallel_tm} proposed an architecture for large scale parallel topic modeling with good results.

Airoldi et al. \cite{pnas} analysed the effect of varying the source text and inference strategies for PNAS biological sciences publications. Newman et al. \cite{etm} proposed a customized graphic model which includes named entity words as extra source of text but with its own hyperparameter estimation, directly learning the entity-topic relationship and making better predictions about entities.

We investigate the effect of varying the source text extracting meaningful information with shallow parsing over the resulting latent structure of categories, as well as their quality. Shallow parsing is frequently reduced to sequence labeling problems, and most of the previous work uses machine learning approaches. Most of the work tackles the task of extracting noun phrases with structured predictors \cite{sha:pereira,mcdonald}. Although other chunk types have not received the same attention as NP chunks, Buchholz et al. \cite{bvd99} present excellent results for NP, VP, PP, ADJP and ADVP chunks.


\section{Job ads corpus}
The job ads corpus was built by extracting job ads from several popular job search websites in Peru. The data was extracted from January to March 2015. We consider job ads published only in January, consisting of 60,000 ads, for prototyping and preliminary experiments. Since the same job ad can be published in more than one website, we consider it as repeated if the same description of the position is found within the last fifteen days in the database.
Furthermore, as our data of interest corresponds to engineering positions, our implemented profession recognizer, as described in Section 4, was used to select job ads requesting engineering professions, obtaining roughly 9,000 job ads. Our initial number of engineering majors was 30, however, we discarded majors with less than 50 job ads, ending up with a total of 23 engineering majors.

\section{Shallow Parsing} \label{section:chunks}
The task of extracting requirements, functions and professions from a job ad is reduced to a sequence labeling problem.

Since natural language processing models are very corpus sensitive, a sample of 800 job ads, more than 130 000 words. This amount of data gave good results for shallow parsing (specifically for named entity extraction) in Spanish, as reported by Carreras et al. \cite{carreras}. This subset was manually tagged following the CoNLL-2000 BIO tagging format, initially proposed by Ramshaw and Marcus (1995) \cite{bioformat}, but without annotating part-of-speech (POS) tags. Preliminary experiments showed that POS information does not contribute significantly to chunkers performance. 

The base phrases defined are FUN (functions of the job, VP), REQ (requirements, NP and VP) and CARR (majors, NP). Table \ref{table:chunks} shows the proportion in the annotated corpus for each chunk defined, as well as the average length in words.

\begin{table}
\centering
\caption{Defined chunks and presence in corpus}
\begin{tabular}{|c|c|c|c|} \hline
Chunk&Base phrase&\# of chunks&Avg. word length\\ \hline
FUN&VP&3291&11.09\\ \hline
REQ&NP \& VP&4833&1.84\\ \hline
CARR&NP&2097&1.64\\ \hline
\end{tabular} \label{table:chunks}
\end{table}


\subsection{Data preparation}
Job ads often contain very sparse information like emails, dates, office hours and salary. We treated this type of tokens as noise and replaced them with appropriate tags (e.g. URL) using regular expressions. Low-frequency words were filtered as well, following Bikel et al. \cite{bikel} approach.

\subsection{Averaged Structured Perceptron}
The structured perceptron and its averaged version was initially introduced by Collins, 2002 \cite{collins}. They differ from the well-known perceptron algorithm in that the output for each training instance pair $(x_{t},y_{t}) \in T$ is a structure $y' \in Y_{t}$, where $Y_{t}$ is the space of permissible structured outputs for input $x$. The inference algorithm to predict $y'$ is problem dependent. In our case, sequence labeling, a first order \textit{Viterbi} decoder is used. In each step, the candidate $y'$ is transformed to a high-dimensional feature representation $f(x, y) \in R^{m}$ and the prediction is determined by a linear classifier based on the dot product of this representation and a weight vector $w \in R^{m}$. 

In practice this algorithm can be implemented easily and behaves remarkably well in several problems. These two characteristics make the structured perceptron algorithm a natural first choice for prototyping structured models.

The set of features used in the chunkers are the following.
\begin{itemize}
\item Trigger word features for the current word \cite{carreras}, only for REQ and CARR chunkers.
\item Lowercase form and position of all words in a window of $\pm$n words \cite{carreras}. For the CARR chunker, n=2 and for the other ones n=3.
\item Stemmed form and position of previous, current and next word.
\item Part-of-list feature ($list::y_{i}$), if current word is part of a list.
\item Orthographic features, including long-word and single-digit \cite{carreras}, for previous, current and next word.
\item Suffix and prefix features, last and first 3 characters respectively, for previous, current and next word.
\item Word brown-cluster mapping features \cite{miller} for previous, current and next word.
\item Bigram and trigram emission features \cite{liang} for lowercase and stemmed form of all words, as well as orthographic class, in a windows of $\pm$2 words.
\item Bigram transition features for word cluster mapping \cite{liang}, used only in REQ chunker.
\item Bigram transition features \cite{liang} for lowercase and stemmed form, as well as orthographic class, of each word in the bigram.
\item Relative position of sentence in document, if current sentence belongs to the document border (first one or last two sentences). Only used in FUN chunker.
\end{itemize}


\section{Topic modeling}
\subsection{Latent Dirichlet Allocation}
In this section, we briefly describe the graphical model called Latent Dirichlet Allocation (LDA) \cite{lda},  originally proposed for doing topic modeling. LDA is a generative probabilistic model in which the data is in the form of a collection of documents, and each document in the form of a collection of words. The model assumes that each document is a mixture of latent topics, and each topic is modeled as a mixture of words. These random mixture distributions are considered Dirichlet-distributed to be inferred from the data. The generative process of LDA can be described as follow:
\begin{enumerate}
\item For all $D$ documents sample  $\theta_{d} \sim Dir(\alpha)$.
\item For all $T$ topics sample  $\phi_{t} \sim Dir(\beta)$.
\item For each of the $N_{d}$ words $\upsilon_{i}$ in document $d$:
\begin{itemize}
\item Sample a topic $z_{i} \sim Multinomial(\theta_{d})$
\item Sample a word $\upsilon_{i} \sim Multinomial(\phi_{z_{i}})$
\item Observe the word
\end{itemize}
\end{enumerate}

We assume symmetric Dirichlet priors for $\theta_{d}$ and $\phi_{t}$, as suggested by  Griffiths and Steyvers \cite{griffiths}.

Nowadays, in the literature there are several learning algorithms for LDA. Regarding inference strategies for the models, we make use of Gibbs Sampling as described in \cite{griffiths} and the Variational Expectation - Maximization (VEM) algorithm as described in \cite{lda}.


\subsection{Experiment configuration} \label{section:setup}
We compare six LDA models in a 2x3 layout, as summarized in Table \ref{table:models}. This analysis approach, suggested by Airoldi et al. \cite{pnas}, aims to explore the effect over model dimensionality, of varying the data source (all the text from the ad versus text extracted by the chunkers) and using different hyperparameters inference strategies.

We explore models both estimating and fixing the latent categories proportions per document hyperparameter ($\alpha$). When estimating $\alpha$, the initial value is set to 5/K, as suggested by Griffiths and Steyvers \cite{griffiths}. Once a value of K in which the global or a local minimum is spotted for the estimated $\alpha$ case, a grid search over $\alpha$ is performed for that value of K in order to find the best $\alpha$ for the fixed-case. This strategy follows the conclusion that the VEM inference algorithm estimates too low $\alpha$ hyperparameters, as reported by Asuncion et al. \cite{asuncion}. Low $\alpha$ hyperparameters cause the model to assign few topics per document, only one in the worse case.

\begin{table}
\centering
\caption{Mixed-membership models in the analysis}
\begin{tabular}{|c|m{2cm}|m{1.7cm}|m{2cm}|} \hline
&VEM with estimated alpha&VEM with fixed alpha&Gibbs with estimated alpha\\ \hline
Full text&model 1&model 2&model 3\\ \hline
Text chunks&model 4&model 5&model 6\\ \hline
\end{tabular} \label{table:models}
\end{table}


\subsection{Dimensionality selection} \label{section:dimsel}
Each time we fit a mixed-membership model to data, we must specify the number of latent categories, K, in the model. The goal of model selection is to find K*, the number of latent categories that is optimal in some sense. We use 10-fold cross-validation following the approach described in \cite{pnas}, and widely used in other machine learning applications. First, we split the N job ads into 10 batches. Then, we estimate the model parameters using the ads in nine batches, and we calculate the posterior perplexity of the ads in the tenth held-out batch. This approach leads to summarize how good a model fits for a given K, on a batch of ads not included in the estimation. We consider a wide range of number of latent categories as follows: increments of 5 for 5$\leq$K$\leq$10, increments of 10 for 60$\leq$K$\leq$120, and increments of 50 for 150$\leq$K$\leq$200. Thus, we fit the models a total of 60 times (10 times in cross-validation for each of 6 models) for each of 24 values of K.

\section{Experiments}
\subsection{Shallow Parsing}
The annotated dataset is divided in 70, 15 and 15 percent for training, validation and testing, respectively. The standard evaluation metrics for a chunker are precision P (fraction of output chunks that exactly match the reference chunks), recall R (fraction of reference chunks returned by the chunker), and their harmonic mean, the $F_{1}$ score $F_{1} = 2 \times P \times R/(P+R)$. The accuracy rate for individual labeling decisions is over-optimistic as an accuracy measure for shallow parsing, given that O labels are more frequent. Even so, we report BIO accuracy for reference.

Table \ref{table:res_chunks} shows results for the chunkers. It can be observed that CARR chunker shows the best performance. This can be explained by the fact that majors are mostly mentioned in determined word patterns in job ads.
For the FUN chunker, taking advantage of the fact that functions are not mentioned in the beginning nor the end of the ad improves the precision significantly in comparison to early experiments. In addition, FUN chunks mostly appear at the beginning of the sentences.

\begin{table}
\centering
\caption{Feature set sizes and chunkers' performance}
\begin{tabular}{|c|c|c|c|c|c|} \hline
Chunker & \# Feat. & P    & R   & $F_{1}$ &BIO Acc.\\ \hline
FUN     & 503701  & 61.1 & 62.3& 61.7    &93.4\\ \hline
REQ     & 605864  & 77.6 & 55.9& 65.0    &97.1\\ \hline
CARR    & 215143  & 87.2 & 86.9& 87.0    &99.5\\ \hline
\end{tabular} \label{table:res_chunks}
\end{table}

\subsection{Topic modeling}
\subsubsection{Dimensionality} \label{section:dimensionality}
Following the procedure described in Section \ref{section:setup}, first, we compute the average held-out perplexity for each of the different values of the number of latent categories, as mentioned in Section \ref{section:dimsel}, for the models with the hyperparameter $\alpha$ estimated (lines red and green in Figure \ref{fig:dimsel}). Then, as can be observed in Figure \ref{fig:dimsel}, among the green and red lines, only the red line (bottom) has a local minimum with the number of latent of categories set to 26. We use this local minimum to find an optimal value for the hyperparameter $\alpha$, as shown in Figure \ref{fig:alphasel}. Finally, we use this optimal value of the hyperparameter to reveal the behavior of the models with fixed hyperparameters (blue lines in Figure \ref{fig:dimsel}).
 
Although the blue line (bottom) in Figure \ref{fig:dimsel} has a global minimum with the number of latent categories set to 36, we use the local minimum found in the red line (bottom) with K set to 26. The latter because the number of engineering majors in Peru is less than 36, and according to our criterion, specializations converge to a mixture of common pools of knowledge and required skills. Therefore, we think that a value of 26 is a more reasonable choice.


\begin{figure}
\centering
\epsfig{file=dimsel.eps}
\caption{Average held-out perplexity as a function of the number of latent categories K for whole text models 1, 2 and 3 (top), and text chunks models 4, 5 and 6 (bottom).} \label{fig:dimsel}
\end{figure}

\begin{figure}
\centering
\epsfig{file=alphasel.eps}
\caption{Average held-out perplexity as a function of $\alpha$, for mixed-membership models using all the words from the ad (red) and using words from the extracted chunks (blue). Both models were fitted by using a number of latent categories set to 26.} \label{fig:alphasel}
\end{figure}

\subsubsection{Qualitative and quantitative analysis of inferred categories}
Regarding the dimensionality analysis, we set K to 26 as explained in Section \ref{section:dimensionality} for the analysis of inferred categories, for every LDA inference strategy.

Latent categories are explored examining high probability words in each category (Tables \ref{table:case1}, \ref{table:case2} and \ref{table:case3}). In addition, the latent category proportion in each major is investigated. For each major, the mean of posterior membership scores of all documents where this major was found is taken (Figure \ref{fig:majortopics}), as proposed by Erosheva et al. \cite{erosheva:mm}. An inspection to the latter plot provides an idea of how related the engineering majors are to one another by observing for each topic the majors that have the most vivid colors.

Furthermore, we show in Figure \ref{fig:mmhell} matrices for the six mixed-membership models, which represent the similarity of the probability distributions over categories between all majors. This similarity is calculated using Hellinger distance. Each row and column of each matrix represent a professional major and its similarity with other majors, regarding the text source and inference strategy applied. Major names are not shown because each matrix has different major names order in rows and columns. The purpose of Figure \ref{fig:mmhell} is to unveil the effect of how professional majors are grouped.

In both graphics (Figures \ref{fig:majortopics} and \ref{fig:mmhell}), it can be observed that for the case of the text chunks models (models 4,5 and 6), getting rid of irrelevant words (ignored by the chunkers) has the effect of smoothing the probability distribution over topics. For instance, in Figure 3, for whole-text models 1, 2 and 3, the job ads for agricultural engineering basically just talks about one topic.

On the other hand, for text-chunk models 4, 5 and 6, the major now talks about more than one topic with similar proportions.  This phenomenon also appears in the Hellinger matrices (Figure \ref{fig:mmhell}) in which by using words from all the text causes the majors have high proportions in few topics. Thus the plot ends up with lots of blue squares which means there is no relationship between the two majors. Whereas for the case of using words from the extracted chunks the topic proportions are more uniformly distributed. Thus the plot contains less blue squares and reveal more significant relationships between the majors.


\begin{figure*}
\centering
\epsfig{file=major_vs_topics.eps}
\caption{Estimated average membership of engineering majors in the 26 latent categories for mixed-membership models 1-6 in Table \ref{table:models}.} \label{fig:majortopics}
\end{figure*}


\begin{figure*}
\centering
\epsfig{file=mmhell.eps}
\caption{Similarity matrices using Hellinger distance between discrete distributions (categories proportion over majors), for each of the six mixed-membership models in Table \ref{table:models}.} \label{fig:mmhell}
\end{figure*}

A closer look at Figure \ref{fig:majortopics} allows to spot three main categories behaviors under the effect of variation of source text (whole text versus text chunks), as follows.

\begin{itemize}
\item Joining of redundant categories\\
Consider the majors of Mechanics and Mechatronics Engineering for models with VEM inference algorithm with fixed alpha (models 2 and 5). In Figure \ref{fig:majortopics}, for whole-text model 2, categories 21 and 23 are predominant and have almost the same high category proportion. It can be seen in Table \ref{table:case1}  that these categories are technically equivalent.

On the other hand, for text-chunk model 5, it can be seen that only category 12 is predominant in these majors. Table \ref{table:case1} confirms that its content is the combination of the categories separated in the whole text model.
The same behaviour is observed when exploring Mining and Geological Engineering, where categories 3 and 10 for model 2 are combined into category 21 in model 5.

\item Splitting in two or more detailed categories\\
Consider the major of Geological Engineering for models with VEM inference algorithm with estimated alpha (models 1 and 4). In Figure \ref{fig:majortopics} for whole-text model 1, category 15 is predominant in the major. Exploration of this category revealed that its contents are related to personnel and risk management, as can be appreciated in Table \ref{table:case2}.
On the other hand, for text-chunk model 4, it can be observed that categories 19 and 26 are predominant and with almost the same proportion. A closer exploration revealed that category 19 is related to personnel management and 26 to risk management, i.e. category 15 in the previous model was splitted.

\item Persistence of latent structure\\
There are cases where the number of predominant categories does not change. Consider the majors of Systems and Informatics Engineering for the models with Gibbs sampling (3 and 6). For whole-text model 3, it can be observed that categories 5 and 23 are predominant and with almost the same proportion. Likewise, for text-chunk model 6, categories 5 and 26 present the same behaviour. Table \ref{table:case3} shows that the content of these categories is maintained in both models.
\end{itemize}

The behaviour of categories over Industrial Engineering deserves closer attention. It can be observed that in all the analysed models, the latent structure of categories over this major persist, i.e. their proportions over this major present low variance. This can be interpreted as the fact that knowledge and skills, once exclusive of this major, are nowadays required for most majors and areas.

\begin{table}
\centering
\caption{Categories behaviour: joining of redundant categories}
\begin{tabular}{|c|c|c|} \hline
\multicolumn{2}{|m{45mm}|}{VEM with fixed $\alpha$ \& whole text (model 2)} & \multicolumn{1}{m{25mm}|}{VEM with fixed $\alpha$ \& text chunks (model 5)}\\ \hline
Cat.21 & Cat.23 & Cat.12 \\ \hline
maintenance & maintenance & maintenance \\
technician & technician & mechanic \\
mechanic & industrial & electricity \\
garage & plant & electronic \\
machinery & mechanic & industrial \\
repair & production & electric \\
replacement & control & plant \\
service & electricity & supervisor \\
customer & machinery & machinery \\
preventive & electronic & replacement \\ \hline
\end{tabular} \label{table:case1}
\end{table}

\begin{table}
\centering
\caption{Categories behaviour: splitting in two or more detailed categories}
\begin{tabular}{|c|c|c|} \hline
\multicolumn{1}{|m{25mm}}{VEM with estimated $\alpha$ \& whole text (model 1)} & \multicolumn{2}{|m{45mm}|}{VEM with estimated $\alpha$ \& text chunks (model 4)} \\ \hline
Cat.15 & Cat.19 & Cat.26 \\ \hline
assurance & management & assurance \\
management & specialization & environmental \\
industrial & system & industrial \\
occupational & entitled & occupational \\
standard & coordinator & management \\
risk & iso & standard \\
iso & industrial & risk \\
supervisor & mastery & prevention \\
procedure & operation & sanitation \\
prevention & manager & incident \\ \hline
\end{tabular} \label{table:case2}
\end{table}


\begin{table*}
\centering
\caption{Categories behaviour: persistence of latent structure}
\begin{tabular}{|c|c|c|c|} \hline
\multicolumn{2}{|m{40mm}|}{Gibbs with estimated $\alpha$ \& whole text (model 3)} & \multicolumn{2}{m{40mm}|}{Gibbs with estimated $\alpha$ \& text chunks (model 6)} \\ \hline
Cat.5 & Cat.23 & Cat.5 & Cat.26 \\ \hline
system & technician & system & system \\
development & system & informatic & analyst \\
analyst & informatic & computer & programmer \\
sql & computer & electronic & sql \\
programmer & configuration & windows & informatic \\
programming & windows & communication & programming \\
informatic & user & operative & server \\
java & service & office & java \\
server & installation & technician & oracle \\
application & electronic & configuration & visual \\ \hline
\end{tabular} \label{table:case3}
\end{table*}



\section{Conclusions}
Throughout the analysis of multiple variants of mixed-membership models, consistent results support the fact that quality of inferred categories significantly improves when using only text extracted by shallow parsing rather that the whole document. In our case study, the relevant text constitutes academic background and skills required in job ads.

Compared to categories inferred using whole-text models, text-chunk models generate categories that join redundant ones and split to high skill-specific categories. In addition, fine-grained categories are preserved with text-chunk models.

A more in-depth study of semantic patterns, inferred from bigger job ads data (e.g. a whole year time span) could lead to reveal connections that get closer to the way industry treats engineering majors in reality. In addition, these results could be used by educational institutions to train professionals in what the labor market really demands. And help professionals to build a more successful career path in industry.

%\end{document}  % This is where a 'short' article might terminate


% The following two commands are all you need in the
% initial runs of your .tex file to
% produce the bibliography for the citations in your paper.
\bibliographystyle{abbrv}
\bibliography{mapaper}  % sigproc.bib is the name of the Bibliography in this case
% You must have a proper ".bib" file
%  and remember to run:
% latex bibtex latex latex
% to resolve all references
%
% ACM needs 'a single self-contained file'!
%

\end{document}
