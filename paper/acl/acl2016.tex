%
% File acl2016.tex
%
%% Based on the style files for ACL-2015, with some improvements
%%  taken from the NAACL-2016 style
%% Based on the style files for ACL-2014, which were, in turn,
%% Based on the style files for ACL-2013, which were, in turn,
%% Based on the style files for ACL-2012, which were, in turn,
%% based on the style files for ACL-2011, which were, in turn, 
%% based on the style files for ACL-2010, which were, in turn, 
%% based on the style files for ACL-IJCNLP-2009, which were, in turn,
%% based on the style files for EACL-2009 and IJCNLP-2008...

%% Based on the style files for EACL 2006 by 
%%e.agirre@ehu.es or Sergi.Balari@uab.es
%% and that of ACL 08 by Joakim Nivre and Noah Smith

\documentclass[11pt]{article}
\usepackage{acl2016}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{subcaption}

%\aclfinalcopy % Uncomment this line for the final submission
%\def\aclpaperid{***} %  Enter the acl Paper ID here

%\setlength\titlebox{5cm}
% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.

\newcommand\BibTeX{B{\sc ib}\TeX}

\title{Improving topic coherence over job ads restricting corpora to extracted job requirements}

\author{First Author \\
  Affiliation / Address line 1 \\
  Affiliation / Address line 2 \\
  Affiliation / Address line 3 \\
  {\tt email@domain} \\\And
  Second Author \\
  Affiliation / Address line 1 \\
  Affiliation / Address line 2 \\
  Affiliation / Address line 3 \\
  {\tt email@domain} \\}

\date{}

\begin{document}
\maketitle
\begin{abstract}
Topic interpretability measures have been developed in recent years as a more natural option for topic quality evaluation, emulating human perception of coherence with word sets correlation scores. We propose a way of improving topic coherence score by restricting the training corpus to that of relevant information in the document in the context of job advertisement data. We find that using only the requirements for the job, previously extracted by a shallow parser, allows the topic model to improve interpretability in about 40 percentual points in average.
Our analysis reveals as well that using the extracted text chunks, some redundant topics are joined while others are splitted into more skill-specific topics. Fine-grained topics observed in models using the whole text are preserved.
\end{abstract}


\section{Introduction}

Probabilistic topic models , such as Latent Dirichlet Allocation \cite{lda} and its many variants \cite{etm,ctm,dtm,hdp,htm}, were introduced in an unsupervised setting to discover latent semantic structures in a collection of documents, namely the topics. However, there is no guarantee that the inferred topics -typically modeled as a set of important words- present human interpretability. Traditionally, held-out likelihood had been used to perform topic model evaluation. \newcite{chang} conducted a study that showed that perplexity actually correlates negatively with human interpretability of such topics.

In recent years, several topic coherence measures have been proposed \cite{Newman:2010,Musat:2011,Mimno:2011,Stevens:2012,Aletras:2013,Lau:2014} in order to automate the method of \newcite{chang} and emulate human interpretability. 
\newcite{Newman:2010} introduced the notion of coherence and was the first to propose an automatic measure based on pairwise pointwise mutual information (PMI) between the topic words. Subsequent empirical works  on topic coherence proposed measures based on word statistics that differ in several details, such as normalization \cite{Lau:2014}, aggregation methods \cite{Mimno:2011}, and reference corpus \cite{Musat:2011,Aletras:2013}.
\newcite{Roder:2015} proposed a framework for the exploration of all possible coherence measures, modeled as a pipeline where the blocks (e.g. aggregation method, confirmation measure) can be exchanged and create new measures. They combined two complementary lines of research on coherence: scientific coherence and topic modeling.

As the acceptance of topic coherence measures increases as a mean of topic model assessment \cite{Paul:2010,Reisinger:2010,Hall:2012}, recent research trends focus on proposing fast and efficient enough model that can be scaled up to big amounts of data \cite{Yang:2015,Nguyen:2015}, using the whole text per document for training.

In this work we propose a pipeline system that increase topic coherence scores by training a topic model over text chunks of relevant information instead of the whole text per document. This chunks are extracted using shallow parsing and named entity recognition. We employ job advertisement posts published in several Peruvian job-hunting websites. In our analysis, the chunks of relevant information are composed of requirements and responsibilities of the job, as well as professional major of preference. Our system successfully increases human-interpretability of inferred topics. A qualitatively analysis of the topics reveals that redundant topics are joined and fine-grained new ones are created.

This paper is organized as follows. The next section briefly review relevant related work. Section \ref{section_data} presents details about the corpus used for training and reference corpus used for coherence metrics. Section \ref{section_shapar} details the shallow parser. Section 5 explains the pipeline and experiment configuration. Section 6 shows the results and section 7 presents the conclusions.


\section{Related Work}
In the past few years, topic models, especially Latent Dirichlet Allocation \cite{lda} and its variants, have received high attention regarding efficient inference algorithms and proper syntonization of hyperparameters \cite{asuncion}, as well as recommended experiments \cite{handbook}.

In order to incorporate the evaluation of semantic coherence into the topic model itself, \newcite{Mimno:2011} proposed a model that record words that co-occur frequently, and update the associated word counts before and after sampling a new topic assignment. This variant was shown to produce more coherent topics than LDA based on their also proposed coherence measure, a log conditional probability of word co-occurrence in top topic words.

Regarding model performance comparison using different text sources for training, \newcite{pnas} analyzed the effect of using only abstracts against using abstracts and bibliography  of PNAS biological sciences publications. They experimented varying inference algorithms as well, obtaining a carefully tuned model that gave comprehensible number of categories and reasonable interpretable topics, even when all their evaluation was based in held-out log likelihood.

In regard to modeling explicitly name entities in the topic model, \newcite{etm} proposed a customized graphical model that includes named entity words as extra source of training text but with their own hyperparameter estimation, aiming to learn directly the entity-topic relationship and make better predictions about entities.

On the other hand, the analysis and extraction of noun and verb phrases, known as shallow parsing, is frequently reduced to sequence labeling problems. Most of the work tackles the task of extracting noun phrases with structured predictors \cite{Sha:2003,Mcdonald:2005}. Although other chunk types have not received the same attention as NP chunks, \cite{bvd99} presented the first good results for NP, VP, PP, ADJP and ADVP chunks.


\section{Dataset: Job ads corpus}
\label{section_data}
The job ads corpus consists of more than 500,000 job advertisement posts in Spanish, extracted from several popular job hunting websites from South American countries  during 2014 and 2015. For our analysis, we focus in ads related to engineering positions published between January and March 2015 in Peru, consisting of roughly 9000 job ads.
The complete dataset as well as the sample used for our experiments, including labeled data for the shallow parser, is publicly available in 

\section{Shallow Parsing}
\label{section_shapar}
The task of extracting requirements, functions and professional majors from a job ad is reduced to a sequence labeling problem.

We manually annotate a sample of 800 job ads (more than 130,000 words) following the CoNLL-2000 BIO tagging format, initially proposed by \newcite{bioformat}, but without annotating part-of-speech (POS) tags. Preliminary experiments showed that POS information does not contribute significantly to chunker performance. 
This amount of data gave good enough results for shallow parsing (specifically for named entity extraction) in Spanish, as reported by \newcite{carreras}.

The base phrases defined are FUN (functions, VP), REQ (requirements, NP and VP) and CARR (professional majors, NP), as detailed in Table \ref{table:chunks}. Figure \ref{fig:tagging} shows an annotation example (translated from Spanish) for professional major chunk (CARR) and job requirement (REQ).

\begin{table}
\centering
\caption{Defined chunks and presence in corpus}
\begin{tabular}{|c|p{15mm}|c|p{17mm}|} \hline
Chunk&Base phrase&\# of chunks&Avg. num. words\\ \hline
FUN&VP&3291&11.09\\ \hline
REQ&NP \& VP&4833&1.84\\ \hline
CARR&NP&2097&1.64\\ \hline
\end{tabular} \label{table:chunks}
\end{table}


\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{figures/tagging.eps}
\caption{Annotation example for an excerpt of job ad.} \label{fig:tagging}
\end{figure}


\subsection{Data Preparation}
Job ads often contain very sparse information like emails, dates, office hours and salary. We treated this information as noise and replaced them with appropriate tags (e.g. URL, EMAIL) using regular expressions. Low-frequency words were filtered as well, following \newcite{bikel} approach.


\subsection{Averaged Structured Perceptron}
The structured perceptron and its averaged version was initially introduced by \newcite{collins}. They differ from the well-known perceptron algorithm in that the output for each training instance pair $(x_{t},y_{t}) \in T$ is a structure $y' \in Y_{t}$, where $Y_{t}$ is the space of permissible structured outputs for input $x$. The inference algorithm to predict $y'$ is problem dependent. In our case, sequence labeling, a first order \textit{Viterbi} decoder is used. In each step, the candidate $y'$ is transformed to a high-dimensional feature representation $f(x, y) \in R^{m}$ and the prediction is determined by a linear classifier based on the dot product of this representation and a weight vector $w \in R^{m}$. 

In practice this algorithm can be implemented easily and behaves remarkably well in several problems. These two characteristics make the structured perceptron algorithm a natural first choice for prototyping structured models.

The set of features used in the chunkers are the following, extending those proposed by \newcite{carreras,miller,liang}.

\begin{itemize}
\item Trigger word features for the current word, only for REQ and CARR chunkers.
\item Lowercase form and position of all words in a window of $\pm$n words. For the CARR chunker, n=2 and for the other ones n=3.
\item Stemmed form and position of previous, current and next word.
\item Part-of-list feature ($list::y_{i}$), if current word is part of a list.
\item Orthographic features, including long-word and single-digit, for previous, current and next word.
\item Suffix and prefix features, last and first 3 characters respectively, for previous, current and next word.
\item Word brown-cluster mapping features for previous, current and next word.
\item Bigram and trigram emission features for lowercase and stemmed form of all words, as well as orthographic class, in a windows of $\pm$2 words.
\item Bigram transition features for word cluster mapping, used only in REQ chunker.
\item Bigram transition features for lowercase and stemmed form, as well as orthographic class, of each word in the bigram.
\item Relative position of sentence in document, if current sentence belongs to the document border (first one or last two sentences). Only used in FUN chunker.
\end{itemize}


\section{Topic modeling}
\subsection{Latent Dirichlet Allocation}
In this section, we briefly describe the graphical model called Latent Dirichlet Allocation (LDA) \cite{lda},  originally proposed for doing topic modeling. LDA is a generative probabilistic model in which the data is in the form of a collection of documents, and each document in the form of a collection of words. The model assumes that each document is a mixture of latent topics, and each topic is modeled as a mixture of words. These random mixture distributions are considered Dirichlet-distributed to be inferred from the data. The generative process of LDA can be described as follow:
\begin{enumerate}
\item For all $D$ documents sample $\theta_{d} \sim Dir(\alpha)$.
\item For all $T$ topics sample $\phi_{t} \sim Dir(\beta)$.
\item For each of the $N_{d}$ words $\upsilon_{i}$ in document $d$:
\begin{itemize}
\item Sample a topic $z_{i} \sim Multinomial(\theta_{d})$
\item Sample a word 

$\upsilon_{i} \sim Multinomial(\phi_{z_{i}})$
\item Observe the word
\end{itemize}
\end{enumerate}

We assume symmetric Dirichlet priors for $\theta_{d}$ and $\phi_{t}$, as suggested by \newcite{griffiths}.

Nowadays, in the literature there are several learning algorithms for LDA. Regarding inference strategies for the models, we make use of Gibbs Sampling as described in \newcite{griffiths} and the Variational Expectation - Maximization (VEM) algorithm as described in \newcite{lda}. 

For our experiments, we use the LDA R library \textit{topicmodels} by \newcite{topicmodelsR}, which wraps \newcite{lda} C code for VEM inference and \newcite{gibbs_cpp} C++ code for Gibbs sampling.

\subsection{Topic coherence}
We use the coherence metric proposed by \newcite{Mimno:2011}, based in conditional log likelihood of co-occurrence of top topic word pairs. We refer to it as \textit{UMass} coherence from now on. It is defined as follows.

$$ C_{UMass} = \frac{2}{N \cdot (N-1)} \sum_{i=2}^{N} \sum_{j=1}^{i-1} \log \frac{P(w_i, w_j) + \epsilon}{P(w_j)} $$
Where $N$ is the number of top words in a topic to consider. Following the literature \cite{chang,Mimno:2011,Aletras:2013,Lau:2014}, we employ the top 10 words by topic.

In our coherence experiments, we use the framework proposed by \newcite{Roder:2015}, available online\footnote{https://github.com/AKSW/Palmetto}, in which much more scores are available and a reference corpus for probability counts can be specified.  Although \newcite{Mimno:2011} doesn't use any external reference corpus, \newcite{Roder:2015} showed that using Wikipedia as an additional reference corpus improved correlation with gold human ratings for this metric. Following this setup, we combine the entire Job Ads dataset (more than 500,000 documents) and Spanish Wikipedia corpus as reference corpus to be used by the framework.


\subsection{Experiment configuration}
\label{section:setup}
We compare six LDA models in a 2x3 layout, as summarized in Table \ref{table:models}. This analysis approach, suggested by \newcite{pnas}, aims to explore the effect over model dimensionality, of varying the data source (all the text from the ad versus text extracted by the chunkers) and using different hyperparameters inference strategies.
We explore models both estimating and fixing the topic proportions per document ($\alpha$) hyperparameter. When estimating $\alpha$, the initial value is set to 5/K, as suggested by \newcite{griffiths}. Once a value of K in which the global or a local minimum is spotted for the estimated $\alpha$ case, a grid search over $\alpha$ is performed for that value of K in order to find the best $\alpha$ for the fixed-case. This strategy follows the conclusion that the VEM inference algorithm estimates too low $\alpha$ hyperparameters, as reported by \newcite{asuncion}. Low $\alpha$ hyperparameters cause the model to assign few topics per document, only one in the worse case.

\begin{table}
\centering
\caption{LDA models in the analysis}
\begin{tabular}{|p{12mm}|p{15mm}|p{15mm}|p{15mm}|} \hline
&VEM estimated alpha&VEM fixed alpha&Gibbs estimated alpha\\ \hline
Full text&model 1&model 2&model 3\\ \hline
Text chunks&model 4&model 5&model 6\\ \hline
\end{tabular} \label{table:models}
\end{table}

\subsection{Dimensionality selection}
\label{section:dimsel}
Each time we fit a mixed-membership model, we must specify the number of topics, K, in the model. The goal of model selection is to find K*, the number of topics that is optimal in some sense. When using perplexity as a measure, we use 10-fold cross-validation following the approach described in \newcite{pnas}, and widely used in other machine learning applications. First, we split the N job ads into 10 batches. Then, we estimate the model parameters using the ads in nine batches, and we calculate the posterior perplexity of the ads in the tenth held-out batch. This approach leads to summarize how good a model fits for a given K, on a batch of ads not included in the estimation. When using the \textit{UMass} topic coherence measure, we do not split the job ads and just process them as one batch. In this case, for a given K we calculate the score of the model by averaging the individual topic scores. We consider a wide range of number of topics for both perplexity and topic coherence as follows: increments of 5 for 5$\leq$K$\leq$10, increments of 10 for 60$\leq$K$\leq$120, and increments of 50 for 150$\leq$K$\leq$200. Thus, we fit the models a total of 66 times (10 times in cross-validation and once for topic coherence for each of 6 models) for each of 24 values of K.

\section{Experiments and results}
\subsection{Shallow Parsing}
The annotated dataset is divided in 70, 15 and 15 percent for training, validation and testing, respectively. The standard evaluation metrics for a chunker are precision P (fraction of output chunks that exactly match the reference chunks), recall R (fraction of reference chunks returned by the chunker), and their harmonic mean, the $F_{1}$ score $F_{1} = 2 \times P \times R/(P+R)$. The accuracy rate for individual labeling decisions is over-optimistic as an accuracy measure for shallow parsing, given that O labels are more frequent. Even so, we report BIO accuracy for reference.

Table \ref{table:res_chunks} shows results for the chunkers. It can be observed that CARR chunker shows the best performance. This can be explained by the fact that majors are mostly mentioned in determined word patterns in job ads.
For the FUN chunker, taking advantage of the fact that functions are not mentioned in the beginning nor the end of the ad improves the precision significantly in comparison to early experiments. In addition, FUN chunks mostly appear at the beginning of the sentences.

\begin{table}
\centering
\caption{Feature set sizes and chunkers' performance}
\begin{tabular}{|c|c|c|c|c|p{8mm}|} \hline
Chunker & \# Feat. & P    & R   & $F_{1}$ &BIO Acc.\\ \hline
FUN     & 503701  & 61.1 & 62.3& 61.7    &93.4\\ \hline
REQ     & 605864  & 77.6 & 55.9& 65.0    &97.1\\ \hline
CARR    & 215143  & 87.2 & 86.9& 87.0    &99.5\\ \hline
\end{tabular} \label{table:res_chunks}
\end{table}

\subsection{Topic modeling}
\subsubsection{Dimensionality}
\label{section:dimensionality}
Following the procedure described in section \ref{section:setup} and \ref{section:dimsel}, we show in Figure \ref{fig:dimsel_perp} the behavior of the held-out perplexity as the number of topics changes. We observe that in general there is no agreement among the methods of inference for the optimal number of topics and that in some cases the perplexity does not converge. 

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{figures/dimsel_perp.eps}
\caption{Average held-out perplexity as a function of the number of latent categories K for whole text models 1, 2 and 3 (top), and text chunks models 4, 5 and 6 (bottom).} \label{fig:dimsel_perp}
\end{figure}

Using the \textit{UMass} topic coherence score to measure the quality of the models as the number of topics changes, we observe in Figure \ref{fig:dimsel_coh} that for each method of inference, the optimal number of topics is found between 5 and 18. Being 10 the optimal value we choose for both models, as it appears twice as the optimal number of topics for the case of using text chunks (right) and is an intermediate value of the optimal number of topics when using the whole text (left).

\begin{figure}
\centering
	\begin{subfigure}[b]{0.52\textwidth}
		\includegraphics[width=\textwidth]{figures/full_coh_dimsel.eps}
	\end{subfigure}
	
	\begin{subfigure}[b]{0.52\textwidth}
		\includegraphics[width=\textwidth]{figures/ne_coh_dimsel.eps}
	\end{subfigure}
\caption{Average \textit{UMass} coherence score as a function of the number of topics K for whole text models 1, 2 and 3 (top), and text chunks models 4, 5 and 6 (bottom).} \label{fig:dimsel_coh}
\end{figure}

\subsubsection{Topic coherence improvement}
For the optimal number of topics chosen in section \ref{section:dimensionality}, 10, the bar plot in Figure \ref{fig:increase_coh} shows the improvement of the UMass topic coherence when restricting the text to the chunks extracted by the shallow parser. Also, it can be observed that this happens independently of the method of inference, and that there is at least an improvement of 40\% in each case, with \textit{VEM estimated alpha} having the better coherence score when text chunks are used.

\begin{figure}
\centering
\includegraphics[width=0.53\textwidth]{figures/inc_coh.eps}
\caption{Comparison of the UMass coherence score for each method of inference, the darker bars represents the text chunks models and the lighter bars the full text models.} \label{fig:increase_coh}
\end{figure}

\subsubsection{Qualitative analysis of inferred topics}
Topics are explored by examining the top 10 words (Tables \ref{table:case1}, \ref{table:case2} and \ref{table:case3}). In addition, the topic proportion for each professional major is investigated. For each major, the mean of posterior membership scores of all documents where this major was required is taken, as proposed by \newcite{erosheva:mm}. Figure \ref{fig:major_vs_topic} shows this calculation for VEM inference method with fixed alpha. An inspection to the latter plot provides an idea of how related the engineering majors are to one another by observing for each topic the majors that have the most vivid colors.

Furthermore, in Figure \ref{fig:major_vs_topic} can be observed that for the case of the text chunks model, getting rid of irrelevant words (ignored by the chunkers) has the effect of smoothing the probability distribution over topics. For instance, for the full text model, the job ads for environmental engineering basically just talks about one topic. On the other hand, for the text chunks model, the major now talks about more than one topic with similar proportions.

\begin{figure*}
\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/full_mt.eps}
		\caption{Whole text}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/ne_mt.eps}
		\caption{Text chunks}
	\end{subfigure}
\caption{Comparison of the effect on the estimated average membership of engineering majors in the 10 topics, for VEM inference with fixed alpha. Whole text model (left), text chunks models (right).} \label{fig:major_vs_topic}
\end{figure*}

A closer look at Figure \ref{fig:major_vs_topic} allows to spot three main behaviors under the effect of restricting the source text (whole text versus text chunks).

\begin{itemize}
\item Joining of redundant categories\\
Consider the major of Electronic Engineering. In Figure \ref{fig:major_vs_topic} for the whole text model, topics 4 and 7 are the predominant ones. See Table \ref{table:case1} for the content of the topics. On the other hand, for the text chunks model, it can be seen that only topic 5 is predominant. Table \ref{table:case1} confirms that topic 5 of the text chunks model contains words (with high probability) from both of the topics of the whole text model.

\item Splitting in two or more detailed categories\\
Consider the majors of Environmental Engineering and Industrial Hygiene and Safety. In Figure \ref{fig:major_vs_topic} for the whole text model, topic 2 is predominant for both majors. Exploration of this topic reveals that its content is related to industrial, environmental safety and management, as can be appreciated in Table \ref{table:case2}. On the other hand, for the text chunks model, it can be observed that categories 2 and 10 are predominant and with almost the same proportion. A closer exploration reveals that topic 2 is related to environmental safety and management but no longer contains the word industrial, which appears in topic 10, i.e. the top two words from topic 2 (whole text model) was splitted.

\item Persistence of latent structure\\
There are cases where the number of predominant topics does not change. Consider the majors of Systems and Informatics Engineering. For the whole text model, it can be observed that topic 4 is predominant. Likewise, for the text chunks model, topic 9 present the same behaviour. Table \ref{table:case3} shows that the content of these topics is maintained in both models.
\end{itemize}


\begin{table}
\centering
\caption{Topics behaviour: joining of redundant categories}
\begin{tabular}{|c|c|c|} \hline
\multicolumn{2}{|p{45mm}|}{Whole text (model 2)} & \multicolumn{1}{p{25mm}|}{Text chunks (model 5)}\\ \hline
Topic 4 & Topic 7 & Topic 5 \\ \hline
systems	&	technician	&	maintenance	 \\
technician	&	maintenance	&	mechanical	 \\
informatics	&	mechanical	&	electronics	 \\
development	&	electrical	&	electrical	 \\
computation	&	electricity	&	electricity	 \\
sql	&	industrial	&	technician	 \\
programmer	&	preventive	&	installation	 \\
analyst	&	electronics	&	repair	 \\
programming	&	systems	&	preventive	 \\
server	&	installation	&	systems	 \\ 
\hline
\end{tabular} \label{table:case1}
\end{table}

\begin{table}
\centering
\caption{Topics behaviour: splitting in two or more detailed categories}
\begin{tabular}{|c|c|c|} \hline
\multicolumn{1}{|p{25mm}}{Whole text (model 2)} & \multicolumn{2}{|p{45mm}|}{Text chunks (model 5)} \\ \hline
Topic 2 & Topic 2 & Topic 10 \\ \hline
safety	&	safety	&	industrial	 \\
industrial	&	risk	&	supervisor	 \\
management	&	environmental	&	administration	 \\
occupational	&	management	&	marketing	 \\
environmental	&	occupational	&	specialization	 \\
supervisor	&	norms	&	selling	 \\
norms	&	documents	&	economy	 \\
capacitation	&	tracing	&	proactive	 \\
risk	&	industrial	&	responsible	 \\
iso	&	support	&	dynamic	 \\
\hline
\end{tabular} \label{table:case2}
\end{table}


\begin{table}
\centering
\caption{Topics behaviour: persistence of latent structure}
\begin{tabular}{|c|c|c|c|} \hline
\multicolumn{1}{|p{25mm}|}{Whole text (model 2)} & \multicolumn{1}{p{25mm}|}{Text chunks (model 5)} \\ \hline
Topic 4 & Topic 9 \\ \hline
systems	&	systems	 \\
technician	&	informatics	 \\
informatics	&	analyst	 \\
development	&	programmer	 \\
computation	&	sql	 \\
sql	&	development	 \\
programmer	&	computation	 \\
analyst	&	programming	 \\
programming	&	server	 \\
server	&	administrator	 \\
\hline
\end{tabular} \label{table:case3}
\end{table}



% include your own bib file like this:
%\bibliographystyle{acl}
%\bibliography{acl2016}
\bibliography{acl2016}
\bibliographystyle{acl2016}

\end{document}
