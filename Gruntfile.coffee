module.exports = (grunt) ->
  # Constants
  ACCOUNT_ROOT       = 'www/account/static/account'
  WEBSITE_ROOT       = 'www/website/static/website'
  BLOG_ROOT          = 'www/blog/static/blog'
    
  ACCOUNT_BUILD_DIR  = "#{ACCOUNT_ROOT}/build/"
  
  WEBSITE_JS_DIR     = "#{WEBSITE_ROOT}/js/"
  BLOG_JS_DIR        = "#{BLOG_ROOT}/js/"

  ACCOUNT_SASS_DIR   = "#{ACCOUNT_ROOT}/sass/"
  WEBSITE_SASS_DIR   = "#{WEBSITE_ROOT}/sass/"
  BLOG_SASS_DIR      = "#{BLOG_ROOT}/sass/"
  
  ACCOUNT_CSS_DIR    = "#{ACCOUNT_ROOT}/css/"
  WEBSITE_CSS_DIR    = "#{WEBSITE_ROOT}/css/"
  BLOG_CSS_DIR       = "#{BLOG_ROOT}/css/"
  
  GLOB_SASS_FILES    = "**/*.sass"
  GLOB_CSS_FILES     = "**/*.css"
  GLOB_JS_FILES      = "**/*.js"

  # Configuration
  grunt.initConfig

    #
    # Minify js files using UglifyJS
    #
    uglify:
      options:
        report: "min"
        compress: true
#      core_js:
#        files:
#          "www/core/static/core/build/core.min.js": [CORE_BUILD_DIR + "core.js"]

    #
    # Minify css files using clean-css
    #
    cssmin:
      options:
        report: "min"
        keepSpecialComments: "0"
      account:
        files:
          "www/account/static/account/build/account.min.css": [ACCOUNT_BUILD_DIR + "account.css"]

    #
    # Watch directories for changes
    #
    watch:
      options: 
        nospawn: true
      account_sass:
        files: [ACCOUNT_SASS_DIR + GLOB_SASS_FILES]
        tasks: ["compass:account", "concat:account_css"]
      account_css:
        files: [ACCOUNT_CSS_DIR + GLOB_CSS_FILES]
        tasks: ["concat:account_css"]

    #
    # Concatenate files
    #
    concat:
      account_css:
        src: [
          ACCOUNT_CSS_DIR + "account.css"
        ]
        dest: ACCOUNT_BUILD_DIR + "account.css"

    #
    # Compass
    #
    compass:
      account:
        options:
          basePath: 'www/account/'
          config: 'www/account/config.rb'
          trace: true

      website:
        options:
          basePath: 'www/website/'
          config: 'www/website/config.rb'
          trace: true
      blog:
        options:
          basePath: 'www/blog/'
          config: 'www/blog/config.rb'
          trace: true
    #
    # Cleaning
    #
    clean:
      release: [
        ACCOUNT_SASS_DIR
        ACCOUNT_CSS_DIR
        WEBSITE_JS_DIR
        WEBSITE_CSS_DIR
        WEBSITE_SASS_DIR      
        BLOG_JS_DIR
        BLOG_CSS_DIR
        BLOG_SASS_DIR 
      ]

  #
  # Load tasks
  #
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-concat'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-compass'
  grunt.loadNpmTasks 'grunt-contrib-cssmin'
  grunt.loadNpmTasks 'grunt-contrib-clean'

  #
  # Register tasks
  # 
  grunt.registerTask 'default', ->
    grunt.task.run [
      'build-dev'
      'watch'
    ]

  grunt.registerTask 'build-dev', 'Development build', ->
    grunt.task.run [
      # Account
      'compass:account'
      'concat:account_css'
      'cssmin:account'
      
      # Website
      'compass:website'

      # Blog
      'compass:blog'
    ]

  grunt.registerTask 'build-prod', 'Production build', ->
    grunt.config.set 'compass.account.options.force', true
    grunt.config.set 'compass.website.options.force', true
    grunt.task.run [
      'build-dev'

      # Account
      'cssmin:account'

      # Cleaning
      'clean:release'
    ]
