__author__ = 'ronald'

import os, sys
import json
import numpy
import pdb

data_SB = json.loads(unicode(open('raw_stackedBars.json').read().decode('utf-8')))
"""
temp = numpy.random.randint(10,300,size=[5,10])
data_SB = []
for i in range(5):
    item = {}
    item["name"] = str(i)
    item["data"] = temp[i]
    data_SB.append(item)
"""
INTERVAL_WIDTH = 100
max_countW = 0
num_areas = len(data_SB)

for area in data_SB:
    mx = 0
    if len(area["data"]) > 0:
        mx = max(area["data"])
    max_countW = max(max_countW,mx)

N_BARS = max_countW / INTERVAL_WIDTH
upperLimit = 0
extra_bar = False

if (max_countW - N_BARS * INTERVAL_WIDTH) > (1.0*INTERVAL_WIDTH) / 2:
    N_BARS += 1
    upperLimit = N_BARS * INTERVAL_WIDTH
    extra_bar = True
elif N_BARS * INTERVAL_WIDTH == max_countW:
    upperLimit = N_BARS * INTERVAL_WIDTH
else:
    upperLimit = (N_BARS + 0.5) * INTERVAL_WIDTH

SB_Matrix = numpy.zeros([num_areas,N_BARS])

for i in range(num_areas):
    data = data_SB[i]["data"]
    for d in data:
        bar = min(d/INTERVAL_WIDTH,N_BARS-1)
        SB_Matrix[i][bar] += 1

output = ''
for i in range(N_BARS):
    up = 0
    if i == N_BARS-1:
        up = upperLimit
    else:
        up = (i+1)*INTERVAL_WIDTH
    output += ',' + str(i*INTERVAL_WIDTH) + " - " + str(up)

output = [output]

for i in range(num_areas):
    name = data_SB[i]["name"]
    output.append( name + ',' +  ','.join([str(int(d)) for d in SB_Matrix[i]]) )

output = '\n'.join(output)

open("stacked_bar.csv",'w').write(output.encode('utf-8'))

#pdb.set_trace()