#!/bin/bash


PARAM=$1

if test -z $1
then
	cd /home/userjob
	source bin/activate
	cd /home/userjob/crawler/JobCrawler/
else
	cd /home/kevinbm/workspace/empanadas/crawler/JobCrawler/
fi

echo "" >> /tmp/spiderPE.log
echo "Crawl Started: $(date)" >> /tmp/spiderPE.log


scrapy crawl bumeran -a from_page=1 -a to_page=50 > /tmp/BUMERAN_PE.log 2>&1
wait
echo "BUMERAN COMPLETED: $(date)" >> /tmp/spiderPE.log

scrapy crawl aptitus -a from_page=1 -a to_page=50 > /tmp/APTITUS_PE.log 2>&1
wait
echo "APTITUS COMPLETED: $(date)" >> /tmp/spiderPE.log

scrapy crawl compuPE -a from_page=1 -a to_page=100 > /tmp/COMPU_PE.log 2>&1
wait
echo "COMPUTRABAJO COMPLETED: $(date)" >> /tmp/spiderPE.log

##
## CHECK CRONTAB IN THE SERVER!
##

echo "Crawl Successful: $(date)" >> /tmp/spiderPE.log