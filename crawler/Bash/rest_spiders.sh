#!/bin/bash


PARAM=$1

if test -z $1
then
	cd /home/userjob
	source bin/activate
	cd /home/userjob/crawler/JobCrawler/
else
	cd /home/kevinbm/workspace/empanadas/crawler/JobCrawler/
fi

echo "" >> /tmp/spiderREST.log
echo "Crawl Started: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranVEN > /dev/null 2>&1
wait
echo "bumeranVEN COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranURU > /dev/null 2>&1
wait
echo "bumeranURU COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranDOM > /dev/null 2>&1
wait
echo "bumeranDOM COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranPAN > /dev/null 2>&1
wait
echo "bumeranPAN COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranMEX > /tmp/BUMERAN_MEX.log 2>&1
wait
echo "bumeranMEX COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranECU > /dev/null 2>&1
wait
echo "bumeranECU COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranCR > /dev/null 2>&1
wait
echo "bumeranCR COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranCHI > /dev/null 2>&1
wait
echo "bumeranCHI COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl bumeranARG > /dev/null 2>&1
wait
echo "bumeranARG COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl compuMEX -a from_page=1 -a to_page=150 > /tmp/COMPU_MEX.log 2>&1
wait
echo "compuMEX COMPLETED: $(date)" >> /tmp/spiderREST.log

scrapy crawl compuCOL -a from_page=1 -a to_page=150 > /tmp/COMPU_COL.log 2>&1
wait
echo "compuCOL COMPLETED: $(date)" >> /tmp/spiderREST.log

echo "Crawl Successful: $(date)" >> /tmp/spiderREST.log


##
## CHECK CRONTAB IN THE SERVER!
##