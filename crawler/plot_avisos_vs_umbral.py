__author__ = 'kevinbm'

import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CRAWLER_DIR = os.path.join(BASE_DIR, 'crawler')
PROJECT_DIR = os.path.join(BASE_DIR, 'www')

sys.path.append(PROJECT_DIR)
sys.path.append(CRAWLER_DIR)

os.environ['DJANGO_SETTINGS_MODULE'] = 'project.dev'

from core.models import Details, Description
import datetime


jobs = Details.objects.all()

to_process = dict()
for job in jobs:
    to_process[job] = True

no_avisos = list()

for semana in range(2, 8, 2):
    count = 0
    debug = 0
    print "**SEMANA %d**" % semana
    for job in jobs:
        debug += 1
        if debug % 1000 == 0:
            print debug
        if to_process[job] is True:
            title = job.title
            company = job.company
            place = job.place
            date = job.date

            repeated_jobs = Details.objects.filter(
                company=company
            ).filter(
                place=place
            ).filter(
                title=title
            ).filter(
                date__gte=date - datetime.timedelta(days=semana*7)
            ).exclude(
                date__gte=date + datetime.timedelta(days=0)
            )

            for description in job.description_set.all():
                des = description.description
                for rjob in repeated_jobs:
                    if to_process[rjob] is True:
                        rdes = rjob.description_set.filter(description=des)
                        if len(rdes) > 0:
                            to_process[rjob] = False
                            break

    print "** Calculating count **"
    for job in jobs:
        if to_process[job] is True:
            count += len(job.description_set.all())

    no_avisos.append(count)

    print count

print no_avisos