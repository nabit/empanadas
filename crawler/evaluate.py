import os, sys
import json
import copy
import numpy as np
import random

temp = os.path.dirname(os.path.abspath(__file__))
pattern_dir = os.path.join(temp,'nlp scripts/patterns')

sys.path.append(pattern_dir)
from regex_patterns import *

## Datos externos
ident_tecnico = strip_encode(leer_tags(open(os.path.join(IDENTIFIER_STEM_DIR,'tecnico'))), False)
tec = stemAugmented('tecnico',degree=1)
ident_tecnico = [w for w in ident_tecnico if w != tec]

fileByIdent = getDocnameByCareer()

########################
# docname : {docname : true name}
nameByFile = json.loads(unicode(open('ident_names.json').read().decode('utf-8')))
temp = {}
for (file,name) in nameByFile.items():
    temp[file.strip(' ')] = name.strip(' ')
nameByFile = dict(temp)
########################

idByfile = {}
fileById = {}
idByName = {}
num_areas = 0

for (file,name) in nameByFile.items():
    idByfile[file] = num_areas
    idByName[name] = num_areas
    fileById[num_areas] = file
    num_areas += 1

###################################################################################################
# CASOS ESPECIALES - para que no los detecten/separen los patrones
# Mecanica Electrica
ident_mec_elect = [w for w in fileByIdent.keys() if fileByIdent[w]=='mecanica_electrica']
ident_mec_elect = '|'.join(addSuffixRegex(ident_mec_elect))

# Tecnologia Medica
ident_tec_med = [w for w in fileByIdent.keys() if fileByIdent[w]=='tecnologia_medica']
ident_tec_med = '|'.join(addSuffixRegex(ident_tec_med))

spec_case_patterns = [
    re.compile(addSuffixRegex(['ing'])[0] ),
    re.compile(ident_mec_elect),
    re.compile(ident_tec_med),
    re.compile(addSuffixRegex([stemAugmented('redes y comunicaciones')] )[0] ),
    re.compile(addSuffixRegex(['computacion e informatica']             )[0] ),
    re.compile(addSuffixRegex(['logistica y operaciones']               )[0] ),
    re.compile(addSuffixRegex(['operaciones y logistica']               )[0] ),
    re.compile(addSuffixRegex(['sistemas e informacion']                )[0] ),
    re.compile(addSuffixRegex([stemAugmented('medico veterinario')]     )[0] ),
    re.compile(addSuffixRegex([stemAugmented('estudio de abogado')] )[0] ),
    re.compile(r'^educacion\s*:'),
    re.compile(r'\([a-z]\)'),
]


spec_case_subs = [
    'ing',                      # normalizacion de ident de ingenieria
    'mecanica electrica',
    'medizintechnik',           # dafuq, tecnologia medica en Aleman xD
    'redes Y comunicaciones',
    'computacion E informatica',
    'logistica Y operaciones',
    'operaciones Y logistica',
    'sistemas E informacion',
    'veterinaria',
    'estudio_de_abogado',
    'formacion:',
    '',                         # elimina (a) o (o)
]

###################################################################################################
##                                  SETUP DE EVALUACION
cm_list=[
    'industrial',
    'sistemas',
    'informatica',
    'electrica',
    'mecanica',
]
total = 1187

correct = np.array([39,18,15,17,14])
incorrect = np.zeros(len(cm_list))
true_neg = 416

total_samples = 541
idByMajor = dict(zip(cm_list,range(len(cm_list))))

####################################
random.seed(42)     # siempre, justo antes de usar random
random_idx = random.sample(range(210000),100000)
random_idx.sort()


###################################################################################################
##                                                                  OBTENCION DE DATA
print "Uploading data to vh_all..."
#updateVHfromDB(query_date=['01-06-2014',])        # solo cuando se carga nueva data

print "Loading vh_all..."
data = readHashVector('vh_all')

print "%% Data loaded"
print "L:",len(data)


######################################################################################################################################################################################################
######################################################################################################################################################################################################
#                                                            HILO PRINCIPAL
patrones = compilePatterns()

for post in data:
    total += 1
    if total not in random_idx:
        continue
    det_pk = post[0]
    desc_pk = post[1]
    job = Details.objects.filter(pk=det_pk)[0]
    desc = job.description_set.filter(pk=desc_pk)[0]

    # armar texto
    title = strip_encode([job.title])
    title.append('')

    description = strip_encode([w for w in desc.description.split('\n')])

    cuerpo = list(title) # copia explicita
    cuerpo.extend(description)
    # Variables para identificacion de carreras
    carreras = set()
    found_areas = set()
    cuerpo_temp = []
    ing_post = False       # flag si posiblemente es un aviso de ingenieria

    for line in cuerpo:
        # preprocesado de casos especiales (ver header)
        for i,pat in enumerate(spec_case_patterns):
            line = pat.sub(spec_case_subs[i],line)
        if spec_case_patterns[0].search(line):
            ing_post = True                         # aviso pide ingenieros
        cuerpo_temp.append(line)
        # busqueda de patrones
        for (i,pattern) in enumerate(patrones):
            for car in careersFromPatterns(line,patrones,i):
                carreras.add(car.lower())

    cuerpo = list(cuerpo_temp)
    carreras = list(carreras)       # set -> list
    origc = list(carreras)  # copia explicita
    carreras = [stemAugmented(line).strip(' ') for line in carreras]
    carreras = list(set(carreras))

    ING_STEM  = 'ing'
    # buscar en identificadores | probar agregando ingenieria +
    for option in carreras:
        # variaciones nombres de ingenierias
        op1 = ING_STEM + " " + option
        op2 = ING_STEM + " de " + option
        op3 = ING_STEM + " en " + option
        op = [option,op1,op2,op3]

        for car in op:
            if car in fileByIdent.keys():
                # CASO ESPECIAL : QUIMICA | ING QUIMICA
                if any([car=='ing quimic',
                        car=='ing en quimic',
                        car=='ing de quimic',
                        car=='quimic']):                    # detect case
                    if ing_post:                            # es post de ingenieria?
                        found_areas.add('quimica')       # ing quimica
                    else:
                        found_areas.add('quimico')       # quimico puro
                else:                                       # caso general
                    found_areas.add(fileByIdent[car])

    if len(found_areas)==0:
        # buscar ident de tecnico menos 'tecnico'
        if searchIdentifier(cuerpo,ident_tecnico):
            found_areas.add('tecnico')
        else:
            found_areas.add('otros')
    """    
        print '''
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    (--   %d  ---)
    %s
    ::: Carreras
    %s
    ''' % (total,'\n'.join(cuerpo),found_areas)
    """
    
    total_samples+=1
    found=False
    for maj in found_areas:
        if maj in cm_list:
            found=True
            correct[idByMajor[maj]]+=1
    if not found:
        true_neg+=1

    if total_samples%1000==0:
        print "Samples:",total_samples
        temp =''
        for i in range(len(cm_list)):
            temp+= '%s(%d) ' % (cm_list[i],correct[i])
        print temp
        
        if total_samples>0:
            temp = ''
            for i in range(len(cm_list)):
                temp+= '%s(%.2f) ' % (cm_list[i],100.0*(correct[i]+true_neg)/total_samples)
            print temp
        print '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
            
    """
    opc = input('TruePos[0], Incorrect[1], TrueNeg[2], Stop[-1]:  ')
    opc = int(opc)
    if opc==2:
        total_samples+=1
        true_neg+=1
        continue
    elif opc==-1:
        break
    else:
        total_samples+=1
        for maj in found_areas:
            if maj in cm_list:
                if opc==0:
                    correct[idByMajor[maj]]+=1
                else:
                    incorrect[idByMajor[maj]]+=1
    """

    

temp = ''
for i in range(len(cm_list)):
    temp+= '%s(%.2f) ' % (cm_list[i],100.0*(correct[i]+true_neg)/total_samples)
print temp