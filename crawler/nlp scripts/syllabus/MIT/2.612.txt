Description
This course discusses the selection and evaluation of commercial and naval ship power and propulsion systems. It will cover the analysis of propulsors, prime mover thermodynamic cycles, propeller-engine matching, propeller selection, waterjet analysis, and reviews alternative propulsors. The course also investigates thermodynamic analyses of Rankine, Brayton, Diesel, and Combined cycles, reduction gears and integrated electric drive. Battery operated vehicles and fuel cells are also discussed. The term project requires analysis of alternatives in propulsion plant design for given physical, performance, and economic constraints. Graduate students complete different assignments and exams.

Topics Covered
Propulsion
Propellers
Waterjets
Other propulsors
Power Plants
Thermodynamics
Reversible cycles, availability
Rankine cycle
Combustion
Brayton cycle, gas turbine
Combined cycles
Diesel cycle
Reliability
Transmissions
Reduction gears
Electric drive
Propulsion dynamics
Propulsion of small underwater vehicles

Calendar
LEC #	TOPICS	KEY DATES
1	Resistance and propulsion (propulsors)	 
2	
Actuator disk

Propeller testing - B series

 
3	
Design using Kt (Kq) curves

Detail design

 
4	
Cavitation

Waterjet notes

 
5	First law	 
6	
Second law

Availability

Assignment 1 due
7	Propeller lifting line theory (Dr. Rich Kimball)	 
8	Propeller lifting line theory (Dr. Rich Kimball) (cont.)	 
9	Propeller lifting line theory (Dr. Rich Kimball) (cont.)	Assignment 2 due
10	
Water properties (Prof. Doug Carmichael)

Rankine cycle (Prof. Doug Carmichael)

Assignment 3 due one day before Lec #10
11	
Rankine cycle vs. pressure and temperature (Prof. Doug Carmichael)

Practical Rankine cycle (Prof. Doug Carmichael)

Rankine cycle with regeneration

Rankine cycle vs. pressure with reheat

 
12	Combustion	 
 	Quiz 1	 
13	Relationships for gases	 
14	Basic dual cycle diesel notes	 
15	Diesel analysis (cont.)	 
16	Diesel (cont.) or catch-up	Assignment 4 due
17	
Polytropic efficiency

Brayton cycle summary 2005

 
18	
Brayton cycle - irreversible examples

Open Brayton cycle

Creep

 
19	Electrical theory overview	Assignment 5 due
20	Motors and generators overview	 
21	Electric propulsion presentation, guest lecturer Prof. Harbour	 
22	
Reliability and availability

Repairable systems supplement

 
23	Reduction gears notes	 
24	
Gear geometry

Helical gear geometry

Assignment 6 due
25	Gear geometry	 
26	
Review, catch-up

Air independent propulsion

 
