About the Course
20.110/2.772 was first taught in 2002 with the aim of providing a foundation in the thermodynamic principles used to describe biomolecular behavior and interactions such as those that lead to assembly of cell membranes, binding of growth factors to cells, annealing of cDNA sequences to oligonucleotides on microarray chips, and separation of complex mixtures of biomolecules for atomic analysis. Many of these problems, as well as related problems in nanotechnology and polymer science, are illuminated by a statistical thermodynamics approach. As the course evolved to become the foundational thermodynamics subject for the Biological Engineering S. B. degree and for many students in Biology, we found that our original syllabus did not include the appropriate treatment of classical thermodynamics required to solve practical problems in biochemical thermodynamics. Further, it became clear that a revision of the 20.110/2.772 syllabus to begin with classical thermodynamics provide a fortuitous overlap with parts of the 5.60 syllabus. Chemistry, Mechanical Engineering, and Biological Engineering thus developed a common syllabus for the first half of the term, with each subject then diverging into significantly different emphases in the latter part of the term (5.60 concludes with chemical kinetics, and 20.110/2.772 with biomolecular structure and interactions). A pilot version of the combined syllabus was taught in Spring 2005 by Professors Silbey, Griffith, and Irvine as 20.110/2.772, and the current syllabus is slightly revised from the pilot.

LEC #	TOPICS	KEY DATES
1	Introduction to Thermo; 0th Law; Temperature; Work; Heat	
2	State Functions, 1st Law, Paths	
3	Joule and Joule-Thompson; Heat Capacity	
4	Reversible and Irreversible Processes	
5	Thermochemistry	Problem set 1 due
6	2nd Law; Entropy (Boltzmann and Clausius)	
7	ΔS for Reversible and Irreversible Processes	Problem set 2 due
8	Equilibrium; Maxwell Relations; Free Energy	
9	Chemical Potential; Phase Equilibrium	
10	Chemical Equilibrium; Equilibrium Constant	Problem set 3 due
11	Standard States; Gibbs-Duhem	
12	ΔG0= -RTlnK; Example	
Hour Exam 1	
13	Boltzmann Distribution	
14	Thermo and Boltzmann Distribution	Problem set 4 due
15	Occupation of States	
16	Third Law	
17	Phase Equilibria, Single Component	Problem set 5 due
18	Phase Equilibria II; Clausius Clapeyron	
19	Regular Solutions; Mixing Energy; Mean Fields	
20	Nonideal Solutions	Problem set 6 due
21	Solvation; Colligative Properties	
Hour Exam 2	
22	Osmotic Pressure and Phase Partitioning	
23	Surface Tension	
24	Polymer 1 - Freely Jointed Chain	Problem set 7 due
25	Polymer 2 - Chain Conformation	
26	Polymer 3 - Rubber Elasticity	
27	Electrolyte Solutions	Problem set 8 due
28	Electrolytes at Interfaces; Debye Length	
29	Titration of Polyelectrolytes	
30	Thermodynamics of DNA Hybridization	Problem set 9 due
31	Cooperativity	
Hour Exam 3	
32	Cooperativity, Part 2	
33	Cooperativity, Part 3	
34	Driving Forces for Self-Assembly	Problem set 10 due
35	Special Topic (Coarse Grain/Monte Carlo Model)	
36	Course Review and Evaluations
