Calendar
WEEK #	LECTURES	LAB/QUIZ	KEY DATES
1	Intro and organization; circuits and Op-Amps	 	Problem set 1 out
2	
Op-Amps as feedback systems

Actuators and transformers

Orientation session (all)	
Problem set 1 due

Problem set 2 out

3	State-space review	Lab 1 (G): Op-Amp feedback circuits	
Problem set 2 due

Problem set 3 out

4	Root locus	Lab 1 (U): Op-Amp feedback circuits	
Problem set 3 due

Quiz 1 review

5	Frequency response, Bode and Nyquist plots	Quiz 1 (all)	Problem set 4 out
6	Frequency-domain design	Lab 2 (G): Op-Amp PID controller	
Problem set 4 due

Problem set 5 out

7	Frequency-domain design (cont.)	Lab 2 (U): Op-Amp PID controller	
Problem set 5 due

Problem set 6 out

8	State-space control design	Lab 3 (G): Frequency domain design	
Problem set 6 due

Problem set 7 out

9	State-space control design (cont.)	Lab 3 (U): Frequency domain design	
Problem set 7 due

Quiz 2 review

10	Discrete-time design	Quiz 2 (all)	Problem set 8 out
11	
Discrete-time design (cont.)

Nonlinear systems, linearization

Lab 4 (G): State-space design	
Problem set 8 due

Problem set 9 out

12	Describing functions	Lab 4 (U): State-space design	
Problem set 9 due

Problem set 10 out

13	Design examples	Lab 5 (U): Discrete-time control	Problem set 10 due
14	Advanced topics; overview and wrapup	Lab 5 (G): Discrete-time control	Final exam review
15	 	Final exam
