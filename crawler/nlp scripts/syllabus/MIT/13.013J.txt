This subject was originally offered in Course 13 (Department of Ocean Engineering) as 13.013J. In 2005, ocean engineering became part of Course 2 (Department of Mechanical Engineering), and this subject merged with 2.003.

1	
Introduction and Motivation

Kinematics

Problem Set 1
2	Kinematics	 
3	Kinematics	 
4	Recitation	
Problem Set 2

Problem Set 1 Due

5	Momentum Formulation for Systems of Particles	 
6	Momentum Formulation for Systems of Particles	 
7	Recitation	
Problem Set 3

Problem Set 2 Due

8	Momentum Formulation for Systems of Particles	 
9	Recitation	Problem Set 4
Problem Set 3 Due
10	Variational Formulation for Systems of Particles	 
11	Variational Formulation for Systems of Particles	 
12	Recitation	Problem Set 5
Problem Set 4 Due
13	Quiz 1	 
14	Variational Formulation for Systems of Particles	 
15	Variational Formulation for Systems of Particles	Problem Set6
16	Recitation	Problem Set 5 Due
17	Recitation	Problem Set7
Problem Set 6 Due
18	Variational Formulation for Systems of Particles	 
19	Dynamics of Systems Containing Rigid Bodies	 
20	Recitation	Problem Set8
Problem Set 7 Due
21	Dynamics of Systems Containing Rigid Bodies	 
22	Dynamics of Systems Containing Rigid Bodies	 
23	Recitation	Problem Set9
Problem Set 8 Due
24	Dynamics of Systems Containing Rigid Bodies	 
25	Vibration of Linear Lumped-Parameter Systems	 
26	Recitation	Problem Set10
Problem Set 9 Due
27	Vibration of Linear Lumped-Parameter Systems	 
28	Recitation	Problem Set 11
Problem Set 10 Due
29	Quiz II	 
30	Vibration of Linear Lumped-Parameter Systems	 
31	Recitation	Problem Set12
Problem Set 11 Due
32	Vibration of Linear Lumped-Parameter Systems	 
33	Vibration of Linear Lumped-Parameter Systems	 
34	Recitation	Problem Set13
Problem Set 12 Due
35	Vibration of Linear Lumped-Parameter Systems	 
36	Recitation	Problem Set 13 Due
37	Dynamics of Continuous Systems	 
38	Dynamics of Continuous Systems	 
39	Recitation: Final Exam Review
