from boto.sdb.db.sequence import increment_by_one

__author__ = 'bmk'

from pydoc import help
from scipy.stats.stats import pearsonr

uni = open("ug_uni.csv")
mit = open("ug_mit.csv")
market = open("ug_market.csv")

uni = uni.read().split()
mit = mit.read().split()
market = market.read().split()

#print uni

X = [word.split(',')[0] for word in mit]
X = X[1:101]

#print X

Y = [word.split(',')[0] for word in uni]
Y = Y[1:101]

newX = []
newY = []
cont = 0
for x in X:
    if x in Y:
        #print x
        cont+=1
        newX.append(cont)
        newY.append((Y.index(x),cont))
        #print y,X.index(y)
newY=sorted(newY,key=lambda newY:newY[0])
newY= [word[1] for word in newY]

print newY
print newX

print len(X),len(Y)
print (1.0*len(newX))/len(X) * 100
print (1.0*len(newY))/len(X) * 100

result = pearsonr(newX,newY)

print result