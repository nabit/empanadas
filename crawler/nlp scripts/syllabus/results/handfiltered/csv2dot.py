try:
    ug_mit = open('ug_mit.csv')
    ug_uni = open('ug_uni.csv')
    ug_market = open('ug_market.csv')

    ug_mit = ug_mit.read().split()
    ug_uni = ug_uni.read().split()
    ug_market = ug_market.read().split()

    ug_hive = open('ug_hive.dot', 'w')
    ug_hive.write('digraph Simple {\n\n')
    cont = 0
    for ug in ug_mit:
        if cont == 101:
            break
        cont += 1
        [name, score] = ug.split(',')
        if score == 'score':
            continue
        inscore = float(score) * 100
        score = str(inscore)
        node = 'mit.' + name
        ug_hive.write(node + ' [\"pack\"=\"mit\" score=' + score + '] # node ' + name + '\n')
        ug_hive.write(node + ' -- ' + 'uni.' + name + ' # edge from ' + node + ' to ' + 'uni.' + name + '\n')
        ug_hive.write(node + ' -- ' + 'market.' + name + ' # edge from ' + node + ' to ' + 'market.' + name + '\n')

    cont = 0
    for ug in ug_uni:
        if cont == 101:
            break
        cont += 1
        [name, score] = ug.split(',')
        if score == 'score':
            continue
        inscore = float(score) * 100
        score = str(inscore)
        node = 'uni.' + name
        ug_hive.write(node + ' [\"pack\"=\"uni\" score=' + score + '] # node ' + name + '\n')
        ug_hive.write(node + ' -- ' + 'mit.' + name + ' # edge from ' + node + ' to ' + 'mit.' + name + '\n')
        ug_hive.write(node + ' -- ' + 'market.' + name + ' # edge from ' + node + ' to ' + 'market.' + name + '\n')

    cont = 0
    for ug in ug_market:
        if cont == 101:
            break
        cont += 1
        [name, score] = ug.split(',')
        if score == 'score':
            continue
        inscore = float(score) * 100
        score = str(inscore)
        node = 'market.' + name
        ug_hive.write(node + ' [\"pack\"=\"market\" score=' + score + '] # node ' + name + '\n')
        ug_hive.write(node + ' -- ' + 'uni.' + name + ' # edge from ' + node + ' to ' + 'uni.' + name + '\n')
        ug_hive.write(node + ' -- ' + 'mit.' + name + ' # edge from ' + node + ' to ' + 'mit.' + name + '\n')

    ug_hive.write('}')
except:
    print 'Error en cargar archivos unigramas...'
"""
#try:
ug_mit = open('bg_mit.csv')
ug_uni = open('bg_uni.csv')
ug_market = open('bg_market.csv')

ug_mit = ug_mit.read().split()
ug_uni = ug_uni.read().split()
ug_market = ug_market.read().split()

ug_hive = open('bg_hive.dot','w')
ug_hive.write('digraph Simple {\n\n')
cont = 0
print ug_mit
for ug in ug_mit:
    if cont==500:
        break
    cont+=1
    [name1,name2,score] = ug.split(',')
    name = name1 + '_' + name2
    if score == 'score':
        continue
    inscore = float(score) * 100
    score = str(inscore)
    node = 'mit.'+name
    ug_hive.write(node + ' [\"pack\"=\"mit\" score='+score+'] # node ' + name+ '\n')
    ug_hive.write(node + ' -- ' + 'uni.' + name +' # edge from '+ node +' to '+ 'uni.' + name + '\n')
    ug_hive.write(node + ' -- ' + 'market.' + name + ' # edge from '+ node +' to '+ 'market.' + name + '\n')

cont = 0
for ug in ug_uni:
    if cont==500:
        break
    cont+=1
    [name1,name2,score] = ug.split(',')
    name = name1 + '_' + name2
    if score == 'score':
        continue
    inscore = float(score) * 100
    score = str(inscore)
    node = 'uni.'+name
    ug_hive.write(node + ' [\"pack\"=\"uni\" score='+score+'] # node ' + name+ '\n')
    ug_hive.write(node + ' -- ' + 'mit.' + name +' # edge from '+ node +' to '+ 'mit.' + name + '\n')
    ug_hive.write(node + ' -- ' + 'market.' + name + ' # edge from '+ node +' to '+ 'market.' + name + '\n')

cont = 0
for ug in ug_market:
    if cont==500:
        break
    cont+=1
    [name1,name2,score] = ug.split(',')
    name = name1 + '_' + name2
    if score == 'score':
        continue
    inscore = float(score) * 100
    score = str(inscore)
    node = 'market.'+name
    ug_hive.write(node + ' [\"pack\"=\"market\" score='+score+'] # node ' + name+ '\n')
    ug_hive.write(node + ' -- ' + 'uni.' + name + ' # edge from '+ node +' to '+ 'uni.' + name + '\n')
    ug_hive.write(node + ' -- ' + 'mit.' + name + ' # edge from '+ node +' to '+ 'mit.' + name + '\n')

ug_hive.write('}')

#except:
#	print 'Error en cargar archivos bigramas...'

#try:
ug_mit = open('tg_mit.csv')
ug_uni = open('tg_uni.csv')
ug_market = open('tg_market.csv')

ug_mit = ug_mit.read().split()
ug_uni = ug_uni.read().split()
ug_market = ug_market.read().split()

ug_hive = open('tg_hive.dot','w')
ug_hive.write('digraph Simple {\n\n')
cont = 0
print ug_mit
for ug in ug_mit:
    if cont==500:
        break
    cont+=1
    [name1,name2,name3,score] = ug.split(',')
    name = name1 + '_' + name2 + '_' + name3
    if score == 'score':
        continue
    inscore = float(score) * 100
    score = str(inscore)
    node = 'mit.'+name
    ug_hive.write(node + ' [\"pack\"=\"mit\" score='+score+'] # node ' + name+ '\n')
    ug_hive.write(node + ' -- ' + 'uni.' + name +' # edge from '+ node +' to '+ 'uni.' + name + '\n')
    ug_hive.write(node + ' -- ' + 'market.' + name + ' # edge from '+ node +' to '+ 'market.' + name + '\n')

cont = 0
for ug in ug_uni:
    if cont==500:
        break
    cont+=1
    [name1,name2,name3,score] = ug.split(',')
    name = name1 + '_' + name2 + '_' + name3
    if score == 'score':
        continue
    inscore = float(score) * 100
    score = str(inscore)
    node = 'uni.'+name
    ug_hive.write(node + ' [\"pack\"=\"uni\" score='+score+'] # node ' + name+ '\n')
    ug_hive.write(node + ' -- ' + 'mit.' + name +' # edge from '+ node +' to '+ 'mit.' + name + '\n')
    ug_hive.write(node + ' -- ' + 'market.' + name + ' # edge from '+ node +' to '+ 'market.' + name + '\n')

cont = 0
for ug in ug_market:
    if cont==500:
        break
    cont+=1
    [name1,name2,name3,score] = ug.split(',')
    name = name1 + '_' + name2 + '_' + name3
    if score == 'score':
        continue
    inscore = float(score) * 100
    score = str(inscore)
    node = 'market.'+name
    ug_hive.write(node + ' [\"pack\"=\"market\" score='+score+'] # node ' + name+ '\n')
    ug_hive.write(node + ' -- ' + 'uni.' + name + ' # edge from '+ node +' to '+ 'uni.' + name + '\n')
    ug_hive.write(node + ' -- ' + 'mit.' + name + ' # edge from '+ node +' to '+ 'mit.' + name + '\n')

ug_hive.write('}')

#except:
#	print 'Error en cargar archivos trigramas...'

"""
