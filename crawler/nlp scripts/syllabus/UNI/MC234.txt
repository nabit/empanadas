TECNOLOGÍA DE LA SOLDADURA I

1° SEMANA	

SEGURIDAD EN SOLDADURA. peligros de la corriente eléctrica, peligros en la manipulación de gases, indumentaria del soldador, ventilación en el ambiente de trabajo, peligros de explosión, triangulo de fuego, certificado de autorización para trabajos de soldadura.

Practica de laboratorio: Seguridad en soldadura

2° SEMANA

PROCESO DE SOLDADURA OXIGAS (OFW). Gases de soldadura, Comparación de los gases combustibles de soldadura, seguridad en el manejo de gases, bloqueadores de llama, reductores de presión, tipos de llama, soldadura de aceros y fierros fundidos, soldadura fuerte, soldadura blanda.

Practica de laboratorio: Soldadura oxigas

3° SEMANA

PROCESO DE SOLDADURA DE ELECTRODO REVESTIDO (SMAW). La máquina de soldar, tipos, accesorios, mantenimiento, fuentes de poder, requisitos y clasificación. Principios del transformador, del rectificador de soldar, materiales de aporte, efectos de la polaridad en CC, funciones del revestimiento en los electrodos de soldar, clasificación de los electrodos estructurales, según el AWS 5.1/5.5, humedad y difusión del hidrógeno, control del soplo magnético. 

Practica de taller: fuentes de poder

4° SEMANA

Practica de taller: Encendido del arco, ejecución de cordones

PROCESO GMAW. Fuentes de poder de tensión constante, sistema alimentador, antorchas de los equipos GMAW, Parámetros de regulación, Diagrama de regulación V-I, gases protectores, materiales de aporte, efecto de la altura del alambre caliente (Stick out), tipos de transferencia,  defectos y correcciones, problemas de flujo del gas protector, regulación de flujo del gas protector, falta de fusión en el proceso GMAW, errores en el manejo del equipo, cuidados de la maquina y del material de aporte

Practica de taller: ejecución de cordones y almodillado en posición plana

5° SEMANA

Practica de taller: soldadura cordones traslapados, ejecución de filetes

PROCESO FCAW, MACAW. Fuentes de poder de voltaje constante y corriente constante, sistema alimentador, antorchas de los equipos de alambre tubular, parámetros de regulación, materiales de aporte, efectos de la humedad en alambres tubulares, errores en el manejo del equipo. 


Practica de taller: Ejecución de cordones de filetes, con electrodos rutilicos y básicos


6° SEMANA

PROCESO GTAW. Fuentes de poder y partes de la maquina, antorcha de la soldadura GTAW, configuración de la máquina, regulación de parámetros, Encendido por alta frecuencia, programa de operación con 4 tiempos, tipos de corriente y aplicaciones, gases protectores, electrodos de tungsteno, materiales de aporte, defectos y corrección 

Practica de taller: Ejecución de cordones con el proceso GMAW

7° SEMANA

PROCESO SAW. fuentes de poder, alimentadores, manipuladores, rotadores, posicionadores, regulación de parámetros, tipos de corriente y aplicaciones, materiales de aporte, alambres y fundentes, variación del contenido de manganeso y silicio, combinación de alambre y fundente para recubrimientos.

Practica de taller: Almodillado en posición plana con el proceso GMAW

8°  SEMANA


SEMANA DE EXAMENES PARCIALES 

9°  SEMANA


METALURGIA DE LA SOLDADURA. Aleaciones hierro carbono, el acero en equilibrio y fuera de equilibrio, el ciclo térmico en la soldadura, soldabilidad de los aceros estructurales, de los inoxidables, agrietamiento en frio y en caliente.

Practica de taller: cordones en posición plana con variación de parámetros con proceso GMAW

10°  SEMANA

CORTE TÉRMICO. Condiciones para el  proceso de oxicorte, materiales aptos y no aptos para el proceso de oxicorte, medidas de seguridad para el cote térmico, concepto del plasma, características del proceso y de los equipos de corte con plasma, selección de gases y parámetros, seguridad en corte plasma.

Practica de taller: almodillado en soldadura de filete con proceso GMAW.

11°  SEMANA

INSPECCIÓN VISUAL. Guía para la inspección visual B1.11, defectos superficiales y sub superficiales, ensayos no destructivos y ensayos destructivos en costuras soldadas, calificación en procedimientos de soldadura, calificación de soldadores.

Practica de taller: Ejecución de cordones de filete con proceso GMAW

12°  SEMANA

COSTOS EN SOLDADURA. Factores a considerarse en costos de soldadura, factores de operación, costeo y evaluación, peso de metal por unidad de longitud, eficiencia de electrodos por proceso, factor de operador por proceso, uso de tablas de costos y rendimientos.

Practica de laboratorio: Inspección visual de soldaduras
13°  SEMANA

EXPOSITOR INVITADO, EXPERIENCIAS PRACTICAS DE INSPECCIÓN

Practica de laboratorio: Ensayos destructivos 

14°  SEMANA

Practica de laboratorio: Ensayos no destructivos

AUTOMATIZACIÓN Y ROBÓTICA EN SOLDADURA

Practica de laboratorio: Calificación de procedimientos de soldadura y soldador


15°  SEMANA

ASESORÍA EN TRABAJOS PRÁCTICOS Y DE INVESTIGACIÓN EN SOLDADURA PARA SU TRABAJO MONOGRÁFICO

Practica de laboratorio: continuación de calificación de procedimientos de calificación 

16°  SEMANA

SEMANA DE EXAMENES FINALES
