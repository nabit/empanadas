Máquinas Eléctricas

PRIMERA SEMANA
Primera Sesión:
Conceptos fundamentales del electromagnetismo: Densidad de flujo magnético (B), flujo magnético (Φm) e intensidad de campo magnético (H). 
Segunda Sesión:
Relación entre B y H (permeabilidad magnética). Materiales ferromagnéticos: características magnéticas (ciclo de histéresis, curva B-H, etc.). Permeabilidad magnética relativa.

SEGUNDA SEMANA
Primera Sesión:
Ley de Ampere. Aplicaciones de la Ley de Ampere en núcleos ferromagnéticos. Reluctancia magnética de un núcleo ferromagnético. Circuitos magnéticos: definición y ejemplos prácticos.
Segunda Sesión: 
Circuitos magnéticos de sección rectangular sin entrehierro excitados con flujo magnético constante (con DC). Problemas. 

TERCERA SEMANA
Primera Sesión:
Circuitos magnéticos de sección rectangular con entrehierro excitados con flujo magnético constante (con DC). Circuitos magnéticos con ramas en serie  excitados con flujo magnético constante (con DC). 
Segunda Sesión:
Circuitos magnéticos de sección rectangular con ramas en paralelo excitados con flujo magnético constante (con DC).Problemas.
Circuitos magnéticos alimentados con voltaje alterno senoidal: fuerza electromotriz inducida. 

CUARTA SEMANA
Primera Sesión:
Pérdidas de energía en los núcleos ferromagnéticos. Pérdidas por histéresis. Pérdidas por Foucault. Pérdidas totales o pérdidas en el fierro. Medición de las pérdidas en el fierro. 
Segunda Sesión:
Corriente de excitación de un reactor de núcleo ferromagnético: forma de onda. Interpretación práctica de la corriente de excitación. . Modelo circuital del reactor de núcleo ferromagnético. Determinación de los parámetros del modelo circuital. Problemas.

QUINTA SEMANA
Primera sesión:
El transformador monofásico: Definición, aspecto Constructivo. El transformador monofásico ideal. Transformador monofásico ideal operando en vacío.
Segunda Sesión:
Transformador monofásico ideal operando con carga. Modelo circuital del transformador monofásico ideal. Circuitos equivalentes del transformador monofásico ideal referidos a uno sus lados: relación de impedancias.
  
SEXTA SEMANA
Primera Sesión:
El transformador monofásico real: características, modelo o circuito equivalente exacto. Modelo o circuito equivalente aproximado. Modelos circuitales referidos a uno de sus lados. 
Segunda Sesión:
Pruebas de control de calidad de los transformadores monofásicos. Prueba en vacío. Prueba de cortocircuito. Regulación de tensión del transformador monofásico de potencia. 

SÉPTIMA SEMANA
Primera sesión:
Eficiencia del transformador monofásico de potencia. Problemas.
Segunda Sesión:  
Polaridad del transformador monofásico. Transformador monofásico conectado como autotransformador. Problemas.

OCTAVA SEMANA
Primera Sesión:
Transformación de tensiones trifásicas: Bancadas trifásicas en conexión estrella (Y), en conexión delta (D) y en conexión mixta.
Segunda Sesión:
Transformadores trifásicos. Problemas.

NOVENA SEMANA
EXAMEN PARCIAL

 
DÉCIMA SEMANA
Primera Sesión: Máquina eléctrica rotativa: generalidades,  definición de generador y motor. Aspecto constructivo genérico de una máquina eléctrica rotativa. Tipos de máquinas rotativas. Principios básicos de la conversión electromecánica de la energía: Principio del generador y principio del motor. 
         Segunda Sesión:
Máquinas eléctricas de corriente alterna: generalidades, tipos. El motor trifásico de inducción: clasificación, aspecto constructivo del estator y del rotor. El campo magnético giratorio. 

DÉCIMO PRIMERA SEMANA
Primera Sesión:
Campo magnético giratorio en el motor trifásico de inducción. Velocidad del campo magnético giratorio.  Principio de funcionamiento del motor trifásico de inducción. Campo magnético giratorio producido por el rotor. Requisitos que debe cumplir el rotor para el correcto funcionamiento del motor asíncrono. 
Segunda Sesión:
Velocidad de deslizamiento y deslizamiento del motor de inducción. Frecuencia del rotor. Conexión de devanados e inversión del sentido de giro del motor trifásico de inducción. Determinación del circuito equivalente del motor trifásico de inducción: caso de rotor bloqueado y devanado rotórico en circuito abierto. 

DÉCIMO SEGUNDA SEMANA
Primera Sesión:
Caso de de rotor bloqueado y devanado rotórico en cortocircuito. Modelo circuital con el rotor en movimiento: circuito equivalente exacto en régimen estable, circuito equivalente aproximado. Determinación de los parámetros del modelo circuital del motor de inducción mediante pruebas de laboratorio: Prueba con corriente continua sobre el estator. 
Segunda Sesión:   
Prueba con rotor bloqueado o prueba de cortocircuito y prueba de rotor libre o prueba en vacío. Flujo o balance de potencias en el motor de inducción: eficiencia. Par o torque de rotación del motor trifásico de inducción: par útil, par desarrollado o par motor.  Problemas.

DÉCIMO TERCERA SEMANA
Primera Sesión:
Curva característica del par desarrollado del motor en función del deslizamiento. Comportamiento del motor con carga acoplada al eje. Expresión del par motor en función de los parámetros del modelo circuital. Par de arranque. Par máximo. Tipos de regímenes de funcionamiento del motor trifásico. 
Segunda Sesión: 
Tipos de arranque del motor de inducción. Formas de control de velocidad del motor de inducción. Selección de los motores de inducción. Problemas.

DÉCIMO CUARTA SEMANA   
Primera Sesión:
La máquina eléctrica de corriente continua o máquina DC: aspecto constructivo. Campo magnético principal en el entrehierro. Máquina DC operando como generador: caso de máquina elemental de dos polos (expresión del voltaje generado). Colector o conmutador. Forma de onda del voltaje generado por la máquina elemental DC.
Segunda Sesión:
Devanado de armadura de una máquina real DC. Expresión del voltaje generado en una máquina real DC. Tipos de devanado de armadura. Aspectos importantes relacionados con el devanado de armadura: grados eléctricos y grados mecánicos, factor de paso de bobina, devanado de armadura de tipo múltiple. Reacción de armadura.

DÉCIMO QUINTA SEMANA
Primera Sesión:
Modelo circuital del generador DC en régimen permanente. Generador DC operando con carga. Máquina DC operando como motor: Motor elemental (expresión del par o torque desarrollado). Motor real DC: expresión del torque desarrollado. Fuerza contraelectromotriz o contravolatje.  Modelo circuital de la máquina DC operando como motor en régimen estable.
Segunda Sesión:
Problemas sobre el generador y motor DC. Tipos de máquinas DC como generador y motor. Modelo circuital en régimen estable de las máquina DC autoexcitadas para operación como generador o motor: máquina DC tipo shunt, máquina DC tipo serie y máquina DC tipo compaund.
DÉCIMO SEXTA SEMANA
Primera Sesión:
Curva característica interna o curva de magnetización de la máquina DC. Autoexcitación del generador shunt. Flujo de potencia en el generador DC: eficiencia. Flujo de potencia en el motor DC: eficiencia. Curvas características de salida de los generadores DC: regulación de tensión. 
Segunda Sesión: 
Curvas características de salida de los motores DC: regulación de velocidad. Problemas.

DÉCIMO SÉPTIMA SEMANA
EXAMEN FINAL 
