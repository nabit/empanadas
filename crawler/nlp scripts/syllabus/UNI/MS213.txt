INGENIERÍA ECONÓMICA Y FINANZAS

Operaciones financieras. Interés simple. Interés compuesto. Tasas de interés en el sistema financiero. Anualidades. Factores financieros. Fondo de amortización. Amortización. Depreciación. Aplicaciones de las tasas de interés en inversiones a largo plazo. Análisis económico – financiero de las empresas. Conceptos y técnicas de la administración financiera de mediano y largo plazo. Incluyendo en ella el uso de indicadores de evaluación del desempeño. Se conocerán los elementos de plantación financiera y los requisitos de acceso como usuario del sistema financiero.


SEMANA Nº 01

1. Introducción. Conceptos generales. Conceptos generales de economía empresarial. Conceptos básicos de Economía
2. El mercado. El valor económico de los recursos. El costo de oportunidad del capital.
3. Operaciones financieras. Interés. Utilidad. Tasa de Interés, nombres que toman de acuerdo a su aplicación. Duración de una operación financiera. Período de tiempo. Valor del dinero a través del tiempo.
4. Aplicaciones. 


SEMANA Nº 02


5. Interés simple; factores que intervienen en la fórmula de interés simple. Valor futuro, valor presente; aplicaciones. Ecuaciones de valor; problemas de aplicación
6. Interés compuesto: Cálculo de la tasa de interés y tiempo. Ecuaciones de valor; problemas de aplicación.
7. El costo financiero. El precio de mercado del dinero. Definición de tasa activa, tasa pasiva, nominal, proporcional, efectiva,  Aplicaciones.
8. El riesgo.
9. La inflación: Tasa de inflación, tasa real. Aplicaciones.







SEMANA Nº 03

10. El valor del dinero en el tiempo
11. El valor presente/valor presente neto
12. El valor futuro
13. Anualidades


SEMANA Nº 04

14. Definición de anualidades; Tipos de anualidades. Aplicaciones
15. Anualidades vencidas: Cálculo del valor presente, valor futuro, renta, número de períodos y tasa de interés; aplicaciones
16. Anualidades anticipadas: Cálculo del monto, valor presente, renta, número de períodos; aplicaciones.
17. Anualidades anticipadas: Tasa de interés; aplicaciones.
18. Anualidades diferidas: Cálculo del monto, valor presente, renta, número de períodos; tasa de interés; aplicaciones.


SEMANA Nº 05

19. Formas de Pago – Métodos de Amortización: Sistema de repago de préstamos. 
20. Amortizaciones constantes (método Alemán).
21. Interés constante (Método Inglés).
22. Suma de los números dígitos (Método Americano). Aplicaciones. 
23. Depreciación
24. Fórmula de depreciación de un activo fijo. 
25. Métodos de depreciación


SEMANA Nº 06

26. La función financiera en la empresa, objetivos y alcances del tema.
27. Estructura financiera: El financiamiento total, la inversión total, riesgos. Análisis de Riesgo
28. El Activo total o inversión total, el Palanqueo Financiero o “Leverage” financiero.


SEMANA Nº 07

29. Introducción a la Contabilidad Empresarial. Estados Financieros: El Balance General. El Estado de Pérdidas y Ganancias. El Estado de Cambios en el Patrimonio. El Estado de Cambios en el Capital de Trabajo
30. Análisis de Balance General, Estado de Resultados, Fuentes y Usos de Fondos (Evaluación de gestión financiera).Intereses (intereses durante la construcción, intereses de la deuda) 


SEMANA Nº 08	

EXAMEN PARCIAL

SEMANA Nº 09

31. Análisis y Control Financiero: Estructura Financiera de Corto Plazo: (Razones de liquidez, eficiencia de gerencia, cobertura de caja y capital de trabajo).
32. Análisis y Control Financiero: Estructura Financiera a Largo Plazo y Rentabilidad Financiera (Ratios o Indicadores de Solvencia, de Endeudamiento y Rentabilidad).
33. Decisiones de la Evaluación de la Gestión de la Empresa: (Conclusiones y Recomendaciones).

SEMANA Nº 10

34. El Financiamiento del Capital de Trabajo.
35. El Crédito de Proveedores: con respaldo de Letra de Cambio; Factura comprobante de pago, Factura conformada: (Amortización y renovación de letra de cambio).
36. El Préstamo Bancario, mediante la firma de un pagaré.
37. Conceptos de obtención de fondos y conceptos de destino de fondos: El Flujo de Caja (Proyectado).
38. El Control de Caja: Determinación de los Saldos: El exceso de fondos, los saldos negativos de fondos, el financiamiento.

SEMANA Nº 11
39. Análisis de los Activos Fijos Tangibles e Intangibles: conceptos y alcances.
40. El Financiamiento a Largo Plazo.
41. Inversión, financiamiento y evaluación: preparación del flujo de fondos, determinación de la financiación, certeza – riesgo.

SEMANA Nº 12

42. Inversión, Financiamiento y Evaluación: Determinación del Flujo de Fondos Netos: Criterios de evaluación: la TIR, el VAN.
43. Inversión,  Financiamiento y Evaluación: Alternativas a financiar, Evaluación económica, evaluación financiera (La TIRE, la TIRF).

SEMANA Nº 13
44. La interrelación entre las decisiones de inversión y de financiamiento.
45. Introducción al riesgo, rentabilidad y costo de oportunidad del capital.
46. Mediciones estadísticas para medir el riesgo y la rentabilidad de un activo financiero.
47. Mediciones estadísticas para medir el riesgo y la rentabilidad de un portafolio.


SEMANA Nº 14

48. Valuación de bonos. Elementos que lo conforman. Valor de rescate. Ventajas y desventajas.
49. Valuación de acciones y de utilidades retenidas. El mercado de capitales.


SEMANA Nº 15

50. Técnicas de presupuesto de capital.
51. El costo de capital. El costo de las fuentes de financiamiento. El costo del financiamiento a corto, mediano y largo plazo. Ponderación.
52. Caso práctico de la determinación del costo de capital de una empresa.
53. El palanqueo operativo. Los costos fijos como riesgo potencial. Caso práctico.
54. El palanqueo financiero, su grado de riesgo y el palanqueo total. Caso práctico.

