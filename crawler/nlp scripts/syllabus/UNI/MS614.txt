Medio Ambiente y Sostenibilidad

El Medio Ambiente fuente de vida, Ética Ambiental, Los contaminantes y contaminadores, El calentamiento global y sus consecuencias, impactos ambientales por los deshechos de plantas industriales y productos, el parque automotor fuente contaminadora, valorizaciones económicas como consecuencia de los impactos ambientales. Proyectos de impactos ambientales en la industria y obras de bien social.    Tratados: Kioto, Río + 5, Río + 10,  Cumbre de Johannesburgo 2002, las Naciones Unidas y el Medio Ambiente, Agenda o Programa 21 nacional regional y local o estrategia nacional de desarrollo; Ordenamiento territorial, Estructura Funcional; Propuesta  Agenda UNI - FIM.

1°  SEMANA
INTRODUCCIÓN. La vida y el Medio Ambiente, Cuidar  el Medio Ambiente es cuidar la vida humana, Ética Ambiental, Destrucción de nuestro hogar, Se puede cuidar el medio Ambiente, Prever los problemas, Utilizar el ingenio humano
Distribución de trabajos.

2°  SEMANA
EL MEDIO AMBIENTE FUENTE DE VIDA.- Medio Ambiente fuente de Vida, Carta a la Tierra- Misión y metas de la carta a la tierra- Responsabilidad Universal- Principios, Pacto global, Bienestar de la Humanidad, Causas de los Problemas.  

3°  SEMANA
CONTAMINANTES Y CONTAMINADORES.- Manejo sin control de productos lesivos al ambiente, consecuencias irreversibles, normas regulatorias,    contaminadores, quien controla a quien, irresponsabilidad humana,  Sustentación de avances de trabajos.

4°  SEMANA
EL CALENTAMIENTO GLOBAL Y SUS CONSECUENCIAS, EL OSCURECIMIENTO GLOBAL.-  Que es el calentamiento global, Que hacemos para frenar el calentamiento global, El Oscurecimiento global y sus consecuencias, Acciones que toman Gobiernos de todo el mundo. Sustentación de  avances de trabajos.

5°  SEMANA
IMPACTOS AMBIENTALES POR DESHECHOS DOMÉSTICOS, INDUSTRIALES Y URBANOS.-  Ubicación de las Industrias, materias primas, productos que laboran, Alteración del eco sistema y del ambiente, cambios en la fauna la flora.  Sustentación de  avances de trabajos.

6°  SEMANA
PARQUE AUTOMOTOR FUENTE CONTAMINADORA, El EFECTO INVERNADERO, antigüedad de los vehículos, irresponsabilidad de los conductores, medición de los contaminantes, ruidos sin control,  Sustentación de  avances de trabajos.

7°  SEMANA
VALORIZACIONES ECONÓMICAS COMO CONSECUENCIA DE LOS IMPACTOS AMBIENTALES.-   valorización económica de impactos ambientales, balances económicos ambientales. 
Sustentación de avances de trabajos

8°  SEMANA  

EXAMEN PARCIAL

9°  SEMANA            
LA CAPA DE OZONO, PROYECTOS AMBIENTALES EN LA INDUSTRIA Y OBRAS DE BIEN SOCIAL.-   Proyectos   viables, normas regulatorias ambientales, compromisos por la alteración ambiental, organismos  reguladores, compromisos. Sustentación final de trabajos.

10°  SEMANA
TRATADO DE: KIOTO, RIO + 5 , RIO + 10.-  Las alteraciones ambientales, Cambios climáticas en la tierra preocupan al hombre, difusión a la no contaminación, todos somos contaminadores,   obligatoriedad y  acatamiento ambientales, Sustentación final de trabajos

11°  SEMANA
CONFERENCIA DE JOHANNESBURGO SOBRE EL MEDIO AMBIENTE.  Riesgos de la tierra, medidas que se deben tener presente para conservar la vida en la tierra, compromisos de los acuerdos concertados, responsabilidad compartida. Sustentación final de trabajos. 
Sustentación final de trabajos

12°  SEMANA
NACIONES UNIDAS Y EL MEDIO AMBIENTE.- Informe Brundtland, nuevo futuro común, administrar los espacios comunes, el equilibrio de vida, el equilibrio en peligro,  estrategias urbanas, regionales, locales. 
Sustentación final de trabajos

13°  SEMANA
EL AGUA FUENTE DE VIDA, AGENDA O PROGRAMA  21.- Acuerdos y compromisos, de los gobiernos para la conservación ambiental, planes y programas orientados  a no degradar  el ambiente, responsabilidades, penalidades. 
Sustentación final de trabajos

14°  SEMANA
SEGURIDAD – DEFENZA NACIONAL,   Defensa Nacional de los Recursos naturales,  Defensa de las Ruinas Arqueológicas y el  medio ambiente, Folklor y Comidas Típicas y su Defensa. Sustentación final de trabajos

15°  SEMANA
AGENDA UNI FIM.- .Elaboración de la agenda UNI, etapas, alcances, compromisos.
Sustentación final de trabajos

EXAMEN FINAL

