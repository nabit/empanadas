LABORATORIO RESISTENCIA DE MATERIALES

Realizar un informe técnico tipo a manera de ejemplo basándose en el ensayo de tracción. Cumplimiento de Normas técnicas sobre resistencia de materiales.
Selección de materiales para fabricar maquinas y dispositivos y maquinas para realizar ensayos mecánicos. Fabricación de dispositivos y maquinas para realizar ensayos mecánicos. Fabricación de dispositivos y maquinas para realizar ensayos mecánicos. Demostración de ensayos  mecánicos entre los cuales están el de impacto, el de resistencia en ejes, perfiles, tubos y otros elementos de maquinas, de flexión, tracción, corte, compresión, torsión, el de fatiga simple y combinada. Desarrollo de monografía que se sustentara teórica y prácticamente sobre uno de los siguientes temas: Carga axial, torsión, flexión, pandeo de columna, pudiendo, a criterio del docente, considerarse adicionalmente algún tema de interés relacionado al tema.

1° SEMANA
Concepto de informe técnico y desarrollo del ensayo de tracción como ejemplo. Distribución de trabajos monográficos que estarán enmarcados en los siguientes temas: Carga axila, torsión, flexión, esfuerzo cortante transversal, cargas combinadas, cargas en vigas, pandeo de columnas, algún trabajo específico que el docente estime conveniente.

2° SEMANA
Revisión en clase de los informes técnicos sobre la tracción. Concepto a desarrollar: Las normas técnicas.

3° SEMANA
Revisión de las normas técnicas recopiladas para el trabajo monográfico. Concepto a desarrollar: Selección de fabricación para el diseño de dispositivos y máquinas para realizar ensayos mecánicos. Demostración del ensayo de impacto.

4° SEMANA
Primer práctica calificada. Normas técnicas. Selección de materiales de fabricación. El ensayo de tracción. El ensayo de impacto. Recepción de informes sobre normas técnicas.

5° SEMANA
Concepto a desarrollar: Máquinas para realizar ensayos mecánicos. Diseño y manufacturas de dispositivos para realizar ensayos mecánicos. Demostración del ensayo de resistencia en tubos.

6° SEMANA
Cálculo de resistencia en los dispositivos para realizar ensayos. Demostración del ensayo de fatiga combinada.

7° SEMANA
Segunda práctica calificada: Máquinas para realizar ensayos mecánicos. Dispositivos para realizar ensayos mecánicos. Resistencia en las máquinas y dispositivos para realizar ensayos mecánicos. Recepción de informes sobre máquinas, dispositivos y cálculos de resistencia.

8° SEMANA

EXAMENES PARCIALES



9° SEMANA
Concepto a desarrollar: La manufactura de dispositivos. Demostración del ensayo compresión.

10° SEMANA
Concepto a desarrollar: El acabado de los dispositivos. Demostración del ensayo de resistencia a la abrasión.

11° SEMANA
Concepto a desarrollar: tecnologías modernas para ensayar materiales. Demostración del ensayo de Torsión.

12° SEMANA
Concepto a desarrollar: Continuación sobre el tema de tecnologías modernas. Demostración del ensayo de fatiga.

13° SEMANA
Tercera práctica calificada: Diseño de máquinas y dispositivos para realizar ensayos mecánicos. Acabado de los dispositivos. En ensayo de torsión. En ensayo de fatiga. Tecnologías modernas para ensayar materiales. Recepción de informes sobre aplicación de tecnologías modernas para realizar ensayos mecánicos.

14° SEMANA
Sustentación teórica y práctica de las monografías.

15° SEMANA
Sustentación teórica y práctica de monografías.

16° SEMANA
EXAMENES FINALES
