TECNOLOGIA DE LAS ENERGIAS RENOVABLES  

Introducción.- Fuentes Renovables de Energía y sus Tecnologías.- Tecnologías para el aprovechamiento de la energía solar .- Tecnologías para el aprovechamiento de la energía eólica .- Tecnologías para el aprovechamiento de la energía hidráulica .- Tecnologías para el aprovechamiento de la energía de la biomasa .- Tecnologías de otras FRE.- Proyectos con FRE y sus tecnologías

1° SEMANA

Fundamentos, conceptos, clasificaciones.

2° SEMANA

 Transformaciones de la ES: fototérmicas y fotovoltaicas. Tecnologías para transformar la ES.: colectores planos y módulos solares.

3° SEMANA

Tecnologías para transformar la ES: colectores concentradores

4° SEMANA

 Tecnologías para aprovechar la ES; aplicaciones fototérmicas y fotovoltaicas.

5° SEMANA

Diseño y cálculo de sistemas solares.

6° SEMANA

Transformación de la EE; Tecnologías para transformar la EE

7° SEMANA

Tecnologías para aprovechar la EE; Aplicaciones mecánicas y eléctricas de las turbinas eólicas; Diseño y selección de sistemas eólicos.

8° SEMANA

Examen Parcial

9° SEMANA

Concepto de fuente renovable de energía aplicado a la EH; Transformación de la EH; Tecnologías para la transformación y aprovechamiento de la EH; Temas para monografía.

10° SEMANA

Tecnologías para la transformación y aprovechamiento de la EH; Consideraciones para el diseño y selección de turbinas hidráulicas.

Transformaciones energéticas de la biomasa.

11° SEMANA

Tecnologías para la transformación de la biomasa y de la energía de la biomasa; Tecnologías para la utilización de la energía de la biomasa.; primera sustentación de monografía.


12° SEMANA

Consideraciones para el diseño de sistemas de energía de la biomasa. 

Transformaciones de energía geotermal, de las olas, de las mareas y, otras.

Descripción y estudio de las tecnologías de transformación de otras FRE, Posibilidades de aplicación de estas otras FRE.

13° SEMANA

Posibilidades tecnológicas de integración de FRE; Economía de las FRE y sus tecnologías; segunda sustentación de la monografía.


14° SEMANA

Proyectos con integración energética renovable; estudio de casos.

15° SEMANA

Estudio de casos; sustentación final


16° SEMANA

SEMANA DE EXAMENES FINALES
