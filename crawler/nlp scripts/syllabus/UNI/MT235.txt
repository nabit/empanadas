CONTROL CLASICO 

Al finalizar el curso, el estudiante será capaz de analizar y diseñar sistemas de control liniales e invariantes en el tiempo de una entrada y una salida, mediante métodos de control clásico, tanto en respuesta en el tiempo, como en respuesta en frecuencia. 

1° SEMANA	

INTRODUCCION A LA INGENIERIA DE CONTROL 
Concepto básico 
Sistemas de control – Componentes de un sistema de control 
Control L.A. – Control de L.C.



2° SEMANA

MODELOS MATEMATICOS
Conceptos matemáticos más usados en Ingenieria de Control 
Transformadas de laplace

3° SEMANA

Funciones de transferencia ( Propiedades)
Operaciones con Diagramas de Bloques 
Linealización de sistemas no lineales 

4° SEMANA

Aplicaciones Moldeamiento matemático de Sistemas Eléctricos 
Mecánicos, Térmicos, etc. 

5° SEMANA

Respuesta en el Tiempo: Sistemas de primer orden – Sistemas de 2º Orden Sistemas de Orden Superior – Especificaciones en el dominio del tiempo. 

6° SEMANA

Propiedades de los sistemas Realimentados: Efectos de la realimentación – Influencia en la respuesta transitoria – Perturbaciones – Sensitividad – Respuesta en estado estacionario. 

7° SEMANA

Análisis del Error en los Sistemas Relaimentación: Precision – Error Dinámico – Errror Estacionario – Coeficientes de Error Estacionario. 

8°  SEMANA

EXAMEN PARCIAL

9°  SEMANA

METODOS DE ANALISIS DE ESTABILIDAD DE LOS SISTEMAS DE CONTROL 

Analisis de Estabilidad en el Dominio del Tiempo: Criterio de Routh Hurwitz – lugar Geométrico de las Raices – Criterio de Nyquist. 




10°  SEMANA y 11º SEMANA

Análisis de Estabilidad en el Dominio de la Frecuencia: Especificaciones en el dominio de la frecuencia – Diagramas de Bode (Magnitud y Face) – Carta de Nichols – Ralciones entre respuesta en el domino de la frecuencia y respuesta en el domino de tiempo. 

12°  SEMANA

DISEÑO DE CONTROLADORES 

Acciones Básicas de Control 

13°  SEMANA

Efectos de las acciones de Control – Obtención de las acciones de control. 

14°  SEMANA

Diseño de compensadores de adelanto de fase y atraso de fase. 

15°  SEMANA

Diseño del controlador PID (P,PI Y PID). 

