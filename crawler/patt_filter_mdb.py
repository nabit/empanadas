import os, sys
import json
import copy
import numpy
import pdb, ipdb

import pymongo
from datetime import date, datetime
from pymongo import MongoClient

temp = os.path.dirname(os.path.abspath(__file__))
pattern_dir = os.path.join(temp,'nlp scripts/patterns')

sys.path.append(pattern_dir)
from regex_patterns import *

## Datos externos
ident_tecnico = strip_encode(leer_tags(open(os.path.join(IDENTIFIER_STEM_DIR,'tecnico'))), False)
tec = stemAugmented('tecnico',degree=1)
ident_tecnico = [w for w in ident_tecnico if w != tec]

fileByIdent = getDocnameByCareer()

########################
# docname : {docname : true name}
nameByFile = json.loads(unicode(open('ident_names.json').read().decode('utf-8')))
temp = {}
for (file,name) in nameByFile.items():
    temp[file.strip(' ')] = name.strip(' ')
nameByFile = dict(temp)
########################

idByfile = {}
fileById = {}
idByName = {}
num_areas = 0

for (file,name) in nameByFile.items():
    idByfile[file] = num_areas
    idByName[name] = num_areas
    fileById[num_areas] = file
    num_areas += 1

###################################################################################################
# CASOS ESPECIALES - para que no los detecten/separen los patrones
# Mecanica Electrica
ident_mec_elect = [w for w in fileByIdent.keys() if fileByIdent[w]=='mecanica_electrica']
ident_mec_elect = '|'.join(addSuffixRegex(ident_mec_elect))

# Tecnologia Medica
ident_tec_med = [w for w in fileByIdent.keys() if fileByIdent[w]=='tecnologia_medica']
ident_tec_med = '|'.join(addSuffixRegex(ident_tec_med))

spec_case_patterns = [
    re.compile(addSuffixRegex(['ing'])[0] ),
    re.compile(ident_mec_elect),
    re.compile(ident_tec_med),
    re.compile(addSuffixRegex([stemAugmented('redes y comunicaciones')] )[0] ),
    re.compile(addSuffixRegex(['computacion e informatica']             )[0] ),
    re.compile(addSuffixRegex(['logistica y operaciones']               )[0] ),
    re.compile(addSuffixRegex(['operaciones y logistica']               )[0] ),
    re.compile(addSuffixRegex(['sistemas e informacion']                )[0] ),
    re.compile(addSuffixRegex([stemAugmented('medico veterinario')]     )[0] ),
    re.compile(addSuffixRegex([stemAugmented('estudio de abogado')] )[0] ),
    re.compile(r'^educacion\s*:'),
    re.compile(r'\([a-z]\)'),
]


spec_case_subs = [
    'ing',                      # normalizacion de ident de ingenieria
    'mecanica electrica',
    'medizintechnik',           # dafuq, tecnologia medica en Aleman xD
    'redes Y comunicaciones',
    'computacion E informatica',
    'logistica Y operaciones',
    'operaciones Y logistica',
    'sistemas E informacion',
    'veterinaria',
    'estudio_de_abogado',
    'formacion:',
    '',                         # elimina (a) o (o)
]

###################################################################################################
##                                  SETUP DE MUESTREO
MUESTREO = False
UPDATE_VH = False
UPDATE_ALL = False          # ACTUALIZA CAMPO CARRERAS EN MONGODB
UPDATE_DB = False     # solo cuando se carga nueva data a la DB
new_vh = []
carrera_muestreo = 'minas'
cm_list = [
           'minas',
           ]

num_posts = 10
total = 0
contador_jobs = 0

###################################################################################################
#                                   SETUP DE CONTEO CIRCLE PLOT
COUNT_ALL = False
AdjMatrix = numpy.zeros([num_areas,num_areas])
TotalSize = numpy.zeros(num_areas)

def sorter(T,sizeById, idByName):
    if "children" not in T:
        T["size"] = int(sizeById[ idByName[T["name"].strip(' ')] ])
        return T["size"]

    children = T["children"]
    temp = []
    _total = 0
    for child in children:
        subt_sum = sorter(child,sizeById, idByName)
        _total += subt_sum
        temp.append(tuple([subt_sum,child]))
    temp.sort(reverse=True)
    T["children"] = [k[1] for k in temp]
    return _total


def getSortedLeaves(T, V):
  if "children" not in T:
    V.append(T["name"])
    return
  for child in T["children"]:
    getSortedLeaves(child,V)

###################################################################################################
#                                   SETUP DE CONTEO STACKED BARS
STACKED_BARS_COUNT = False
sb_Count = [[] for i in range(num_areas)]

###################################################################################################
##                                                                  OBTENCION DE DATA

## MongoDB authentication and connection
uri = 'mongodb://giscia@localhost/'
client = MongoClient()
# conexion a nueva base de datos
db = client.JobDB
# Coleccion de trabajos
job_posts = db.core
#data = job_posts.find({'url' : {'$regex' : 'computrabajo'},'carreras' : {'$exists' : 0} }).batch_size(1000)
data = job_posts.find({ 'carreras' : {'$exists' : 0}
                        #'date': {'$gte': datetime(2014,07,1)}
                        }).limit(0).batch_size(1000)

print "%% Data loaded"

#ipdb.set_trace()

######################################################################################################################################################################################################
######################################################################################################################################################################################################
#                                                            HILO PRINCIPAL
patrones = compilePatterns()

for post in data:
    #print post['_id']
    # armar texto
    title = ''
    if post.get('title'):
      title = post.get('title')
    title = strip_encode(title)
    cuerpo = list([title]) # copia explicita
    
    des = post.get('description')
    if des:
      description = strip_encode([w for w in des.split('\n')])
      cuerpo.extend(description)

    # Variables para identificacion de carreras
    carreras = set()
    found_areas = []
    cuerpo_temp = []
    ing_post = False       # flag si posiblemente es un aviso de ingenieria

    for line in cuerpo:
        # preprocesado de casos especiales (ver header)
        for i,pat in enumerate(spec_case_patterns):
            line = pat.sub(spec_case_subs[i],line)
        if spec_case_patterns[0].search(line):
            ing_post = True                         # aviso pide ingenieros
        cuerpo_temp.append(line)
        
        # busqueda de patrones
        for (i,pattern) in enumerate(patrones):
            for car in careersFromPatterns(line,patrones,i):
                carreras.add(car.lower())

    cuerpo = list(cuerpo_temp)
    carreras = list(carreras)       # set -> list
    origc = list(carreras)  # copia explicita
    carreras = [stemAugmented(line).strip(' ') for line in carreras]
    carreras = list(set(carreras))

    ING_STEM  = 'ing'
    # buscar en identificadores | probar agregando ingenieria +
    for option in carreras:
        # variaciones nombres de ingenierias
        op1 = ING_STEM + " " + option
        op2 = ING_STEM + " de " + option
        op3 = ING_STEM + " en " + option
        op = [option,op1,op2,op3]

        for car in op:
            if car in fileByIdent.keys():
                # CASO ESPECIAL : QUIMICA | ING QUIMICA
                if any([car=='ing quimic',
                        car=='ing en quimic',
                        car=='ing de quimic',
                        car=='quimic']):                    # detect case
                    if ing_post:                            # es post de ingenieria?
                        found_areas.append('quimica')       # ing quimica
                    else:
                        found_areas.append('quimico')       # quimico puro
                else:                                       # caso general
                    found_areas.append(fileByIdent[car])

    if len(found_areas)==0:
        # buscar ident de tecnico menos 'tecnico'
        if searchIdentifier(cuerpo,ident_tecnico):
            found_areas.append('tecnico')
        else:
            # buscar ident de carreras en titulo
            found = False
            special_med = any([ searchIdentifier(cuerpo[0],stemAugmented('ventas')          ),
                                searchIdentifier(cuerpo[0],stemAugmented('producto medico') ),
                                searchIdentifier(cuerpo[0],stemAugmented('visitador medico')),
                                ])

            for (ident,doc) in fileByIdent.iteritems():
                # CASO ESPECIAL DE VENDEDORES DE PRODUCTOS MEDICOS
                if doc == 'medicina' and special_med:
                    continue
                if searchIdentifier(cuerpo[0],ident):
                    found_areas.append(doc)
                    found = True
            if not found:
                found_areas.append('otros')

    found_areas = list(set(found_areas))

    ##########################################################
    # CONTEO DE ARISTAS PARA CIRCLE PLOT
    if COUNT_ALL:
        if len(found_areas)>1:
            for (i,area) in enumerate(found_areas):
                u = idByfile[found_areas[i]]
                TotalSize[u] += 1
                for j in range(i+1,len(found_areas)):
                    v = idByfile[found_areas[j]]
                    AdjMatrix[u][v] += 1
                    AdjMatrix[v][u] += 1
        else:
            u = idByfile[found_areas[0]]
            AdjMatrix[u][u] += 1
            TotalSize[u] += 1

    ##########################################################
    # CONTEO DE PALABRAS PARA BARRAS APILADAS
    if STACKED_BARS_COUNT:
        term_freq = {}
        updateTermFreq(cuerpo,term_freq)
        total_words = sum(term_freq.values())
        for area in found_areas:
            sb_Count[idByfile[area]].append(total_words)

    ##########################################################
    # UPDATE DE VECTOR HASH DE CARRERAS SELECCIONADAS
    if UPDATE_VH or UPDATE_ALL:
        try:
            job_posts.update({'_id':post['_id']},{'$set' : {'carreras' : found_areas }})
        except:
            print "ERROR EN UPDATE:", sys.exc_info()[0]

    ##########################################################
    # MUESTREO DE CARRERA MENCIONADA
    if MUESTREO and not UPDATE_VH and not UPDATE_ALL:
        print '''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
(--   %d  ---)
%s
::: Carreras sin stemming
%s
::: Carreras
%s
::: Areas
%s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
''' % (contador_jobs,'\n'.join(cuerpo),origc,carreras,found_areas)
        contador_jobs += 1
    ##################################################################

    if total % 1000==0:
            print 'Total --> ',total
    total += 1
# ENDFOR
####################################################################################################################################


####################################################################################################################################
if COUNT_ALL:
    print 'Campo de trabajo, Numero de trabajos'

    # debug total count
    for i in range(num_areas):
        print "%s : %d" % (fileById[i],TotalSize[i])

    #pdb.set_trace()
    output = []
    unw_output = []
    for i in range(num_areas):
        u = fileById[i]
        item = {}
        item["name"] = nameByFile[u]
        item["size"] = TotalSize[i]

        # Self loop edge: only career
        only_carrer = AdjMatrix[i][i]
        self_loop = {}
        item["imports"] = []

        #UNweighted graph
        unw_item = copy.deepcopy(item)
        unw_item["imports"] = []

        for j in range(num_areas):
            v = fileById[j]
            if AdjMatrix[i][j] > 0:
                if i!=j:
                    temp = dict(name=nameByFile[v], weight=AdjMatrix[i][j])
                    item["imports"].append(temp)
                else:
                    temp = dict(name=nameByFile[v], weight=only_carrer)
                    self_loop = temp
                unw_item["imports"].append(temp["name"])
        if self_loop == {}:     # caso q no haya carrera_solo
            self_loop= dict(name=item["name"],weight=0)

        item["imports"].append(self_loop)

        output.append(item)
        unw_output.append(unw_item)

    # Actualiza cuentas y orden relativo en carreras json
    tree = json.loads(unicode(open('carreras.json').read().decode('utf-8')))
    suma = sorter(tree, TotalSize, idByName)
    open("carreras.json",'w').write(json.dumps(tree,ensure_ascii=False, encoding='utf-8', indent = 2).encode('utf-8'))

    sorted_leaves = []
    getSortedLeaves(tree,sorted_leaves)

    temp = []
    for name in sorted_leaves:
        for car in output:
            if car["name"] == name:
                temp.append(car)
                break
    output = list(temp)

    temp = []
    for name in sorted_leaves:
        for car in unw_output:
            if car["name"] == name:
                temp.append(car)
                break
    unw_output = list(temp)

    open("adjmatrix.json",'w').write(json.dumps(output,ensure_ascii=False, encoding='utf-8',indent = 2).encode('utf-8'))
    open("unw_adjmatrix.json",'w').write(json.dumps(unw_output,ensure_ascii=False, encoding='utf-8',indent = 2).encode('utf-8'))

    print "ADJM & Carreras actualizadas"

####################################################################################################################################
if STACKED_BARS_COUNT:
    max_Length = 0
    for i,area in enumerate(sb_Count):
        area.sort()
        if len(area)>0:
            max_Length = max(max_Length,area[-1])
    output = []
    for i in range(num_areas):
        u = fileById[i]
        item = {}
        item["name"] = nameByFile[u]
        item["data"] = sb_Count[i]
        output.append(item)

    open("raw_stackedBars.json",'w').write(json.dumps(output,ensure_ascii=False, encoding='utf-8').encode('utf-8'))

    print "----------------------------"
    print "Maxima cantidad de palabras: ",max_Length
