from lib_spiders import *
from core.models import Details, Description

URL_BASE = u'http://www.computrabajo.com.pe'
FROM_PAGE = 1
TO_PAGE = 2
LOG_FILE = 'compuPE.log'


class JobSpider(scrapy.Spider):

    name = "compuPE"
    allowed_domains = ["www.computrabajo.com.pe"]

    def __init__(self, from_page=FROM_PAGE, to_page=TO_PAGE):
        self.from_page = int(from_page)
        self.to_page = int(to_page)
        self.start_urls = [
            "http://www.computrabajo.com.pe/ofertas-de-trabajo/?p=%d" % s for s in range(int(from_page), int(to_page))
        ]
        self.jobs_seen = 0
        self.jobs_repeated = 0
        self.jobs_no_title = 0
        self.jobs_des = 0
        self.jobs_no_des = 0
        self.jobs_error_detail = 0
        begin_log = '''
        ******************************************************
        *   SESSION STARTED [%s]     *
        ******************************************************
        ''' % datetime.datetime.now()
        print begin_log
        with open(LOG_FILE, 'a') as log:
            log.write(begin_log)

        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def parse(self, response):

        links = response.xpath('//*[@id="p_ofertas"]/li/article/div/div[1]/h1/a/@href').extract()
        dates = response.xpath('//*[@id="p_ofertas"]/li/article/div/div[2]/ul/li[1]/text()').extract()
        areas = response.xpath('//*[@id="p_ofertas"]/li/article/div/div[2]/ul/li[3]/text()').extract()

        requests = []

        if len(links) == 0:
            with open(LOG_FILE, 'a') as log:
                log.write('\n[%s]:  [WARNING]  numbers of links is zero\n' % datetime.datetime.now())
                log.write('url: %s\n' % response.url)

        for i in range(len(links)):
            item = JobItem()

            item['url'] = URL_BASE + links[i]
            item['date'] = join_text(strip_encode([dates[i]], flag_lower=True))
            item['area'] = areas[i]

            request = scrapy.Request(url=item['url'], callback=self.parseJob)
            request.meta['item'] = item
            requests.append(request)

        return requests

    def parseJob(self, response):
        item = response.meta['item']

        categories = strip_encode(response.xpath('/html/body/div[2]/article/section[5]/ul/li/h3/text()').extract(),
                                  flag_code=True, flag_lower=True)

        self.jobs_seen += 1  # JOB SEEN ++

        title = company = typeJob = salary = place = ''
        for i, category in enumerate(categories):
            dir_cat = '/html/body/div[2]/article/section[5]/ul/li[%d]/p/text()' % (i+1)
            cat_txt = join_text(strip_encode(response.xpath(dir_cat).extract()))
            if category == 'empresa':
                company = cat_txt
            if category == 'titulo de la oferta':
                title = cat_txt
            if category == 'jornada':
                typeJob = cat_txt
            if category == 'salario':
                salary = cat_txt
            if category == 'localidad':
                place = cat_txt

        if title == '' or company == '':
            self.jobs_no_title += 1  # JOB NOT SAVED - MISSING TITLE OR COMPANY
            with open(LOG_FILE, 'a') as log:
                log.write('\n[%s]:  [WARNING]  parser did not find title or company\n' % datetime.datetime.now())
                log.write('url: %s\n' % response.url)
            return

        date = item['date']
        date = dateComputrabajo(date)
        item['date'] = date

        area = item['area']

        Nodedes = response.xpath('/html/body/div[2]/article/section[1]/div[2]/ul')
        childs = Nodedes.xpath('*')
        childs = childs[:-3]

        des = ''
        for node in childs:
            text_field = node.xpath('text()').extract()
            text_field = join_text(process(join_text(strip_encode(text_field))))
            des += text_field + '\n'

        des = clean_space(des)

        #########################################################################################
        # Ver si se repite aviso
        # Query: mismos detalles, dif de 14 dias

        repeated_jobs = Details.objects.filter(
            company=company
        ).filter(
            date__gte=date - datetime.timedelta(days=14)
        ).exclude(
            date__gte=date + datetime.timedelta(days=1)
        ).filter(
            place=place
        ).filter(
            title=title
        )

        # Query: misma descripcion
        repeated = False
        for job in repeated_jobs:
            descs = job.description_set.filter(description=des)
            if len(descs) > 0:
                repeated = True
                break

        if repeated:
            self.jobs_repeated += 1  # JOB REPEATED BETWEEN THE 14-DAY RANGE
            return item

        ###############################################################################
        # Make PK as hash
        # escoge 50 primeras y 50 ultimas para hacer el hash de descripcion

        if len(des) <= 300:
            hdes = des
        else:
            delta = (len(des) - 250) / 2
            hdes = des[delta: delta + 250]

        # Details Hash
        detail_hash = hashlib.sha256()
        detail_hash.update((title + company + place + str(date)).encode('utf8'))
        detail_hash = detail_hash.hexdigest()

        # Description Hash
        description_hash = hashlib.sha256()
        description_hash.update(hdes.encode('utf8'))
        description_hash = description_hash.hexdigest()

        detail_saved = False
        try:
            # ##############################################################################################
            # Agregando el Job a la DB
            # Primero se crea el q no tiene M2M field
            details = Details(hash=detail_hash, title=title, company=company, date=date,
                              place=place, typeJob=typeJob, salary=salary, url=item['url'])
            details.save()
            detail_saved = True
            try:
                # existe, Agregar relacion descripcion - detalles
                descrip = Description.objects.get(pk=description_hash)
                descrip.job.add(details)
                self.jobs_des += 1

            except ObjectDoesNotExist:
                # Crear description entry
                self.jobs_no_des += 1
                descrip = Description(hash=description_hash, description=des, area=area)
                descrip.save()
                descrip.job.add(details)
        finally:
            if detail_saved is False:
                self.jobs_error_detail += 1
                with open(LOG_FILE, 'a') as log:
                    log.write('\n[%s]:  **ERROR**  detail not saved\n' % datetime.datetime.now())
                    log.write('url: %s\n' % response.url)
            pass

        return item

    def spider_closed(self, spider):

        if spider is not self:
            return

        final_log = '''
        **************** LOG PAGES[%4d, %4d> ******************************
        * >>> Found %4d jobs in this session [%s]  *
        * >>> Found %4d jobs repeated                                      *
        * >>> Found %4d jobs with no details                               *
        * >>> Found %4d jobs with no description                           *
        * >>> Found %4d jobs with description                              *
        * >>> Found %4d jobs with error detail                             *
        *********************************************************************
        ''' % (self.from_page, self.to_page, self.jobs_seen, datetime.datetime.now(), self.jobs_repeated,
               self.jobs_no_title, self.jobs_no_des, self.jobs_des, self.jobs_error_detail)

        with open(LOG_FILE, 'a') as log:
                log.write(final_log)

        print final_log

















