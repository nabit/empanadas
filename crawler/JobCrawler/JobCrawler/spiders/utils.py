import pymongo
from datetime import date, datetime
from pymongo import MongoClient

## MongoDB authentication and connection

def connect2Mongo():
  ''' :return : database object
  '''
  uri = 'mongodb://ronotex:1234@localhost/admin'
  client = MongoClient(uri)
  # conexion a nueva base de datos
  db = client.JobDB
  
  return db
