from lib_spiders import *
from core_ve.models import Details, Description

URL_BASE = u'http://www.bumeran.com.ve'


class JobSpider(scrapy.Spider):
    name = "bumeranVEN"
    allowed_domains = ["www.bumeran.com.ve"]
    start_urls = [
        #"http://www.bumeran.com.ve/empleos-pagina-%s.html" %s for s in range(1,1190)
        "http://www.bumeran.com.ve/empleos-publicacion-hoy-pagina-%s.html" %s for s in range(1, 36)
    ]

    def parse(self, response):
        links = response.xpath('//div[@class="span12"]/a/@href').extract()
        requests = []

        for i in range(len(links)):
            item = JobItem()
            item['url'] = URL_BASE + links[i]
            request = scrapy.Request(url=item['url'], callback=self.parseJob)
            request.meta['item'] = item
            requests.append(request)

        return requests

    def parseJob(self, response):
        item = response.meta['item']

        title = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/div[2]/h2/text()').extract())).strip()

        company = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/div[2]/h3/a/text()').extract())).strip()
        if len(company) == 0:
            company = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/div[2]/h3/text()').extract())).strip()

        date = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/table/tbody/tr[1]/td/text()').extract())).strip()
        date = dateBumeran(date)

        typeJob = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/table/tbody/tr[3]/td/text()').extract())).strip()

        salary = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/table/tbody/tr[4]/td/text()').extract())).strip()

        place = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/table/tbody/tr[5]/td/a[1]/text()').extract())).strip()

        area = join_text(strip_encode(response.xpath('//*[@id="contenidoAviso"]/div[2]/table/tbody/tr[2]/td/text()').extract())).strip()

        des = []
        nodeAviso = response.xpath('//div[@id="contenido_aviso"]/*')
        for node in nodeAviso:
            des += process(node.extract())

        des = join_text(strip_encode(des)).strip()

        #################################################################################
        ### Cambios a campos de tablas

        dateToday = date

        #########################################################################################
        ############################################
        ## Ver si se repite aviso
        # Query: mismos detalles, dif de 14 dias
        repeated_jobs = Details.objects.filter(
            company=company
        ).filter(
            date__gte=date - datetime.timedelta(days=14)
        ).exclude(
            date__gte=date + datetime.timedelta(days=1)
        ).filter(
            place=place
        ).filter(
            title=title
        )
        # Query: misma descripcion
        repeated = False
        for job in repeated_jobs:
            descs = job.description_set.filter(description=des)
            if len(descs) > 0:
                repeated = True
                break

        if repeated:
            return item  # Se repite, salir

        ###############################################################################
        ## Make PK as hash
        # escoge 50 primeras y 50 ultimas para hacer el hash de descripcion
        if len(des) < 200:
            hdes = des
        else:
            hdes = des[:100] + des[-100:]

        # Details Hash
        detail_hash = hashlib.sha256()
        detail_hash.update((title + company + place + str(date)).encode('utf8'))
        detail_hash = detail_hash.hexdigest()

        # Description Hash
        description_hash = hashlib.sha256()
        description_hash.update(hdes.encode('utf8'))
        description_hash = description_hash.hexdigest()

        try:
            ###############################################################################################
            ## Agregando el Job a la DB
            # Primero se crea el q no tiene M2M field
            details = Details(hash=detail_hash, title=title, company=company, date=date,
                              place=place, typeJob=typeJob, salary=salary , url=item['url'])
            details.save()

            # Buscar si descripcion ya existe
            descrip = None
            try:
                # existe, Agregar relacion descripcion - detalles
                descrip = Description.objects.get(pk=description_hash)
                descrip.job.add(details)
            except ObjectDoesNotExist:
                # Crear description entry
                descrip = Description(hash=description_hash, description=des, area=area)
                descrip.save()
                descrip.job.add(details)
        except:
            pass

        return item
