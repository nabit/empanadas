__author__ = 'kevinbm'

import unicodedata
import datetime
from JobCrawler.settings import MESES
import hashlib
from django.core.exceptions import ObjectDoesNotExist
import scrapy
from JobCrawler.items import JobItem
from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals


def parseDate(fecha):
    # if hoy is found in date then return 0
    if fecha.find(u'hoy') != -1:
        return 0
    # else find the number of days ago
    data = fecha.split(' ')
    for i in range(len(data)):
        if data[i] == 'hace':
            return int(data[i + 1])
    # is supposed that never gets here
    return -1


def dateBumeran(fecha):
    dia = int(fecha[0:2])
    anio = int(fecha[len(fecha) - 4:])
    mes = int(MESES[fecha[6:len(fecha) - 8]])
    return datetime.date(anio, mes, dia)


def parseDateAptitus(date):
    # if hoy is found in date then return 0
    if date.find(u'hoy') != -1:
        return 0
    # else find the number of days ago
    data = date.split(' ')
    for i in range(len(data)):
        if data[i] == 'hace':
            return -1 * int(data[i + 1])
    # it is supposed that never gets here
    return -1


def separatorAptitus(text):
    ans = []
    for line in text:
        ind = line.find('\n')
        if ind != -1:
            ans += [line[:ind]] + separatorAptitus(strip_encode([line[ind + 1:]], False))
        else:
            ans += [line]
    strip_encode(ans, False)
    return ans


def dateTextAptitus(date):
    d = parseDateAptitus(date)
    fecha = datetime.date.today() + datetime.timedelta(d)
    return fecha


def print_job_details(title, company, url, area=None):
    print 'title > ', title
    print 'compa > ', company
    print 'url   > ', url
    if area is not None:
        print 'area  > ', area
    return


def dateComputrabajo(fecha):
    fecha = fecha.split(',')[0]

    if fecha == 'hoy':
        return datetime.date.today()

    if fecha == 'ayer':
        return datetime.date.today() + datetime.timedelta(-1)

    fecha = fecha.strip().split(' ')
    dia = fecha[0].strip()
    mes = fecha[-1].strip()

    dia = int(dia)
    mes = int(MESES[mes])

    if mes == 1:
        anio = 2015
    else:
        anio = 2014

    return datetime.date(anio, mes, dia)


def process(text):
    ind1 = text.find(u'<')
    ind2 = text.find(u'>')
    ans = []
    if ind1 != -1 and ind2 != -1:
        tag = text[ind1 + 1:ind2]
        if tag == u'br':
            ans += [text[0:ind1]]
            ans += process(text[ind2 + 1:])
        else:
            ans += process(text[0:ind1] + " " + text[ind2 + 1:])
        return ans
    else:
        return [text]


def strip_encode(text, flag_code=False, flag_lower=False):  # return ascii and strip text
    ans = []
    if flag_code:
        text = [unicodedata.normalize('NFKD', line).encode('ascii', 'ignore') for line in text]
    for texto in text:
        if flag_lower:
            texto = texto.lower()
        texto = texto.strip()
        ans.append(texto)
    return ans


def join_text(text):
    ans = u''
    if isinstance(text, basestring):
        return text
    if len(text) > 1:
        for line in text:
            ans += line + u' \n '
    else:
        for line in text:
            ans += line
    return ans


def clean_space(text):
    import re
    text = re.sub('[ \r\t\f\v]+', ' ', text)
    text = re.sub('(\n+ *)+', '\n', text)
    return text
