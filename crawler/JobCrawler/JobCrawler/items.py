# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib.djangoitem import DjangoItem

from core.models import Details, Description


class JobModel(DjangoItem):  # hash, title, typeJob, date, salary, company, url, place
    django_model = Details


class DesModel(DjangoItem):  # job, functions, requirements, area
    django_model = Description


class JobItem(Item):
    title = Field()
    company = Field()
    functions = Field()
    requirements = Field()
    place = Field()
    url = Field()
    date = Field()
    area = Field()


class JobItemBumeran(Item):
    title = Field()
    company = Field()
    date = Field()
    area = Field()
    typeJob = Field()
    salary = Field()
    place = Field()
    url = Field()
    description = Field()
    requirements = Field()
    functions = Field()
    bloques = Field()

