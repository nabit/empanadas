# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
# http://doc.scrapy.org/en/latest/topics/settings.html
#
import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
CRAWLER_DIR = os.path.join(BASE_DIR, 'crawler')
PROJECT_DIR = os.path.join(BASE_DIR, 'www')
TAGS_DIR = os.path.join(CRAWLER_DIR, "block_tags")
STOP_DIR = os.path.join(CRAWLER_DIR, "stopwords")

ESPECIALES = ['\n', '\r', '\t', '\a', '\b', ' ', '-', '>', '*']

MESES = {'enero': '01', 'febrero': '02', 'marzo': '03', 'abril': '04',
         'mayo': '05', 'junio': '06', 'julio': '07', 'agosto': '08',
         'setiembre': '09', 'septiembre': '09', 'octubre': '10', 'noviembre': '11', 'diciembre': '12',
         1: 'enero', 2: 'febrero', 3: 'marzo', 4: 'abril',
         5: 'mayo', 6: 'junio', 7: 'julio', 8: 'agosto',
         9: 'septiembre', 10: 'octubre', 11: 'noviembre', 12: 'diciembre'
}

sys.path.append(PROJECT_DIR)
sys.path.append(CRAWLER_DIR)

os.environ['DJANGO_SETTINGS_MODULE'] = 'project.dev'

# import django
# django.setup() #for django1.7

"""
Config Scrapy
"""
BOT_NAME = 'JobCrawler'

SPIDER_MODULES = ['JobCrawler.spiders']
NEWSPIDER_MODULE = 'JobCrawler.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = '(+FriendlyMan)'
